-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 25, 2018 at 10:26 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `com_fmu`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` bigint(20) NOT NULL,
  `customer_address` varchar(255) NOT NULL,
  `customer_city` varchar(255) NOT NULL,
  `customer_contact` varchar(255) NOT NULL,
  `customer_country` varchar(255) NOT NULL,
  `customer_email` varchar(255) NOT NULL,
  `customer_f_name` varchar(255) NOT NULL,
  `customer_l_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `customer_address`, `customer_city`, `customer_contact`, `customer_country`, `customer_email`, `customer_f_name`, `customer_l_name`) VALUES
(1, 'Al-Hafeez Shooping Cebter', 'Lahore', 'Muhammad Nadeem', 'Pakistan', 'messenger@anydatapoint.com', 'Muhammad Yaseen', 'Muhammad Ameen'),
(2, 'Al-Hafeez Shooping Cebter', 'Lahore', 'Atieq ur Rehman', 'Pakistan', 'atieq.geoviz@gmail.com', 'Atieq ur Rehman', 'M Anwar'),
(3, '', '', '', '', 'gsn.ahmad@gmail.com', 'dsg', ''),
(4, '', '', '', '', 'mkh@gmail.com', 'MKH', ''),
(5, '', '', '', '', 'gsn.ahmad@gmail.com', 'dsg', ''),
(6, '', '', '', '', 'kamran@gmail.com', 'kamran', ''),
(7, '', '', '', '', 'kamran@gmail.com', 'kamran', ''),
(8, '', '', '', '', 'kamran@gmail.com', 'kamran', '');

-- --------------------------------------------------------

--
-- Table structure for table `email_transport`
--

CREATE TABLE `email_transport` (
  `email_transport_id` bigint(20) NOT NULL,
  `authentication` varchar(255) DEFAULT NULL,
  `email_password` varchar(255) DEFAULT NULL,
  `email_port` int(11) DEFAULT NULL,
  `email_server` varchar(255) DEFAULT NULL,
  `email_username` varchar(255) DEFAULT NULL,
  `secure_connection` varchar(255) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_transport`
--

INSERT INTO `email_transport` (`email_transport_id`, `authentication`, `email_password`, `email_port`, `email_server`, `email_username`, `secure_connection`, `customer_id`) VALUES
(2, 'YES', '(Geoviz)', 2525, 'm53.siteground.biz', 'messenger@anydatapoint.com', 'YES', 2);

-- --------------------------------------------------------

--
-- Table structure for table `file_pattern`
--

CREATE TABLE `file_pattern` (
  `file_merging_pattern_id` bigint(20) NOT NULL,
  `inbound_pattern_name` varchar(255) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `inbound_custom_token` text,
  `inbound_custom_identifier` varchar(255) DEFAULT NULL,
  `inbound_custom_identifier_order` int(11) DEFAULT NULL,
  `inbound_document_identifier` varchar(255) DEFAULT NULL,
  `inbound_document_identifier_order` int(11) DEFAULT NULL,
  `inbound_document_seprator` varchar(255) DEFAULT NULL,
  `inbound_package_identifier` varchar(255) DEFAULT NULL,
  `inbound_package_identifier_order` int(11) DEFAULT NULL,
  `merging_job_context_id` bigint(20) DEFAULT NULL,
  `pattern_name` varchar(255) DEFAULT NULL,
  `file_transportid` int(11) DEFAULT NULL,
  `ftp_remote_directory` varchar(255) DEFAULT NULL,
  `file_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `file_pattern`
--

INSERT INTO `file_pattern` (`file_merging_pattern_id`, `inbound_pattern_name`, `customer_id`, `inbound_custom_token`, `inbound_custom_identifier`, `inbound_custom_identifier_order`, `inbound_document_identifier`, `inbound_document_identifier_order`, `inbound_document_seprator`, `inbound_package_identifier`, `inbound_package_identifier_order`, `merging_job_context_id`, `pattern_name`, `file_transportid`, `ftp_remote_directory`, `file_order`) VALUES
(7, 'DDM_EntryNumber_DOT_VinNumber_DOTNumber_ShipValue', 2, '[{\"customKey\":\"Custom Token\",\"customValue\":\"VinNumber\",\"customOrder\":3,\"uuid\":\"e3c8b99e-ca06-4d6b-abb4-fb9d84f3e257\",\"documentIdentifier\":\"DOT\"},{\"customKey\":\"Custom Token\",\"customValue\":\"DOTNumber\",\"customOrder\":4,\"uuid\":\"f294421e-64ec-473e-bc41-142d32fe211b\",\"documentIdentifier\":\"DOT\"},{\"customKey\":\"Custom Token\",\"customValue\":\"ShipValue\",\"customOrder\":5,\"uuid\":\"61c8b142-1029-418b-bb09-0000fc4ac66d\",\"documentIdentifier\":\"DOT\"}]', 'DDM', 0, 'DOT', 2, '_', 'EntryNumber', 1, 3, 'DOT', 1, '/gvtest/my/inbound', 0),
(8, 'DDM_EntryNumber_EPA_VinNumber', 2, '[{\"customKey\":\"Custom Token\",\"customValue\":\"VinNumber\",\"customOrder\":3,\"uuid\":\"0ab3b050-f5c7-471c-a276-267ace577625\",\"documentIdentifier\":\"EPA\"}]', 'DDM', 0, 'EPA', 2, '_', 'EntryNumber', 1, 3, 'EPA', 1, '/gvtest/my/inbound', 1),
(9, 'DDM_EntryNumber_NAFTA_VinNumber', 2, '[{\"customKey\":\"Custom Token\",\"customValue\":\"VinNumber\",\"customOrder\":3,\"uuid\":\"ffe1d2ec-7c4c-4612-b3c5-26ffbace7505\",\"documentIdentifier\":\"NAFTA\"}]', 'DDM', 0, 'NAFTA', 2, '_', 'EntryNumber', 1, 3, 'NAFTA', 1, '/gvtest/my/inbound', 2),
(10, 'DDM_EntryNumber_POLICY_VinNumber_PolicyNumber', 2, '[{\"customKey\":\"Custom Token\",\"customValue\":\"VinNumber\",\"customOrder\":3,\"uuid\":\"6019fd26-95a4-42e2-9e0e-274274c8e4ce\",\"documentIdentifier\":\"POLICY\"},{\"customKey\":\"Custom Token\",\"customValue\":\"PolicyNumber\",\"customOrder\":4,\"uuid\":\"87297c4d-7188-477f-8c9f-e5694a14372c\",\"documentIdentifier\":\"POLICY\"}]', 'DDM', 0, 'POLICY', 2, '_', 'EntryNumber', 1, 3, 'POLICY', 1, '/gvtest/my/inbound', 3);

-- --------------------------------------------------------

--
-- Table structure for table `ftp_transport`
--

CREATE TABLE `ftp_transport` (
  `file_transportid` int(11) NOT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `ftp_local_directory` varchar(255) DEFAULT NULL,
  `ftp_name` varchar(255) DEFAULT NULL,
  `host_password` varchar(255) DEFAULT NULL,
  `host_user_name` varchar(255) DEFAULT NULL,
  `inbound_ip_address` varchar(255) DEFAULT NULL,
  `merged_files_location` varchar(255) DEFAULT NULL,
  `port` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ftp_transport`
--

INSERT INTO `ftp_transport` (`file_transportid`, `customer_id`, `ftp_local_directory`, `ftp_name`, `host_password`, `host_user_name`, `inbound_ip_address`, `merged_files_location`, `port`) VALUES
(1, 2, 'C:\\Users\\GV-PK005\\Desktop\\Base Folder\\2\\syncFolder/inbound', 'SimplyVins', 'vins(123)', 'simplyvins', 'ftp.simplyvins.omnieddi.com', NULL, 21);

-- --------------------------------------------------------

--
-- Table structure for table `merged_files`
--

CREATE TABLE `merged_files` (
  `merged_file_id` bigint(20) NOT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `file_context` varchar(255) DEFAULT NULL,
  `job_type` varchar(255) DEFAULT NULL,
  `timestamp` date DEFAULT NULL,
  `directory` text,
  `received_files` text,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merged_files`
--

INSERT INTO `merged_files` (`merged_file_id`, `customer_id`, `file_context`, `job_type`, `timestamp`, `directory`, `received_files`, `status`) VALUES
(1, 2, NULL, 'Merging Job', '2018-09-13', 'DDM_30025880691_1GT423C83BF173445_1920188969_DDM_30025880691_EPA_1GT423C83BF173445_DDM_30025880691_NAFTA_1GT423C83BF173445_DDM1098018_20180913125729.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(2, 2, NULL, 'Merging Job', '2018-09-13', 'DDM_30025880691_1GT423C83BF173445_1920188969_DDM_30025880691_EPA_1GT423C83BF173445_DDM_30025880691_NAFTA_1GT423C83BF173445_DDM1098018_20180913125729.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(3, 2, NULL, 'Merging Job', '2018-09-13', 'DDM_30025880691_1GT423C83BF173445_1920188969_DDM_30025880691_EPA_1GT423C83BF173445_DDM_30025880691_NAFTA_1GT423C83BF173445_DDM1098018_20180913130159.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(4, 2, NULL, 'Merging Job', '2018-09-13', 'DDM_30025880691_1GT423C83BF173445_1920188969_DDM_30025880691_EPA_1GT423C83BF173445_DDM_30025880691_NAFTA_1GT423C83BF173445_DDM1098018_20180913130159.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(5, 2, NULL, 'Merging Job', '2018-09-13', 'DDM_30025880691_1GT423C83BF173445_1920188969_DDM_30025880691_EPA_1GT423C83BF173445_DDM_30025880691_NAFTA_1GT423C83BF173445_DDM1098018_20180913130859.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(6, 2, NULL, 'Merging Job', '2018-09-13', 'DDM_30025880691_1GT423C83BF173445_1920188969_DDM_30025880691_EPA_1GT423C83BF173445_DDM_30025880691_NAFTA_1GT423C83BF173445_DDM1098018_20180913130859.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(7, 2, NULL, 'Merging Job', '2018-09-24', 'TEST20180924050722.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(8, 2, NULL, 'Merging Job', '2018-09-24', 'TEST20180924052530.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(9, 2, NULL, 'Merging Job', '2018-09-24', 'TEST20180924052530.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(10, 2, NULL, 'Merging Job', '2018-09-24', 'TEST20180924052740.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(11, 2, NULL, 'Merging Job', '2018-09-24', 'TEST20180924052740.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(12, 2, NULL, 'Merging Job', '2018-09-24', 'TEST20180924053228.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(13, 2, NULL, 'Merging Job', '2018-09-24', 'TEST20180924053646.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(14, 2, NULL, 'Merging Job', '2018-09-24', 'TEST20180924053646.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(15, 2, NULL, 'Merging Job', '2018-09-24', 'java.io.FileOutputStream@43fcc63f', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(16, 2, NULL, 'Merging Job', '2018-09-24', 'java.io.FileOutputStream@43fcc63f', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(17, 2, NULL, 'Merging Job', '2018-09-24', 'java.io.FileOutputStream@7805f318', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(18, 2, NULL, 'Merging Job', '2018-09-24', 'java.io.FileOutputStream@7805f318', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(19, 2, NULL, 'Merging Job', '2018-09-24', 'java.io.FileOutputStream@638ff5d1', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(20, 2, NULL, 'Merging Job', '2018-09-24', 'java.io.FileOutputStream@638ff5d1', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(21, 2, NULL, 'Merging Job', '2018-09-24', 'java.io.FileOutputStream@175202bc', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(22, 2, NULL, 'Merging Job', '2018-09-24', 'java.io.FileOutputStream@175202bc', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(23, 2, NULL, 'Merging Job', '2018-09-24', 'java.io.FileOutputStream@6b217013', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(24, 2, NULL, 'Merging Job', '2018-09-24', 'java.io.FileOutputStream@6b217013', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(25, 2, NULL, 'Merging Job', '2018-09-24', 'TEST20180924063033.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(26, 2, NULL, 'Merging Job', '2018-09-24', 'TEST20180924063033.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(27, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_20180924075410.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(28, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_20180924080048.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(29, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_1GT423C83BF173445_1GT423C83BF173445_1GT423C83BF173445_20180924082145.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(30, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_20180924082701.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(31, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_20180924083301.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(32, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_20180924084219.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(33, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_20180924084219.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(34, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_20180924084517.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(35, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_20180924084517.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(36, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_20180924084656.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(37, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_20180924084656.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(38, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_20180924085203.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(39, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_20180924085203.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(40, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_20180924085729.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(41, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_20180924091609.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(42, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_20180924093327.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1'),
(43, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_20180924093327.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '0'),
(44, 2, NULL, 'Merging Job', '2018-09-24', 'DDM_30025880691_1920188969_DDM1098018_1GT423C83BF173445_20180924093647.pdf', '[{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"},{\"filename\":\"inbound\",\"filePath\":\"DDM_30025880691_POLICY_1GT423C83BF173445_DDM1098018_20180626140336-OUT.PDF\"}]', '1');

-- --------------------------------------------------------

--
-- Table structure for table `merging_job_context`
--

CREATE TABLE `merging_job_context` (
  `merging_job_context_id` bigint(20) NOT NULL,
  `bundle_identifier` varchar(255) DEFAULT NULL,
  `custom_identifier` varchar(255) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `file_extension` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merging_job_context`
--

INSERT INTO `merging_job_context` (`merging_job_context_id`, `bundle_identifier`, `custom_identifier`, `customer_id`, `file_extension`) VALUES
(3, 'EntryNumber', 'DDM', 2, 'pdf');

-- --------------------------------------------------------

--
-- Table structure for table `merging_workflow`
--

CREATE TABLE `merging_workflow` (
  `workflow_id` bigint(20) NOT NULL,
  `merging_actions` int(11) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `merging_workflow_name` varchar(255) DEFAULT NULL,
  `merging_job_context_id` bigint(20) DEFAULT NULL,
  `outbound_id` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merging_workflow`
--

INSERT INTO `merging_workflow` (`workflow_id`, `merging_actions`, `customer_id`, `merging_workflow_name`, `merging_job_context_id`, `outbound_id`, `user_id`) VALUES
(3, 1, 2, 'DDM - Docs', 3, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `outbound_ftp`
--

CREATE TABLE `outbound_ftp` (
  `outbound_id` int(11) NOT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `outbound_pattern_id` int(11) DEFAULT NULL,
  `outbound_merged_files_location` varchar(255) DEFAULT NULL,
  `file_transportid` int(11) DEFAULT NULL,
  `ftp_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outbound_ftp`
--

INSERT INTO `outbound_ftp` (`outbound_id`, `customer_id`, `file_path`, `outbound_pattern_id`, `outbound_merged_files_location`, `file_transportid`, `ftp_name`) VALUES
(4, 2, NULL, 3, '/gvtest/my/merged', 1, 'SimplyVins');

-- --------------------------------------------------------

--
-- Table structure for table `outbound_pattern`
--

CREATE TABLE `outbound_pattern` (
  `outbound_pattern_id` int(11) NOT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `outbound_custom_identifier` varchar(255) DEFAULT NULL,
  `outbound_custom_identifier_order` int(11) DEFAULT NULL,
  `outbound_custom_token` text,
  `outbound_document_identifier` varchar(255) DEFAULT NULL,
  `outbound_document_identifier_order` int(11) DEFAULT NULL,
  `outbound_document_seprator` varchar(255) DEFAULT NULL,
  `outbound_package_identifier` varchar(255) DEFAULT NULL,
  `outbound_package_identifier_order` int(11) DEFAULT NULL,
  `ounbound_pattern_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outbound_pattern`
--

INSERT INTO `outbound_pattern` (`outbound_pattern_id`, `customer_id`, `outbound_custom_identifier`, `outbound_custom_identifier_order`, `outbound_custom_token`, `outbound_document_identifier`, `outbound_document_identifier_order`, `outbound_document_seprator`, `outbound_package_identifier`, `outbound_package_identifier_order`, `ounbound_pattern_name`, `email`) VALUES
(3, 2, 'DDM', 0, '[{\"customKey\":\"Custom Token\",\"customValue\":\"DOTNumber(DOT)\",\"customOrder\":2,\"uuid\":\"0f91f4fd-4d46-435b-b1e9-a25abc743bd6\"},{\"customKey\":\"Custom Token\",\"customValue\":\"PolicyNumber(POLICY)\",\"customOrder\":3,\"uuid\":\"05f4efe3-5456-4969-8cc7-af5d6dc6891b\"},{\"customKey\":\"Custom Token\",\"customValue\":\"VinNumber(DOT)\",\"customOrder\":4,\"uuid\":\"013241af-27d3-4ac9-86b4-740e5c631167\"}]', NULL, NULL, '_', 'EntryNumber', 1, 'DDM_EntryNumber_DOTNumber(DOT)_PolicyNumber(POLICY)_VinNumber(DOT)', 'sqa.whiztec@gmail.com,myaseen.whiztec@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `user_status` smallint(6) DEFAULT NULL,
  `user_full_name` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  `customer_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `address`, `city`, `country`, `user_email`, `user_password`, `phone`, `province`, `user_status`, `user_full_name`, `user_name`, `token`, `zip_code`, `customer_id`) VALUES
(1, 'ABCD', 'Lahore', 'oh', 'hkjhkjh', 'Admin', 'kjh', 'Punjab', 0, 'Muhammad Yaseen', 'Admin', 'ZQQWAQT0R6DAHH1TH5', '54000', 1),
(2, 'ABCD', 'Lahore', 'oh', 'atieq.geoviz@gmail.com', '123', '', '', 1, 'Atieq ur Rehman', 'aradmin', 'TWKL4QB7XT13EUCJKU', '54000', 2),
(3, NULL, NULL, NULL, 'gsn.ahmad@gmail.com', '123', NULL, NULL, NULL, NULL, 'aradmin23', NULL, NULL, 3),
(4, NULL, NULL, NULL, 'mkh@gmail.com', '123', NULL, NULL, NULL, NULL, 'mkh', NULL, NULL, 4),
(5, NULL, NULL, NULL, 'gsn.ahmad@gmail.com', 'Admin', NULL, NULL, NULL, NULL, 'mkh1', 'L3QFH56EREV3XHFLQ2', NULL, 5),
(6, NULL, NULL, NULL, 'kamran@gmail.com', '123', NULL, NULL, NULL, NULL, 'karman', NULL, NULL, 6),
(7, NULL, NULL, NULL, 'kamran@gmail.com', '123', NULL, NULL, NULL, NULL, 'kamran', NULL, NULL, 7),
(8, NULL, NULL, NULL, 'kamran@gmail.com', '123', NULL, NULL, NULL, NULL, 'kamran1', NULL, NULL, 8);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `user_role_id` int(11) NOT NULL,
  `role` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `email_transport`
--
ALTER TABLE `email_transport`
  ADD PRIMARY KEY (`email_transport_id`),
  ADD UNIQUE KEY `UK_9sb1sjcdvabnadn67s68gcnul` (`email_username`),
  ADD KEY `FKng6fum6l86sbt4m43u6oklpvk` (`customer_id`);

--
-- Indexes for table `file_pattern`
--
ALTER TABLE `file_pattern`
  ADD PRIMARY KEY (`file_merging_pattern_id`),
  ADD KEY `FKs7j0070d205fi7y3v4xcnhxnc` (`file_transportid`),
  ADD KEY `FKto3n775moqndvd7i749stdkc1` (`merging_job_context_id`);

--
-- Indexes for table `ftp_transport`
--
ALTER TABLE `ftp_transport`
  ADD PRIMARY KEY (`file_transportid`);

--
-- Indexes for table `merged_files`
--
ALTER TABLE `merged_files`
  ADD PRIMARY KEY (`merged_file_id`);

--
-- Indexes for table `merging_job_context`
--
ALTER TABLE `merging_job_context`
  ADD PRIMARY KEY (`merging_job_context_id`);

--
-- Indexes for table `merging_workflow`
--
ALTER TABLE `merging_workflow`
  ADD PRIMARY KEY (`workflow_id`),
  ADD KEY `FKsn8r9dkk410kkx9eqq89pwit9` (`merging_job_context_id`),
  ADD KEY `FKk264ea1tvawhvsbii7bcq32q9` (`outbound_id`),
  ADD KEY `FKoxqp27yq2fa3tcw6vft85sdny` (`user_id`);

--
-- Indexes for table `outbound_ftp`
--
ALTER TABLE `outbound_ftp`
  ADD PRIMARY KEY (`outbound_id`),
  ADD KEY `FKjujx54h5pb54dbcmtmyk25yx3` (`outbound_pattern_id`),
  ADD KEY `FK4mh7u4ffudctw90ljeg7h578` (`file_transportid`);

--
-- Indexes for table `outbound_pattern`
--
ALTER TABLE `outbound_pattern`
  ADD PRIMARY KEY (`outbound_pattern_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `FKdptx0i3ky01svofwjytq5iry0` (`customer_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`user_role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `email_transport`
--
ALTER TABLE `email_transport`
  MODIFY `email_transport_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `file_pattern`
--
ALTER TABLE `file_pattern`
  MODIFY `file_merging_pattern_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `ftp_transport`
--
ALTER TABLE `ftp_transport`
  MODIFY `file_transportid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `merged_files`
--
ALTER TABLE `merged_files`
  MODIFY `merged_file_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `merging_job_context`
--
ALTER TABLE `merging_job_context`
  MODIFY `merging_job_context_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `merging_workflow`
--
ALTER TABLE `merging_workflow`
  MODIFY `workflow_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `outbound_ftp`
--
ALTER TABLE `outbound_ftp`
  MODIFY `outbound_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `outbound_pattern`
--
ALTER TABLE `outbound_pattern`
  MODIFY `outbound_pattern_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `email_transport`
--
ALTER TABLE `email_transport`
  ADD CONSTRAINT `FKng6fum6l86sbt4m43u6oklpvk` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`);

--
-- Constraints for table `file_pattern`
--
ALTER TABLE `file_pattern`
  ADD CONSTRAINT `FKs7j0070d205fi7y3v4xcnhxnc` FOREIGN KEY (`file_transportid`) REFERENCES `ftp_transport` (`file_transportid`),
  ADD CONSTRAINT `FKto3n775moqndvd7i749stdkc1` FOREIGN KEY (`merging_job_context_id`) REFERENCES `merging_job_context` (`merging_job_context_id`);

--
-- Constraints for table `merging_workflow`
--
ALTER TABLE `merging_workflow`
  ADD CONSTRAINT `FKk264ea1tvawhvsbii7bcq32q9` FOREIGN KEY (`outbound_id`) REFERENCES `outbound_pattern` (`outbound_pattern_id`),
  ADD CONSTRAINT `FKoxqp27yq2fa3tcw6vft85sdny` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `FKsn8r9dkk410kkx9eqq89pwit9` FOREIGN KEY (`merging_job_context_id`) REFERENCES `merging_job_context` (`merging_job_context_id`);

--
-- Constraints for table `outbound_ftp`
--
ALTER TABLE `outbound_ftp`
  ADD CONSTRAINT `FK4mh7u4ffudctw90ljeg7h578` FOREIGN KEY (`file_transportid`) REFERENCES `ftp_transport` (`file_transportid`),
  ADD CONSTRAINT `FKjujx54h5pb54dbcmtmyk25yx3` FOREIGN KEY (`outbound_pattern_id`) REFERENCES `outbound_pattern` (`outbound_pattern_id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FKdptx0i3ky01svofwjytq5iry0` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
