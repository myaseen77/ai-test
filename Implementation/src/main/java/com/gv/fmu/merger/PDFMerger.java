package com.gv.fmu.merger;


import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFMergerUtility;

import java.io.*;

import java.util.ArrayList;
import java.util.List;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

public class PDFMerger {

//	public static void main(String[] args) {
//		PDFMerger merger = new PDFMerger();
//		merger.combine("d:/");
//	}

	public static void combineFolders(String sourcePath, String destinationPath , String fileName) {
		try
		{
			PDFMergerUtility mergePdf = new PDFMergerUtility();
			File _folder = new File(sourcePath);
			File[] filesInFolder;
//			listing all files from the source folder
			filesInFolder = _folder.listFiles();
			for (File string : filesInFolder)
			{
//				Utility only merge PDF 
				if(getFileExtension(string).equals("pdf") || getFileExtension(string).equals("PDF") ){
//					merging files by adding source to PDFMergerUtil 
					mergePdf.addSource(string);
				}
			}
//			setting final combined file to specific location
			mergePdf.setDestinationFileName(destinationPath+"/"+fileName+".pdf");
//			finally merging files
			mergePdf.mergeDocuments();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void doMerge(List<InputStream> list, OutputStream outputStream) throws DocumentException, IOException
	{
		Document document = new Document();
		PdfWriter writer = PdfWriter.getInstance(document, outputStream);
		document.open();
		PdfContentByte cb = writer.getDirectContent();

		for (InputStream in : list)
		{
			PdfReader reader = new PdfReader(in);
			for (int i = 1; i <= reader.getNumberOfPages(); i++)
			{
				document.newPage();
				//import the page from source pdf
				PdfImportedPage page = writer.getImportedPage(reader, i);

				//add the page to the destination pdf
				cb.addTemplate(page, 0, 0);
			}
		}

		outputStream.flush();
		document.close();
		outputStream.close();
	}
	
	public static String combineFiles(List<String> sourcefiles, String destinationPath , String fileName)
    {
        String finalName = "";
        try
		{
            PDFMergerUtility mergePdf = new PDFMergerUtility();
            List<PDDocument> pdDocuments = new ArrayList<>();
            for (String file : sourcefiles)
            {
                File string = new File(file);

//				Utility only merge PDF
                if(getFileExtension(string).equals("pdf") || getFileExtension(string).equals("PDF"))
                {
//					merging files by adding source to PDFMergerUtil
                    PDDocument doc1 = PDDocument.load(string);
                    pdDocuments.add(doc1);
                    mergePdf.addSource(string);
					doc1.close();
                }
            }
//			setting final combined file to specific location
            finalName = destinationPath + "/" + fileName + ".pdf";

			File file = new File(finalName);

            mergePdf.setDestinationFileName(file.toString());

//			finally merging files
            mergePdf.mergeDocuments();
			pdDocuments.clear();

/*            for (PDDocument doc : pdDocuments)
            {
                doc.close();
            }*/
		}
		catch (Exception e)
        {
			e.printStackTrace();
		}
		return finalName;
	}

//	getting file extension 
	public static String getFileExtension(File file)
    {
		String[] strArray = file.getName().split("\\.");
		return  strArray[strArray.length - 1];
	}
	

	public void createNew() {
		PDDocument document = null;
		try {
			String filename = "test.pdf";
			document = new PDDocument();
			PDPage blankPage = new PDPage();
			document.addPage(blankPage);
			document.save(filename);
		} catch (Exception e) {

		}
	}
}
