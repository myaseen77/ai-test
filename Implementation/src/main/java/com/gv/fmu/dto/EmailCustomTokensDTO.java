package com.gv.fmu.dto;

import lombok.Data;

import java.util.List;

/**
 * Created by Muhammad Yaseen on 5/24/2018.
 */

public class EmailCustomTokensDTO
{
    private String emailKey;
    private String emailValue;

    public String getEmailKey() {
        return emailKey;
    }

    public void setEmailKey(String emailKey) {
        this.emailKey = emailKey;
    }

    public String getEmailValue() {
        return emailValue;
    }

    public void setEmailValue(String emailValue) {
        this.emailValue = emailValue;
    }
}
