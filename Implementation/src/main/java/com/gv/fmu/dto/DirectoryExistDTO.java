package com.gv.fmu.dto;

import lombok.Data;

/**
 * Created by Muhammad Yaseen on 8/2/2018.
 */
@Data
public class DirectoryExistDTO {
    private String directory;
    private String customerId;
    private String ip;

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
