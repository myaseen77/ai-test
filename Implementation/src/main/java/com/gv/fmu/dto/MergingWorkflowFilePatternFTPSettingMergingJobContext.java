package com.gv.fmu.dto;

import com.gv.fmu.domain.FileMergingPattern;
import com.gv.fmu.domain.InputOutputSetting;
import com.gv.fmu.domain.MergingJobContext;
import com.gv.fmu.domain.MergingWorkFlow;

/**
 * Created by Maan on 25/05/2018.
 */
public class MergingWorkflowFilePatternFTPSettingMergingJobContext
{
    private MergingWorkFlow mergingWorkFlow;
    private MergingJobContext mergingJobContext;
    private InputOutputSetting inputOutputSetting;
    private FileMergingPattern fileMergingPattern;

    public MergingWorkFlow getMergingWorkFlow()
    {
        return mergingWorkFlow;
    }

    public void setMergingWorkFlow(MergingWorkFlow mergingWorkFlow)
    {
        this.mergingWorkFlow = mergingWorkFlow;
    }

    public MergingJobContext getMergingJobContext()
    {
        return mergingJobContext;
    }

    public void setMergingJobContext(MergingJobContext mergingJobContext)
    {
        this.mergingJobContext = mergingJobContext;
    }

    public InputOutputSetting getInputOutputSetting()
    {
        return inputOutputSetting;
    }

    public void setInputOutputSetting(InputOutputSetting inputOutputSetting)
    {
        this.inputOutputSetting = inputOutputSetting;
    }

    public FileMergingPattern getFileMergingPattern()
    {
        return fileMergingPattern;
    }

    public void setFileMergingPattern(FileMergingPattern fileMergingPattern)
    {
        this.fileMergingPattern = fileMergingPattern;
    }
}
