package com.gv.fmu.dto;

import com.gv.fmu.domain.*;

import java.util.List;

/**
 * Created by Maan on 25/05/2018.
 */

public class MergingWorkAttributesDTO
{
    private Long workflowId;

    private String workflowName;

    private String workflow_Inbound;

    private String workflow_Outbound;

    private Integer actions;

    private Long customerId;

    private OutboundMergingPatternDTO outboundMergingPatternDTO;

    private List<FileMergingPatternDTO> fileMergingPatternDTO;

    private MergingJobContext mergingJobContext;

    private User user;

    private OutBoundFTP outBoundFTP;

    public MergingJobContext getMergingJobContext() {
        return mergingJobContext;
    }

    public void setMergingJobContext(MergingJobContext mergingJobContext) {
        this.mergingJobContext = mergingJobContext;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(Long workflowId) {
        this.workflowId = workflowId;
    }

    public String getWorkflowName()
    {
        return workflowName;
    }

    public void setWorkflowName(String workflowName)
    {
        this.workflowName = workflowName;
    }

    public String getWorkflow_Inbound() {
        return workflow_Inbound;
    }

    public void setWorkflow_Inbound(String workflow_Inbound) {
        this.workflow_Inbound = workflow_Inbound;
    }

    public String getWorkflow_Outbound()
    {
        return workflow_Outbound;
    }

    public void setWorkflow_Outbound(String workflow_Outbound)
    {
        this.workflow_Outbound = workflow_Outbound;
    }

    public Integer getActions()
    {
        return actions;
    }

    public void setActions(Integer actions)
    {
        this.actions = actions;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public OutboundMergingPatternDTO getOutboundMergingPatternDTO() {
        return outboundMergingPatternDTO;
    }

    public void setOutboundMergingPatternDTO(OutboundMergingPatternDTO outboundMergingPatternDTO) {
        this.outboundMergingPatternDTO = outboundMergingPatternDTO;
    }

    public List<FileMergingPatternDTO> getFileMergingPatternDTO() {
        return fileMergingPatternDTO;
    }

    public void setFileMergingPatternDTO(List<FileMergingPatternDTO> fileMergingPatternDTO) {
        this.fileMergingPatternDTO = fileMergingPatternDTO;
    }

    public OutBoundFTP getOutBoundFTP() { return outBoundFTP; }

    public void setOutBoundFTP(OutBoundFTP outBoundFTP) { this.outBoundFTP = outBoundFTP; }
}
