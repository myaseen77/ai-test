package com.gv.fmu.dto;

import java.util.UUID;

/**
 * Created by Muhammad Yaseen on 6/4/2018.
 */
public class JobContext
{
    String status;
    UUID uuid;
    String docType;
    String filePathLocal;

    public JobContext(String status, UUID uuid, String filePathLocal, String docType)
    {
        this.status = status;
        this.uuid = uuid;
        this.filePathLocal = filePathLocal;
        this.docType = docType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getFilePathLocal() {
        return filePathLocal;
    }

    public void setFilePathLocal(String filePathLocal) {
        this.filePathLocal = filePathLocal;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }
}
