package com.gv.fmu.dto;

import com.gv.fmu.domain.Customer;

import java.util.List;

/**
 * Created by Maan on 27/05/2018.
 */
public class EmailTransportDTO
{
    private Long emailTransportId;

    private String emailServer;

    private String authentication;

    private String secureConnection;

    private String emailUserName;

    private String emailPassword;

    private Integer emailPort;

/*    private List<EmailCustomTokensDTO> emailCustomTokens;*/

    private Customer customer;

    public Long getEmailTransportId() {
        return emailTransportId;
    }

    public void setEmailTransportId(Long emailTransportId) {
        this.emailTransportId = emailTransportId;
    }

    public String getEmailServer() {
        return emailServer;
    }

    public void setEmailServer(String emailServer) {
        this.emailServer = emailServer;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public String getSecureConnection() {
        return secureConnection;
    }

    public void setSecureConnection(String secureConnection) {
        this.secureConnection = secureConnection;
    }

    public String getEmailUserName() {
        return emailUserName;
    }

    public void setEmailUserName(String emailUserName) {
        this.emailUserName = emailUserName;
    }

    public String getEmailPassword() {
        return emailPassword;
    }

    public void setEmailPassword(String emailPassword) {
        this.emailPassword = emailPassword;
    }

    public Integer getEmailPort() {
        return emailPort;
    }

    public void setEmailPort(Integer emailPort) {
        this.emailPort = emailPort;
    }

/*    public List<EmailCustomTokensDTO> getEmailCustomTokens() {
        return emailCustomTokens;
    }

    public void setEmailCustomTokens(List<EmailCustomTokensDTO> emailCustomTokens) {
        this.emailCustomTokens = emailCustomTokens;
    }*/

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
