package com.gv.fmu.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Created by WhiztecTestingServer on 5/21/2018.
 */

public class MergedFilesDTO
{
    private Long mergedFileId;

    private List<MergedReceivedFileContextDTO> receivedFiles;

    private String mergedFile;

    private String fileContext;

    private String jobType;

    private Date localDate;

    private String status;

    public MergedFilesDTO()
    {

    }

    public Long getMergedFileId() {
        return mergedFileId;
    }

    public void setMergedFileId(Long mergedFileId) {
        this.mergedFileId = mergedFileId;
    }

    public List<MergedReceivedFileContextDTO> getReceivedFiles() {
        return receivedFiles;
    }

    public void setReceivedFiles(List<MergedReceivedFileContextDTO> receivedFiles) {
        this.receivedFiles = receivedFiles;
    }

    public String getMergedFile() {
        return mergedFile;
    }

    public void setMergedFile(String mergedFile) {
        this.mergedFile = mergedFile;
    }

    public String getFileContext() {
        return fileContext;
    }

    public void setFileContext(String fileContext) {
        this.fileContext = fileContext;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public Date getLocalDate() {
        return localDate;
    }

    public void setLocalDate(Date localDate) {
        this.localDate = localDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
