package com.gv.fmu.dto;

import com.gv.fmu.domain.InputOutputSetting;
import com.gv.fmu.domain.OutBoundFTP;

import java.util.List;

/**
 * Created by Muhammad Yaseen on 6/8/2018.
 */
public class OutboundMergingPatternDTO
{
    private Integer outboundPatternId;

    private List<OutboundCustomTokens> outboundCustomToken;

    private String outboundCustomIdentifier;

    private Integer outboundCustomIdentifierOrder;

    private String outboundDocumentIdentifier;

    private Integer outboundDocumentIdentifierOrder;

    private String outboundDocumentSeprator;

    private String outboundPackageIdentifier;

    private Integer outboundPackageIdentifierOrder;

    private String outboundPatternName;

    private String email;

    private String emailBody;

    private String emailSubject;

    private List<InputOutputSetting> inputOutputSettings;

    private List<OutBoundFTP> outBoundFTPs;

    private Long customerId;

    private Long workflowId;

    public Integer getOutboundPatternId() {
        return outboundPatternId;
    }

    public void setOutboundPatternId(Integer outboundPatternId) {
        this.outboundPatternId = outboundPatternId;
    }

    public List<OutboundCustomTokens> getOutboundCustomToken() {
        return outboundCustomToken;
    }

    public void setOutboundCustomToken(List<OutboundCustomTokens> outboundCustomToken)
    { this.outboundCustomToken = outboundCustomToken; }

    public String getOutboundCustomIdentifier() {
        return outboundCustomIdentifier;
    }

    public void setOutboundCustomIdentifier(String outboundCustomIdentifier) {
        this.outboundCustomIdentifier = outboundCustomIdentifier;
    }

    public String getOutboundDocumentIdentifier() {
        return outboundDocumentIdentifier;
    }

    public void setOutboundDocumentIdentifier(String outboundDocumentIdentifier) {
        this.outboundDocumentIdentifier = outboundDocumentIdentifier;
    }

    public String getOutboundDocumentSeprator() {
        return outboundDocumentSeprator;
    }

    public void setOutboundDocumentSeprator(String outboundDocumentSeprator) {
        this.outboundDocumentSeprator = outboundDocumentSeprator;
    }

    public String getOutboundPackageIdentifier() {
        return outboundPackageIdentifier;
    }

    public void setOutboundPackageIdentifier(String outboundPackageIdentifier) {
        this.outboundPackageIdentifier = outboundPackageIdentifier;
    }

    public String getOutboundPatternName() {
        return outboundPatternName;
    }

    public void setOutboundPatternName(String outboundPatternName) {
        this.outboundPatternName = outboundPatternName;
    }

    public Integer getOutboundCustomIdentifierOrder() {
        return outboundCustomIdentifierOrder;
    }

    public void setOutboundCustomIdentifierOrder(Integer outboundCustomIdentifierOrder) {
        this.outboundCustomIdentifierOrder = outboundCustomIdentifierOrder;
    }

    public Integer getOutboundDocumentIdentifierOrder() {
        return outboundDocumentIdentifierOrder;
    }

    public void setOutboundDocumentIdentifierOrder(Integer outboundDocumentIdentifierOrder) {
        this.outboundDocumentIdentifierOrder = outboundDocumentIdentifierOrder;
    }

    public Integer getOutboundPackageIdentifierOrder() {
        return outboundPackageIdentifierOrder;
    }

    public void setOutboundPackageIdentifierOrder(Integer outboundPackageIdentifierOrder) {
        this.outboundPackageIdentifierOrder = outboundPackageIdentifierOrder;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public List<InputOutputSetting> getInputOutputSettings() {
        return inputOutputSettings;
    }

    public void setInputOutputSettings(List<InputOutputSetting> inputOutputSettings) {
        this.inputOutputSettings = inputOutputSettings;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<OutBoundFTP> getOutBoundFTPs() {
        return outBoundFTPs;
    }

    public void setOutBoundFTPs(List<OutBoundFTP> outBoundFTPs) {
        this.outBoundFTPs = outBoundFTPs;
    }

/*    public List<InboundTokenForOutboundDTO> getInboundTokenForOutboundDTOs() {
        return inboundTokenForOutboundDTOs;
    }

    public void setInboundTokenForOutboundDTOs(List<InboundTokenForOutboundDTO> inboundTokenForOutboundDTOs) {
        this.inboundTokenForOutboundDTOs = inboundTokenForOutboundDTOs;
    }*//*    public List<InboundTokenForOutboundDTO> getInboundTokenForOutboundDTOs() {
        return inboundTokenForOutboundDTOs;
    }

    public void setInboundTokenForOutboundDTOs(List<InboundTokenForOutboundDTO> inboundTokenForOutboundDTOs) {
        this.inboundTokenForOutboundDTOs = inboundTokenForOutboundDTOs;
    }*/

    public Long getWorkflowId() { return workflowId; }

    public void setWorkflowId(Long workflowId) { this.workflowId = workflowId; }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }
}
