package com.gv.fmu.dto;

import com.gv.fmu.domain.InputOutputSetting;
import com.gv.fmu.domain.MergingJobContext;
import com.gv.fmu.domain.MergingWorkFlow;

import java.util.List;

/**
 * Created by Muhammad Yaseen on 5/22/2018.
 */

public class FileMergingPatternDTO
{
    private Long fileMergingPatternId;

    private List<InboundCustomTokens> inboundCustomToken;

    private String inboundCustomerIdentifier;

    private Integer inboundCustomerIdentifierOrder;

    private String inboundDocumentIdentifier;

    private Integer inboundDocumentIdentifierOrder;

    private String inboundDocumentSeprator;

    private String inboundPackageIdentifier;

    private Integer inboundPackageIdentifierOrder;

    private String inboundPatternName;

    private String completePattern;

    private Integer fileOrder;

    private Long customerId;

    private String ftpRemoteDirectory;

    private InputOutputSetting inputOutputSetting;

    private MergingJobContext mergingJobContext;

    private MergingWorkFlow mergingWorkFlow;

    private FileMergingPatternDTO2 fileMergingPatternDTO2;

    public Long getFileMergingPatternId() {
        return fileMergingPatternId;
    }

    public void setFileMergingPatternId(Long fileMergingPatternId) {
        this.fileMergingPatternId = fileMergingPatternId;
    }

    public List<InboundCustomTokens> getInboundCustomToken() {
        return inboundCustomToken;
    }

    public void setInboundCustomToken(List<InboundCustomTokens> inboundCustomToken) {
        this.inboundCustomToken = inboundCustomToken;
    }

    public String getInboundCustomerIdentifier() {
        return inboundCustomerIdentifier;
    }

    public void setInboundCustomerIdentifier(String inboundCustomerIdentifier) {
        this.inboundCustomerIdentifier = inboundCustomerIdentifier;
    }

    public String getInboundDocumentIdentifier() {
        return inboundDocumentIdentifier;
    }

    public void setInboundDocumentIdentifier(String inboundDocumentIdentifier) {
        this.inboundDocumentIdentifier = inboundDocumentIdentifier;
    }

    public String getInboundDocumentSeprator() {
        return inboundDocumentSeprator;
    }

    public void setInboundDocumentSeprator(String inboundDocumentSeprator) {
        this.inboundDocumentSeprator = inboundDocumentSeprator;
    }

    public String getInboundPackageIdentifier() {
        return inboundPackageIdentifier;
    }

    public void setInboundPackageIdentifier(String inboundPackageIdentifier) {
        this.inboundPackageIdentifier = inboundPackageIdentifier;
    }

    public String getInboundPatternName() {
        return inboundPatternName;
    }

    public void setInboundPatternName(String inboundPatternName) {
        this.inboundPatternName = inboundPatternName;
    }

    public InputOutputSetting getInputOutputSetting() {
        return inputOutputSetting;
    }

    public void setInputOutputSetting(InputOutputSetting inputOutputSetting) {
        this.inputOutputSetting = inputOutputSetting;
    }

    public MergingJobContext getMergingJobContext() {
        return mergingJobContext;
    }

    public void setMergingJobContext(MergingJobContext mergingJobContext) {
        this.mergingJobContext = mergingJobContext;
    }

    public Integer getInboundCustomerIdentifierOrder() {
        return inboundCustomerIdentifierOrder;
    }

    public void setInboundCustomerIdentifierOrder(Integer inboundCustomerIdentifierOrder) {
        this.inboundCustomerIdentifierOrder = inboundCustomerIdentifierOrder;
    }

    public Integer getInboundDocumentIdentifierOrder() {
        return inboundDocumentIdentifierOrder;
    }

    public void setInboundDocumentIdentifierOrder(Integer inboundDocumentIdentifierOrder) {
        this.inboundDocumentIdentifierOrder = inboundDocumentIdentifierOrder;
    }

    public Integer getInboundPackageIdentifierOrder() {
        return inboundPackageIdentifierOrder;
    }

    public void setInboundPackageIdentifierOrder(Integer inboundPackageIdentifierOrder) {
        this.inboundPackageIdentifierOrder = inboundPackageIdentifierOrder;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCompletePattern() {
        return completePattern;
    }

    public void setCompletePattern(String completePattern) {
        this.completePattern = completePattern;
    }

    public String getFtpRemoteDirectory() {
        return ftpRemoteDirectory;
    }

    public void setFtpRemoteDirectory(String ftpRemoteDirectory) {
        this.ftpRemoteDirectory = ftpRemoteDirectory;
    }

    public MergingWorkFlow getMergingWorkFlow() {
        return mergingWorkFlow;
    }

    public void setMergingWorkFlow(MergingWorkFlow mergingWorkFlow) {
        this.mergingWorkFlow = mergingWorkFlow;
    }

    public FileMergingPatternDTO2 getFileMergingPatternDTO2() {
        return fileMergingPatternDTO2;
    }

    public void setFileMergingPatternDTO2(FileMergingPatternDTO2 fileMergingPatternDTO2) {
        this.fileMergingPatternDTO2 = fileMergingPatternDTO2;
    }

    public Integer getFileOrder() {
        return fileOrder;
    }

    public void setFileOrder(Integer fileOrder) {
        this.fileOrder = fileOrder;
    }
}
