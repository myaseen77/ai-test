package com.gv.fmu.dto;

import lombok.Data;

/**
 * Created by Muhammad Yaseen on 5/9/2018.
 */

@Data
public class ResponseObject
{
    private Integer status;
    private String message;
    private Object data;
    private Integer searchCount;

    public ResponseObject(Integer status, String message, Object data)
    {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public ResponseObject(Integer status, String message, Object data, Integer QueryCount)
    {
        this.status = status;
        this.message = message;
        this.data = data;
        this.searchCount = QueryCount;
    }


}
