package com.gv.fmu.dto;

import lombok.Data;

import java.util.List;

/**
 * Created by Muhammad Yaseen on 7/13/2018.
 */
@Data
public class FileMergingPatternDTO2
{
    private Long fileMergingPatternId;

    private List<InboundCustomTokens> inboundCustomToken;

    private String inboundCustomerIdentifier;

    private Integer inboundCustomerIdentifierOrder;

    private String inboundDocumentIdentifier;

    private Integer inboundDocumentIdentifierOrder;

    private String inboundDocumentSeprator;

    private String inboundPackageIdentifier;

    private Integer inboundPackageIdentifierOrder;

    private String completePattern;

    private Integer fileOrder;

    private Long customerId;

    private String patternName;

    private Long mergingJobContextId;
}
