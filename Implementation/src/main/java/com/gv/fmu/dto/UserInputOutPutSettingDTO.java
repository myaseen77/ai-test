package com.gv.fmu.dto;

import com.gv.fmu.domain.User;

/**
 * Created by Muhammad Yaseen on 5/8/2018.
 */

public class UserInputOutPutSettingDTO
{
    private User objUser;
    private InputOutputSettingDTO inputOutputSettingDTO;

    public User getObjUser()
    {
        return objUser;
    }

    public void setObjUser(User objUser) {
        this.objUser = objUser;
    }

    public InputOutputSettingDTO getInputOutputSettingDTO() {
        return inputOutputSettingDTO;
    }

    public void setInputOutputSettingDTO(InputOutputSettingDTO inputOutputSettingDTO) {
        this.inputOutputSettingDTO = inputOutputSettingDTO;
    }
}
