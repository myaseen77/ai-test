package com.gv.fmu.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Muhammad Yaseen on 5/21/2018.
 */

@NoArgsConstructor
public class MergedReceivedFileContextDTO
{
    private String filename;
    private String filePath;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
