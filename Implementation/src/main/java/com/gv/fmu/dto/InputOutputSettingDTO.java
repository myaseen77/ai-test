package com.gv.fmu.dto;

/**
 * Created by Maan on 26/05/2018.
 */
public class InputOutputSettingDTO
{
    private Long fileTransportID;

    private String inboundIpAddress;

    private String outboundIpAddress;

    private String ftpName;

    private String hostUserName;

    private String hostPassword;

    private Integer port;

    private String flag;

    public Long getFileTransportID() {
        return fileTransportID;
    }

    public void setFileTransportID(Long fileTransportID) {
        this.fileTransportID = fileTransportID;
    }

    public String getInboundIpAddress() {
        return inboundIpAddress;
    }

    public void setInboundIpAddress(String inboundIpAddress) {
        this.inboundIpAddress = inboundIpAddress;
    }

    public String getOutboundIpAddress() {
        return outboundIpAddress;
    }

    public void setOutboundIpAddress(String outboundIpAddress) {
        this.outboundIpAddress = outboundIpAddress;
    }

    public String getFtpName() {
        return ftpName;
    }

    public void setFtpName(String ftpName) {
        this.ftpName = ftpName;
    }

    public String getHostUserName() {
        return hostUserName;
    }

    public void setHostUserName(String hostUserName) {
        this.hostUserName = hostUserName;
    }

    public String getHostPassword() {
        return hostPassword;
    }

    public void setHostPassword(String hostPassword) {
        this.hostPassword = hostPassword;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
