package com.gv.fmu.dto;

import com.gv.fmu.domain.FileMergingPattern;
import com.gv.fmu.domain.InputOutputSetting;
import com.gv.fmu.domain.MergingJobContext;
import com.gv.fmu.domain.MergingWorkFlow;
import lombok.Data;

/**
 * Created by Muhammad Yaseen on 5/8/2018.
 */

/*
    This is DTO of below mentioned classes
    InputOutputSetting
    FileMergingPattern
    MergingJobContext
 */

public class InputOutputSettingFileMergigPatternDTO
{
    private InputOutputSetting inputOutputSetting;
    private FileMergingPatternDTO fileMergingPattern;
    private MergingJobContext mergingJobContext;
    private MergingWorkFlow mergingWorkFlow;

    public InputOutputSetting getInputOutputSetting() {
        return inputOutputSetting;
    }

    public void setInputOutputSetting(InputOutputSetting inputOutputSetting) {
        this.inputOutputSetting = inputOutputSetting;
    }

    public FileMergingPatternDTO getFileMergingPattern() {
        return fileMergingPattern;
    }

    public void setFileMergingPattern(FileMergingPatternDTO fileMergingPattern) {
        this.fileMergingPattern = fileMergingPattern;
    }

    public MergingJobContext getMergingJobContext() {
        return mergingJobContext;
    }

    public void setMergingJobContext(MergingJobContext mergingJobContext) {
        this.mergingJobContext = mergingJobContext;
    }

    public MergingWorkFlow getMergingWorkFlow() {
        return mergingWorkFlow;
    }

    public void setMergingWorkFlow(MergingWorkFlow mergingWorkFlow) {
        this.mergingWorkFlow = mergingWorkFlow;
    }
}
