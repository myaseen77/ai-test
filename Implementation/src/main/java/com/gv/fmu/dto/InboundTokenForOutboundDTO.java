package com.gv.fmu.dto;

import lombok.Data;

/**
 * Created by Muhammad Yaseen on 7/6/2018.
 */

@Data
public class InboundTokenForOutboundDTO
{
    private Integer key;
    private String value;
}
