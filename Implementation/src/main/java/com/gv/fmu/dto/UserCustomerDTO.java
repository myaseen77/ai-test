package com.gv.fmu.dto;

import com.gv.fmu.domain.Customer;
import com.gv.fmu.domain.User;
import lombok.Data;

/**
 * Created by Muhammad Yaseen on 5/7/2018.
 */

public class UserCustomerDTO
{
    private User user;
    private Customer customer;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
