package com.gv.fmu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Muhammad Yaseen on 5/23/2018.
 */

@Entity
@Table(name = "merging_job_context")
@Transactional
public class MergingJobContext
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long mergingJobContextId;

    @Column(name = "custom_identifier")
    private String customIdentifier;

    @Column(name = "bundle_identifier")
    private String bundleIdentifier;

    @Column(name = "file_extension")
    private String fileExtension;

    @Column(name = "customer_id")
    private Long customerId;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "mergingJobContextId")
    private List<FileMergingPattern> fileMergingPattern;

    public MergingJobContext()
    {

    }

    public void setMergingJobContextId(Long mergingJobContextId) {
        this.mergingJobContextId = mergingJobContextId;
    }

    public String getCustomIdentifier() {
        return customIdentifier;
    }

    public void setCustomIdentifier(String customIdentifier) {
        this.customIdentifier = customIdentifier;
    }

    public String getBundleIdentifier() {
        return bundleIdentifier;
    }

    public void setBundleIdentifier(String bundleIdentifier) {
        this.bundleIdentifier = bundleIdentifier;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public List<FileMergingPattern> getFileMergingPattern() {
        return fileMergingPattern;
    }

    public void setFileMergingPattern(List<FileMergingPattern> fileMergingPattern) {
        this.fileMergingPattern = fileMergingPattern;
    }

    public Long getMergingJobContextId() {
        return mergingJobContextId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }
}
