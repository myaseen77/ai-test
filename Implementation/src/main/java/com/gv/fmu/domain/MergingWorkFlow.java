package com.gv.fmu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

/**
 * Created by Muhammad Yaseen on 5/14/2018.
 */

@Entity
@Table(name = "merging_workflow")
public class MergingWorkFlow
{
    @Id
    @Column(name = "workflow_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long workflowId;

    @Column(name = "merging_workflow_name")
    private String workflowName;

    @Column(name = "merging_actions")
    private Integer actions;

    @Column(name = "customer_id")
    private Long customerId;

    @OneToOne
    @JoinColumn(name = "mergingJobContextId")
    private MergingJobContext mergingJobContext;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "outboundId")
    @Fetch(FetchMode.SELECT)
    private OutBoundPattern outBoundPattern;

    @ManyToOne
    @JoinColumn(name = "userId", referencedColumnName = "userId")
    private User user;

    public MergingWorkFlow()
    {

    }

    public Long getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(Long workflowId) {
        this.workflowId = workflowId;
    }

    public String getWorkflowName()
    {
        return workflowName;
    }

    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }

    public Integer getActions() {
        return actions;
    }

    public void setActions(Integer actions) {
        this.actions = actions;
    }

    public MergingJobContext getMergingJobContext() {
        return mergingJobContext;
    }

    public void setMergingJobContext(MergingJobContext mergingJobContext)
    { this.mergingJobContext = mergingJobContext; }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public OutBoundPattern getOutBoundPattern()
    {
        return outBoundPattern;
    }

    public void setOutBoundPattern(OutBoundPattern outBoundPattern) {
        this.outBoundPattern = outBoundPattern;
    }

    public Long getCustomerId() { return customerId; }

    public void setCustomerId(Long customerId) { this.customerId = customerId; }
}

