package com.gv.fmu.domain;

import lombok.Data;

import javax.persistence.*;
/**
 * Created by Muhammad Yaseen on 5/23/2018.
 */

@Entity
@Table(name = "email_transport")
public class EmailTransport
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long emailTransportId;

    @Column(name = "email_server")
    private String emailServer;

    @Column(name = "authentication")
    private String authentication;

    @Column(name = "secure_connection")
    private String secureConnection;

    @Column(name = "email_username", unique = true)
    private String emailUserName;

    @Column(name = "email_password")
    private String emailPassword;

    @Column(name = "email_port")
    private Integer emailPort;

/*    @Column(name = "email_custom_tokens")
    private String emailCustomTokens;*/

    @OneToOne
    @JoinColumn(name = "customerId")
    private Customer customer;

    public EmailTransport()
    {

    }

    public Long getEmailTransportId() {
        return emailTransportId;
    }

    public void setEmailTransportId(Long emailTransportId) {
        this.emailTransportId = emailTransportId;
    }

    public String getEmailServer() {
        return emailServer;
    }

    public void setEmailServer(String emailServer) {
        this.emailServer = emailServer;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public String getSecureConnection() {
        return secureConnection;
    }

    public void setSecureConnection(String secureConnection) {
        this.secureConnection = secureConnection;
    }

    public String getEmailUserName() {
        return emailUserName;
    }

    public void setEmailUserName(String emailUserName) {
        this.emailUserName = emailUserName;
    }

    public String getEmailPassword() {
        return emailPassword;
    }

    public void setEmailPassword(String emailPassword) {
        this.emailPassword = emailPassword;
    }

    public Integer getEmailPort() {
        return emailPort;
    }

    public void setEmailPort(Integer emailPort) {
        this.emailPort = emailPort;
    }

/*    public String getEmailCustomTokens() {
        return emailCustomTokens;
    }

    public void setEmailCustomTokens(String emailCustomTokens) {
        this.emailCustomTokens = emailCustomTokens;
    }*/

    public Customer getCustomer() { return customer; }

    public void setCustomer(Customer customer) { this.customer = customer; }
}
