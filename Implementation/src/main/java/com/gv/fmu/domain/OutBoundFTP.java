package com.gv.fmu.domain;

import javax.persistence.*;

/**
 * Created by Muhammad Yaseen on 6/8/2018.
 */

@Entity
@Table(name = "outbound_ftp")
public class OutBoundFTP
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer outboundId;

/*    @Column(name = "outbound_ip_address")
    private String outboundIpAddress;

    @Column(name = "outbound_ftp_name")
    private String outboundFTPName;

    @Column(name = "outbound_host_userName")
    private String outboundHostUserName;

    @Column(name = "outbound_host_password")
    private String outboundHostPassword;

    @Column(name = "outbound_port")
    private Integer outboundPort;*/

    @Column(name = "outbound_merged_files_location")
    private String outboundDirectory;

    @Column(name = "outboundPatternId")
    private Integer outboundPatternId;

/*    @Column(name = "file_merging_pattern_id")
    private Long fileMergingPatternId;*/

    @Column(name = "file_transportid")
    private Integer fileTransportID;

/*    @Column(name = "file_name")
    private String fileName;*/

    @Column(name = "file_path")
    private String filePath;

    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "ftpName")
    private String outboundFTPName;

    public OutBoundFTP()
    {

    }

    public Integer getOutboundId() {
        return outboundId;
    }

    public void setOutboundId(Integer outboundId) {
        this.outboundId = outboundId;
    }

/*    public String getOutboundIpAddress() {
        return outboundIpAddress;
    }

    public void setOutboundIpAddress(String outboundIpAddress) {
        this.outboundIpAddress = outboundIpAddress;
    }

    public String getOutboundFTPName() {
        return outboundFTPName;
    }

    public void setOutboundFTPName(String outboundFTPName) {
        this.outboundFTPName = outboundFTPName;
    }

    public String getOutboundHostUserName() {
        return outboundHostUserName;
    }

    public void setOutboundHostUserName(String outboundHostUserName) {
        this.outboundHostUserName = outboundHostUserName;
    }

    public String getOutboundHostPassword() {
        return outboundHostPassword;
    }

    public void setOutboundHostPassword(String outboundHostPassword) {
        this.outboundHostPassword = outboundHostPassword;
    }

    public Integer getOutboundPort() {
        return outboundPort;
    }

    public void setOutboundPort(Integer outboundPort) {
        this.outboundPort = outboundPort;
    }*/

    public String getOutboundDirectory() {
        return outboundDirectory;
    }

    public void setOutboundDirectory(String outboundDirectory) {
        this.outboundDirectory = outboundDirectory;
    }

/*    public Integer getFileTransportID() { return fileTransportID; }

    public void setFileTransportID(Integer fileTransportID) { this.fileTransportID = fileTransportID; }*/

/*    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }*/

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Integer getOutboundPatternId() {
        return outboundPatternId;
    }

    public void setOutboundPatternId(Integer outboundPatternId) {
        this.outboundPatternId = outboundPatternId;
    }

/*    public Long getFileMergingPatternId() {
        return fileMergingPatternId;
    }

    public void setFileMergingPatternId(Long fileMergingPatternId) {
        this.fileMergingPatternId = fileMergingPatternId;
    }*/

    public Integer getFileTransportID() {
        return fileTransportID;
    }

    public void setFileTransportID(Integer fileTransportID) {
        this.fileTransportID = fileTransportID;
    }

    public String getOutboundFTPName() {
        return outboundFTPName;
    }

    public void setOutboundFTPName(String outboundFTPName) {
        this.outboundFTPName = outboundFTPName;
    }
}
