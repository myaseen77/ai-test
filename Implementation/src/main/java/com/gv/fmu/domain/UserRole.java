package com.gv.fmu.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by WhiztecTestingServer on 7/5/2018.
 */
@Entity
@Table(name = "user_role")
@Data
public class UserRole
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer userRoleId;

    @Column(name = "role")
    private String userRole;

}
