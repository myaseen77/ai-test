package com.gv.fmu.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import com.gv.fmu.dto.MergedReceivedFileContextDTO;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Muhammad Yaseen on 5/2/2018.
 */
@Entity
@Table(name = "merged_files")
public class MergedFiles
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long mergedFileId;

    @Column(name = "received_files")
    private String receivedFiles;

    @Column(name = "directory")
    private String mergedFile;

    @Column(name = "file_context")
    private String fileContext;

    @Column(name = "job_type")
    private String jobType;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd-MM-yyy")
    @Column(name = "timestamp")
    private Date localDate;

    @Column(name = "status")
    private String status;

    @Column(name = "customer_id")
    private Long customerId;

    public MergedFiles()
    {

    }

    public Long getMergedFileId() {
        return mergedFileId;
    }

    public void setMergedFileId(Long mergedFileId) {
        this.mergedFileId = mergedFileId;
    }

    public String getReceivedFiles() {
        return receivedFiles;
    }

    public void setReceivedFiles(String receivedFiles) {
        this.receivedFiles = receivedFiles;
    }

    public String getMergedFile() {
        return mergedFile;
    }

    public void setMergedFile(String mergedFile) {
        this.mergedFile = mergedFile;
    }

    public String getFileContext() {
        return fileContext;
    }

    public void setFileContext(String fileContext) {
        this.fileContext = fileContext;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public Date getLocalDate() {
        return localDate;
    }

    public void setLocalDate(Date localDate) {
        this.localDate = localDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }
}