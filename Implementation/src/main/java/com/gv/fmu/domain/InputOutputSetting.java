package com.gv.fmu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Muhammad Yaseen on 5/2/2018.
 */

@Entity
@Table(name = "ftp_transport")
public class InputOutputSetting
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer fileTransportID;

    @Column(name = "inbound_ip_address")
    private String inboundIpAddress;

    @Column(name = "ftp_name")
    private String ftpName;

    @Column(name = "host_userName")
    private String hostUserName;

    @Column(name = "host_password")
    private String hostPassword;

    @Column(name = "port")
    private Integer port;
/*
    @Column(name = "ftp_remote_directory")
    private String ftpRemoteDirectory;*/

    @Column(name = "ftp_local_directory")
    private String ftpLocalDirectory;

    @Column(name = "merged_files_location")
    private String mergedFilesLocation;

    @Column(name = "customer_id")
    private Long customerId;

    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "file_transportid")
    private List<OutBoundFTP> outBoundFTPs;

    public InputOutputSetting()
    {

    }

    public Integer getFileTransportID() {
        return fileTransportID;
    }

    public void setFileTransportID(Integer fileTransportID) {
        this.fileTransportID = fileTransportID;
    }

    public String getInboundIpAddress() {
        return inboundIpAddress;
    }

    public void setInboundIpAddress(String inboundIpAddress) {
        this.inboundIpAddress = inboundIpAddress;
    }

    public String getFtpName() {
        return ftpName;
    }

    public void setFtpName(String ftpName) {
        this.ftpName = ftpName;
    }

    public String getHostUserName() {
        return hostUserName;
    }

    public void setHostUserName(String hostUserName) {
        this.hostUserName = hostUserName;
    }

    public String getHostPassword() {
        return hostPassword;
    }

    public void setHostPassword(String hostPassword) {
        this.hostPassword = hostPassword;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

/*    public String getFtpRemoteDirectory() {
        return ftpRemoteDirectory;
    }

    public void setFtpRemoteDirectory(String ftpRemoteDirectory) {
        this.ftpRemoteDirectory = ftpRemoteDirectory;
    }*/

    public String getFtpLocalDirectory() {
        return ftpLocalDirectory;
    }

    public void setFtpLocalDirectory(String ftpLocalDirectory) {
        this.ftpLocalDirectory = ftpLocalDirectory;
    }

    public String getMergedFilesLocation() { return mergedFilesLocation; }

    public void setMergedFilesLocation(String mergedFilesLocation) { this.mergedFilesLocation = mergedFilesLocation; }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public List<OutBoundFTP> getOutBoundFTPs()
    { return outBoundFTPs; }

    public void setOutBoundFTPs(List<OutBoundFTP> outBoundFTPs)
    {
        this.outBoundFTPs = outBoundFTPs;
    }
}
