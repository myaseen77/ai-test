package com.gv.fmu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.transaction.Transactional;

/**
 * Created by Muhammad Yaseen on 5/2/2018.
 */

@Entity
@Table(name = "file_pattern")
@Transactional
public class FileMergingPattern
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long fileMergingPatternId;

    @Column(name = "inbound_custom_token", columnDefinition="TEXT")
    private String inboundCustomToken;

    @Column(name = "inbound_custom_identifier")
    private String inboundCustomerIdentifier;

    @Column(name = "inbound_custom_identifier_order")
    private Integer inboundCustomerIdentifierOrder;

    @Column(name = "inbound_document_identifier")
    private String inboundDocumentIdentifier;

    @Column(name = "inbound_document_identifier_order")
    private Integer inboundDocumentIdentifierOrder;

    @Column(name = "inbound_document_seprator")
    private String inboundDocumentSeprator;

    @Column(name = "inbound_package_identifier")
    private String inboundPackageIdentifier;

    @Column(name = "inbound_package_identifier_order")
    private Integer inboundPackageIdentifierOrder;

    @Column(name = "inbound_pattern_name")
    private String completePattern;

    @Column(name = "file_order")
    private Integer fileOrder;

    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "pattern_name")
    private String patternName;

    @Column(name = "ftp_remote_directory")
    private String ftpRemoteDirectory;

    @Column(name = "mergingJobContextId")
    private Long mergingJobContextId;

    @ManyToOne
    @JoinColumn(name = "fileTransportID", referencedColumnName = "fileTransportID")
    @JsonIgnore
    private InputOutputSetting inputOutputSetting;

    public FileMergingPattern()
    {

    }

    public Long getFileMergingPatternId() {
        return fileMergingPatternId;
    }

    public void setFileMergingPatternId(Long fileMergingPatternId) {
        this.fileMergingPatternId = fileMergingPatternId;
    }

    public String getInboundCustomToken() {
        return inboundCustomToken;
    }

    public void setInboundCustomToken(String inboundCustomToken) {
        this.inboundCustomToken = inboundCustomToken;
    }

    public String getInboundCustomerIdentifier() {
        return inboundCustomerIdentifier;
    }

    public void setInboundCustomerIdentifier(String inboundCustomerIdentifier) {
        this.inboundCustomerIdentifier = inboundCustomerIdentifier;
    }

    public String getInboundDocumentIdentifier() {
        return inboundDocumentIdentifier;
    }

    public void setInboundDocumentIdentifier(String inboundDocumentIdentifier) {
        this.inboundDocumentIdentifier = inboundDocumentIdentifier;
    }

    public String getInboundDocumentSeprator() {
        return inboundDocumentSeprator;
    }

    public void setInboundDocumentSeprator(String inboundDocumentSeprator) {
        this.inboundDocumentSeprator = inboundDocumentSeprator;
    }

    public String getInboundPackageIdentifier() {
        return inboundPackageIdentifier;
    }

    public void setInboundPackageIdentifier(String inboundPackageIdentifier) {
        this.inboundPackageIdentifier = inboundPackageIdentifier;
    }

    public String getCompletePattern() {
        return completePattern;
    }

    public void setCompletePattern(String completePattern) {
        this.completePattern = completePattern;
    }

    public InputOutputSetting getInputOutputSetting() {
        return inputOutputSetting;
    }

    public void setInputOutputSetting(InputOutputSetting inputOutputSetting) {
        this.inputOutputSetting = inputOutputSetting;
    }


    public String getPatternName() {
        return patternName;
    }

    public void setPatternName(String patternName) {
        this.patternName = patternName;
    }

    public Long getMergingJobContextId() {
        return mergingJobContextId;
    }

    public void setMergingJobContextId(Long mergingJobContextId) {
        this.mergingJobContextId = mergingJobContextId;
    }

    public Integer getInboundCustomerIdentifierOrder()
    {
        return inboundCustomerIdentifierOrder;
    }

    public void setInboundCustomerIdentifierOrder(Integer inboundCustomerIdentifierOrder) {
        this.inboundCustomerIdentifierOrder = inboundCustomerIdentifierOrder;
    }

    public Integer getInboundDocumentIdentifierOrder() {
        return inboundDocumentIdentifierOrder;
    }

    public void setInboundDocumentIdentifierOrder(Integer inboundDocumentIdentifierOrder) {
        this.inboundDocumentIdentifierOrder = inboundDocumentIdentifierOrder;
    }

    public Integer getInboundPackageIdentifierOrder() {
        return inboundPackageIdentifierOrder;
    }

    public void setInboundPackageIdentifierOrder(Integer inboundPackageIdentifierOrder) {
        this.inboundPackageIdentifierOrder = inboundPackageIdentifierOrder;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getFtpRemoteDirectory() {
        return ftpRemoteDirectory;
    }

    public void setFtpRemoteDirectory(String ftpRemoteDirectory) {
        this.ftpRemoteDirectory = ftpRemoteDirectory;
    }

    public Integer getFileOrder() {
        return fileOrder;
    }

    public void setFileOrder(Integer fileOrder) {
        this.fileOrder = fileOrder;
    }
}
