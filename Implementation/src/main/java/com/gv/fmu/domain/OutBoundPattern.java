package com.gv.fmu.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

/**
 * Created by Muhammad Yaseen on 6/7/2018.
 */

@Entity
@Table(name = "outbound_pattern")
public class OutBoundPattern
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer outboundPatternId;

    @Column(name = "outbound_custom_token", columnDefinition="TEXT")
    private String outboundCustomToken;

    @Column(name = "outbound_custom_identifier")
    private String outboundCustomIdentifier;

    @Column(name = "outbound_custom_identifier_order")
    private Integer outboundCustomIdentifierOrder;

    @Column(name = "outbound_document_identifier")
    private String outboundDocumentIdentifier;

    @Column(name = "outbound_document_identifier_order")
    private Integer outboundDocumentIdentifierOrder;

    @Column(name = "outbound_document_seprator")
    private String outboundDocumentSeprator;

    @Column(name = "outbound_package_identifier")
    private String outboundPackageIdentifier;

    @Column(name = "outbound_package_identifier_order")
    private Integer outboundPackageIdentifierOrder;

    @Column(name = "ounbound_pattern_name")
    private String outboundPatternName;

    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "email")
    private String email;

    @Column(name = "email_subject")
    private String emailSubject;

    @Column(name = "email_body")
    private String emailBody;


    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @Fetch(FetchMode.SELECT)
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "outboundPatternId")
    private List<OutBoundFTP> outBoundFTPs;

    public OutBoundPattern()
    {

    }

    public Integer getOutboundPatternId() {
        return outboundPatternId;
    }

    public void setOutboundPatternId(Integer outboundPatternId) {
        this.outboundPatternId = outboundPatternId;
    }

    public String getOutboundCustomToken() {
        return outboundCustomToken;
    }

    public void setOutboundCustomToken(String outboundCustomToken) {
        this.outboundCustomToken = outboundCustomToken;
    }

    public String getOutboundCustomIdentifier() {
        return outboundCustomIdentifier;
    }

    public void setOutboundCustomIdentifier(String outboundCustomIdentifier) {
        this.outboundCustomIdentifier = outboundCustomIdentifier;
    }

    public Integer getOutboundCustomIdentifierOrder() {
        return outboundCustomIdentifierOrder;
    }

    public void setOutboundCustomIdentifierOrder(Integer outboundCustomIdentifierOrder) {
        this.outboundCustomIdentifierOrder = outboundCustomIdentifierOrder;
    }

    public String getOutboundDocumentIdentifier() {
        return outboundDocumentIdentifier;
    }

    public void setOutboundDocumentIdentifier(String outboundDocumentIdentifier) {
        this.outboundDocumentIdentifier = outboundDocumentIdentifier;
    }

    public Integer getOutboundDocumentIdentifierOrder() {
        return outboundDocumentIdentifierOrder;
    }

    public void setOutboundDocumentIdentifierOrder(Integer outboundDocumentIdentifierOrder) {
        this.outboundDocumentIdentifierOrder = outboundDocumentIdentifierOrder;
    }

    public String getOutboundDocumentSeprator() {
        return outboundDocumentSeprator;
    }

    public void setOutboundDocumentSeprator(String outboundDocumentSeprator) {
        this.outboundDocumentSeprator = outboundDocumentSeprator;
    }

    public String getOutboundPackageIdentifier() {
        return outboundPackageIdentifier;
    }

    public void setOutboundPackageIdentifier(String outboundPackageIdentifier) {
        this.outboundPackageIdentifier = outboundPackageIdentifier;
    }

    public Integer getOutboundPackageIdentifierOrder() {
        return outboundPackageIdentifierOrder;
    }

    public void setOutboundPackageIdentifierOrder(Integer outboundPackageIdentifierOrder) {
        this.outboundPackageIdentifierOrder = outboundPackageIdentifierOrder;
    }

    public String getOutboundPatternName() {
        return outboundPatternName;
    }

    public void setOutboundPatternName(String outboundPatternName) {
        this.outboundPatternName = outboundPatternName;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<OutBoundFTP> getOutBoundFTPs() {
        return outBoundFTPs;
    }

    public void setOutBoundFTPs(List<OutBoundFTP> outBoundFTPs) {
        this.outBoundFTPs = outBoundFTPs;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }
}