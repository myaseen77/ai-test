package com.gv.fmu.connection.setting;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.springframework.stereotype.Service;

import java.util.Properties;

/**
 * Created by Muhammad Yaseen on 6/12/2018.
 */
@Service
public class OutboundSFTPOperations
{
    private JSch mJSchSession;
    private Session mSHHSession;
    private ChannelSftp mChannelSFTP;

    public Boolean connectChannel(String ipAddress, int port, String userName, String password)
    {
        boolean result = false;
        try
        {
            this.mJSchSession = new JSch();
            java.util.Properties properties = new Properties();
            properties.put("StrictHostKeyChecking","no");

            this.mJSchSession.setConfig(properties);

            this.mSHHSession = mJSchSession.getSession(userName, ipAddress, port);
            this.mSHHSession.setPassword(password);
            this.mSHHSession.connect();
            this.mChannelSFTP = (ChannelSftp) this.mSHHSession.openChannel("sftp");
            this.mChannelSFTP.connect();
            if(this.mChannelSFTP != null)
                 result = true;
        }
        catch (JSchException e)
        {
            e.printStackTrace();
        }
        return result;
    }

    public Boolean uploadFiles(String source, String destination)
    {
        boolean result = false;
        try
        {
            this.mChannelSFTP.put(source, destination);
            result = true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }

    public Boolean disConnectChannel()
    {
        boolean result = false;
        try
        {
            this.mChannelSFTP.disconnect();
            result  = true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }
}
