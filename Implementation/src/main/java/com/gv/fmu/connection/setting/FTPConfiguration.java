package com.gv.fmu.connection.setting;


import com.gv.fmu.controller.MergedFilesController;
import com.gv.fmu.controller.MergingWorkflowController;
import com.gv.fmu.controller.UserController;
import com.gv.fmu.domain.MergingWorkFlow;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import javax.annotation.PostConstruct;
import java.io.File;


@Configuration
public class FTPConfiguration
{

    @Value("${local.ftp.tempMerged.storage.basePath}")
    private String basePath;

    @Value("${local.ftp.tempMerged.storage.path}")
    private String tempMergedFilesPath;

    @PostConstruct
    public void init()
    {

    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public String getTempMergedFilesPath() {
        return tempMergedFilesPath;
    }

    public void setTempMergedFilesPath(String tempMergedFilesPath) {
        this.tempMergedFilesPath = tempMergedFilesPath;
    }
}

