package com.gv.fmu.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.reflect.TypeToken;
import com.google.gson.*;
import com.gv.fmu.connection.setting.FTPConfiguration;
import com.gv.fmu.connection.setting.OutboundSFTPOperations;
import com.gv.fmu.domain.EmailTransport;
import com.gv.fmu.domain.MergedFiles;
import com.gv.fmu.domain.MergingWorkFlow;
import com.gv.fmu.domain.OutBoundFTP;
import com.gv.fmu.dto.*;
import com.gv.fmu.ftp.listener.*;
import com.gv.fmu.ftp.listener.FTPTransportService;
import com.gv.fmu.merger.PDFMerger;
import com.gv.fmu.repository.*;
import com.gv.fmu.util.HibernateProxyTypeAdapter;
import com.lowagie.text.DocumentException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by Muhammad Yaseen on 5/2/2018.
 */

@SuppressWarnings("ALL")
@RestController
@RequestMapping("/mergedFiles")
@Service("mergedFilesController")
public class MergedFilesController
{
     static Cache<String, Map<String, FileInfo>> cache = CacheBuilder.newBuilder().maximumSize(1000).build();
     static FTPConfiguration ftpConfiguration;
     static Logger LOGGER = LoggerFactory.getLogger(MergedFilesController.class);
     static MergedFilesDAO objMergedFilesQuery;
     static UserController userController;
     static EmailTransportDAO emailTransportQuery;
     static CustomerDAO customerRepositoryQuery;
     static MergingWorkflowDAO mergingWorkflowQuery;
     static FileMergingPatternDAO fileMergePatternQuery;
     static InputOutputSettingDAO inputOutputSettingQuery;
     static MergingJobContextDAO mergingJobContextQuery;
     static OutboundFTPDAO outboundFTPQuery;
     static DynamicOutboundFTPChannelResolver dynamicOutboundFTPChannelResolver;
     static ApplicationContext applicationContext;
     static FTPTransportService ftpTransportService;
     static OutboundSFTPOperations sftpOperations;
     static DynamicInboundFTPChannelResolver dynamicInboundFTPChannelResolver;
     static FileMergingPatternDAO fileMergingPatternDAO;
     static String emailBody = "";
     static String emailSubject = "";

    private final String INVALID_USER = "Invalid User";
    @Autowired
    private UserRepositoryDAO userRepositoryDAO;


    @Autowired
    public void setMergedFilesQuery(MergedFilesDAO objMergedFilesQuery,
                                    UserController userController,
                                    EmailTransportDAO emailTransportQuery,
                                    CustomerDAO customerRepositoryQuery,
                                    MergingWorkflowDAO mergingWorkflowQuery,
                                    OutboundFTPDAO outboundFTPQuery,
                                    DynamicOutboundFTPChannelResolver dynamicOutboundFTPChannelResolver,
                                    ApplicationContext applicationContext,
                                    FTPTransportService ftpTransportService,
                                    OutboundSFTPOperations sftpOperations,
                                    FileMergingPatternDAO fileMergePatternQuery,
                                    InputOutputSettingDAO inputOutputSettingQuery,
                                    FTPConfiguration ftpConfiguration,
                                    DynamicInboundFTPChannelResolver dynamicInboundFTPChannelResolver,
                                    FileMergingPatternDAO fileMergingPatternDAO)
    {
        MergedFilesController.objMergedFilesQuery = objMergedFilesQuery;
        MergedFilesController.userController = userController;
        MergedFilesController.emailTransportQuery = emailTransportQuery;
        MergedFilesController.customerRepositoryQuery = customerRepositoryQuery;
        MergedFilesController.mergingWorkflowQuery = mergingWorkflowQuery;
        MergedFilesController.outboundFTPQuery = outboundFTPQuery;
        MergedFilesController.dynamicOutboundFTPChannelResolver = dynamicOutboundFTPChannelResolver;
        MergedFilesController.applicationContext = applicationContext;
        MergedFilesController.ftpTransportService = ftpTransportService;
        MergedFilesController.sftpOperations = sftpOperations;
        MergedFilesController.fileMergePatternQuery = fileMergePatternQuery;
        MergedFilesController.inputOutputSettingQuery = inputOutputSettingQuery;
        MergedFilesController.ftpConfiguration = ftpConfiguration;
        MergedFilesController.dynamicInboundFTPChannelResolver = dynamicInboundFTPChannelResolver;
        MergedFilesController.fileMergingPatternDAO = fileMergingPatternDAO;
    }

    public static String saveMergedFiles(File fileName, @Header(value = "workflowId")Long workflowId) throws IOException
    {
        MergingWorkFlow mergingWorkFlow = mergingWorkflowQuery.findOne(workflowId);
        List<String> getFileSequenceFromDB = new ArrayList<>();
        List<String> getFileSepratorFromDB = new ArrayList<>();
        List<MergedReceivedFileContextDTO> nameWithIdentifier = new ArrayList<>();
        Map<Integer, String> fileOrder = new HashMap<>();

        Path path = Paths.get(ftpConfiguration.getBasePath(), mergingWorkFlow.getUser().getCustomer().getCustomerId() + "", "temp");
        String tempFolder = path.toString();
        Path path1 = Paths.get(ftpConfiguration.getBasePath(), mergingWorkFlow.getUser().getCustomer().getCustomerId() + "", "merged");
        String tempMergedFolder = path1.toString();
        Path path2 = Paths.get(ftpConfiguration.getBasePath(), mergingWorkFlow.getUser().getCustomer().getCustomerId() + "", "invalidFiles");
        String invalidFiles = path2.toString();
        Path path3 = Paths.get(ftpConfiguration.getBasePath(), mergingWorkFlow.getUser().getCustomer().getCustomerId() + "", "invalidImporter");
        String invalidImpoterType = path3.toString();
        File dir = null;

        for(int j = 0; j < mergingWorkFlow.getMergingJobContext().getFileMergingPattern().size(); j++)
        {
            getFileSequenceFromDB.add(mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(j).getInboundDocumentIdentifier());
            getFileSepratorFromDB.add(mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(j).getInboundDocumentSeprator());
            fileOrder.put(mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(j).getFileOrder(), mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(j).getInboundDocumentIdentifier());
        }


        for(int x = 0; x < mergingWorkFlow.getMergingJobContext().getFileMergingPattern().size(); x++)
        {
            MergedReceivedFileContextDTO mergedReceivedFileContextDTO = new MergedReceivedFileContextDTO();

            // fields configuration save
            List<String> fileNameDataList = getFileNameAttributes(fileName.getName(), getFileSepratorFromDB.get(x));
            Map<String, FileInfo> files = new HashMap<String, FileInfo>();
            Map<String, FileInfo> newMap = new TreeMap<String, FileInfo>(String.CASE_INSENSITIVE_ORDER);
            if (CollectionUtils.isNotEmpty(fileNameDataList))
            {
                int inboundPackageIdentifierOrderPosition = mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(x).getInboundPackageIdentifierOrder();
                String[] fileNameLiterals = fileName.getName().split(mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(x).getInboundDocumentSeprator());
                String packageIdentifier = "";
                packageIdentifier = fileNameLiterals[inboundPackageIdentifierOrderPosition];

                String setIdentifier = mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(x).getInboundCustomerIdentifier() +
                        mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(x).getInboundDocumentSeprator()
                        + packageIdentifier;


                System.out.println("Finding file with set of identifier = " + setIdentifier);
                if(cache.asMap().get(setIdentifier) != null)
                {
                    files = cache.getIfPresent(setIdentifier);

                    if (files != null && CollectionUtils.isNotEmpty(files.keySet()))
                    {
                        // Already there is a file
                        FileInfo fileInfo = new FileInfo();
                        fileInfo.setFile(fileName);
                        fileInfo.setFileName(fileName.getName());
                        fileInfo.setFileNameAttributes(fileNameDataList);
                        fileNameDataList.add(fileInfo.getFileName());
                        String fileType  = mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(x).getInboundDocumentIdentifier();

                        //check in file if file contain customer identifier than process it
                        if(fileNameDataList.contains(mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(x).getInboundCustomerIdentifier())
                            /*&& fileNameDataList.contains(packageIdentifier)*/)
                        {
                            for(String temp : fileNameDataList)
                            {
                                if (temp.equalsIgnoreCase(fileType))
                                {
                                    if (getFileSequenceFromDB.get(x).equals(fileType))
                                    {
                                        files.put(fileType, fileInfo);
                                        newMap.putAll(files);
                                        invalidFile(fileName, tempFolder, "");
                                        fileName.delete();
                                    }
                                }
                            }
                        }
                        else
                        {
                            String message = "Warning : Importer not found for the file";
                            invalidFile(fileName, invalidImpoterType, message);
                            System.out.println(fileName.getName() + ": File has been deleted successfully because it doesn't contain any matching pattern");
                            fileName.delete();
                        }

                        System.out.println("\nCurrent File Set Size = " + files.keySet().size() + " == Required == " + getFileSequenceFromDB.size());

                        List<String> filesNames = new ArrayList<>();

                        if (files.keySet().size() == getFileSequenceFromDB.size())
                        {
                            for (String fileSeq : getFileSequenceFromDB)
                            {
                                for (Map.Entry<Integer, String> order : fileOrder.entrySet())
                                {
                                    if (order.getValue() == fileSeq)
                                    {
                                        FileInfo info = files.get(order.getValue());
                                        filesNames.add(info.getFile().getAbsolutePath());
                                        mergedReceivedFileContextDTO = new MergedReceivedFileContextDTO();
                                        mergedReceivedFileContextDTO.setFilename("inbound");
                                        mergedReceivedFileContextDTO.setFilePath(info.getFileName());
                                        nameWithIdentifier.add(mergedReceivedFileContextDTO);
                                    }
                                }
                            }

                            String finalFileName = new String();
                            String finalMergedFileName = new String();
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                            if(finalFileName == null)
                            {
                                finalFileName = finalFileName.concat(mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(x).getInboundDocumentSeprator() + sdf.format(new Date()));
                            }
                            else
                            {
                                ObjectMapper objectMapper = new ObjectMapper();
                                objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
                                GsonBuilder b = new GsonBuilder();
                                b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
                                Gson gson = b.create();

                                finalFileName =  mergingWorkFlow.getOutBoundPattern().getOutboundPatternName();
                                List<String> outboundPatterns = getFileNameAttributes(finalFileName, getFileSepratorFromDB.get(x));

                                List<InboundCustomTokens> inboundCustomTokenses = new ArrayList<>();
                                Type objType = new TypeToken<List<InboundCustomTokens>>() {}.getType();
                                List<List<InboundCustomTokens>> inboundTokensList = new ArrayList<>();

                                List<OutboundCustomTokens> outboundCustomTokens = new ArrayList<>();
                                List<OutboundCustomTokens> getOutboundCustomTokensFromDB = new ArrayList<>();
                                Type objType1 = new TypeToken<List<OutboundCustomTokens>>() {}.getType();

                                List<FileInfo> tempFile = new ArrayList<>();
                                List<String> tokenizer = new ArrayList<>();
                                List<List<String>> filesNameDataList = new ArrayList<>();
                                List<String> singleFileNameDataList = new ArrayList<>();

                                List<String> tempFiles = fileMergingPatternDAO.getAllTokensByWorkflowId(String.valueOf(mergingWorkFlow.getWorkflowId()));

                                for (String tokenSet : tempFiles)
                                {
                                    if(!tokenSet.isEmpty())
                                        inboundTokensList.add(new Gson().fromJson(tokenSet, objType));
                                }

                                for (List<InboundCustomTokens> customTokenses : inboundTokensList)
                                {
                                    for (InboundCustomTokens tokens : customTokenses)
                                    {
                                        if(!inboundCustomTokenses.contains(tokens))
                                            inboundCustomTokenses.add(tokens);
                                    }
                                }

                                //outbound temprory List
                                OutboundCustomTokens outboundCustomTokens1 = new OutboundCustomTokens();
                                outboundCustomTokens1.setCustomValue(mergingWorkFlow.getOutBoundPattern().getOutboundCustomIdentifier());
                                outboundCustomTokens1.setCustomOrder(mergingWorkFlow.getOutBoundPattern().getOutboundCustomIdentifierOrder());
                                outboundCustomTokens1.setUuid("CST");
                                outboundCustomTokens1.setCustomKey("Customer Identifier");
                                outboundCustomTokens1.setDocumentIdentifier(mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(0).getInboundDocumentIdentifier());
                                outboundCustomTokens.add(outboundCustomTokens1);

                                outboundCustomTokens1 = new OutboundCustomTokens();
                                outboundCustomTokens1.setCustomValue(mergingWorkFlow.getOutBoundPattern().getOutboundPackageIdentifier());
                                outboundCustomTokens1.setCustomOrder(mergingWorkFlow.getOutBoundPattern().getOutboundPackageIdentifierOrder());
                                outboundCustomTokens1.setUuid("PKD");
                                outboundCustomTokens1.setCustomKey("Package Identifier");
                                outboundCustomTokens1.setDocumentIdentifier(mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(0).getInboundDocumentIdentifier());
                                outboundCustomTokens.add(outboundCustomTokens1);


                                gson = new Gson();
                                String founderJson = mergingWorkFlow.getOutBoundPattern().getOutboundCustomToken();
                                OutboundCustomTokens[] founderArray = gson.fromJson(founderJson, OutboundCustomTokens[].class);
                                emailBody = mergingWorkFlow.getOutBoundPattern().getEmailBody().toLowerCase();
                                emailSubject = mergingWorkFlow.getOutBoundPattern().getEmailSubject().toLowerCase();

                                List<List<OutboundCustomTokens>> tokensList = new ArrayList<>();

                                for (String tokenSet : tempFiles)
                                {
                                    if(!tokenSet.isEmpty())
                                        tokensList.add(new Gson().fromJson(tokenSet, objType1));
                                }

                                for (List<OutboundCustomTokens> customTokenses : tokensList)
                                {
                                    for (OutboundCustomTokens tokens : customTokenses)
                                    {
                                        if (!outboundCustomTokens.contains(tokens))
                                            outboundCustomTokens.add(tokens);
                                    }
                                }

                                for(OutboundCustomTokens tokens : founderArray)
                                {
                                    if (!outboundCustomTokens.contains(tokens))
                                        outboundCustomTokens.add(tokens);

                                }

                                //get files from momery
                                for(Map.Entry<String, FileInfo> temp : files.entrySet())
                                {
                                    tempFile.add(temp.getValue());
                                }

                                //add fils to tokenizer List
                                for(FileInfo file : tempFile)
                                {
                                    if (!tokenizer.contains(file))
                                        tokenizer.add(file.getFile().getName());
                                }
                            /*
                                TODO: Convert string into list of object while saving outbound patternName & on frontend convert from list to string again.
                             */

                                //making file name
                                String[] outboundName = mergingWorkFlow.getOutBoundPattern().getOutboundPatternName().split(mergingWorkFlow.getOutBoundPattern().getOutboundDocumentSeprator());
                                List<String> name = new ArrayList<>();
                                int counter = 0;
                                for (String te : outboundName)
                                {
                                    for(OutboundCustomTokens token : outboundCustomTokens)
                                    {
                                        if(token.getDocumentIdentifier() == null)
                                        {
                                            if(te.equals(token.getCustomValue()) && !token.getCustomValue().contains("("))
                                            {
                                                name.add(token.getCustomValue());
                                            }
                                        }
                                        else if(te.equals(token.getCustomValue()))
                                        {
                                            int inboundValuePos = 0;
                                            inboundValuePos = token.getCustomOrder();
                                            String target = files.get(token.getDocumentIdentifier()).getFileNameAttributes().get(inboundValuePos).replace(".PDF", "");
                                            name.add(inboundValuePos, target);
                                        }
                                        else if(te.indexOf("(") != -1)
                                        {
                                            if(te.substring(0, te.indexOf("(")).equalsIgnoreCase(token.getCustomValue()) && te.substring(te.indexOf("(") + 1, te.lastIndexOf(")")).equals(token.getDocumentIdentifier()))
                                            {
                                                int inboundValuePos = 0;
                                                inboundValuePos = token.getCustomOrder();
                                                String target = files.get(token.getDocumentIdentifier()).getFileNameAttributes().get(inboundValuePos).replace(".PDF", "");
                                                name.add(inboundValuePos, target);
                                            }
                                        }
                                    }
                                    counter++;
                                }

                                for(OutboundCustomTokens token : outboundCustomTokens)
                                {
                                    if(token.getDocumentIdentifier() != null)
                                    {
                                        int inboundValuePos = 0;
                                        inboundValuePos = token.getCustomOrder();
                                        String target = files.get(token.getDocumentIdentifier()).getFileNameAttributes().get(inboundValuePos).replace(".PDF", "");
                                        StringBuffer findStringInEmailBody = new StringBuffer("|*".concat(token.getCustomValue()).concat("(").concat(token.getDocumentIdentifier()).concat(")").concat("*|").toLowerCase());
                                        emailBody = emailBody.toLowerCase().replace(findStringInEmailBody, target);
                                        emailSubject = emailSubject.replace(findStringInEmailBody, target);
                                    }
                                }

                                for (String extract : name)
                                {
                                    finalMergedFileName = finalMergedFileName + extract + mergingWorkFlow.getOutBoundPattern().getOutboundDocumentSeprator();
                                }
                                finalMergedFileName = finalMergedFileName.concat(sdf.format(new Date()));
                            }

                            System.out.println("--------------------------------------------" + finalMergedFileName);
                            File makeMergedDirectory = new File(tempMergedFolder);
                            System.out.println(makeMergedDirectory);
                            String mergedDirectory = new String();
                            makeMergedDirectory(tempMergedFolder);

                            List<InputStream> list = new ArrayList<>();
                            if(list.size() == 0)
                            {
                                File folder = new File(tempFolder);
                                File[] listOfFiles = folder.listFiles();
                                for (Map.Entry<Integer, String> filesss : fileOrder.entrySet())
                                {
                                    for(File fils : listOfFiles)
                                    {
                                        String smalFils = fils.getName().toLowerCase();
                                        String smalFilesss = filesss.getValue().toLowerCase();
                                        if(smalFils.contains(smalFilesss))
                                        {
                                            if(!list.add(new FileInputStream(new File(fils.getAbsoluteFile().toString()))))
                                                list.add(new FileInputStream(new File(fils.getAbsoluteFile().toString())));
                                        }
                                    }
                                }
                            }

                            File file = null;
                            OutputStream out = new FileOutputStream(new File(Paths.get(tempMergedFolder).toString().concat("/").concat(finalMergedFileName).concat(".pdf")));
                            try
                            {
                                PDFMerger.doMerge(list, out);
                                out.flush();
                                out.close();
                                file = new File((Paths.get(tempMergedFolder).toString().concat("/").concat(finalMergedFileName).concat(".pdf")));
                            }
                            catch (DocumentException e)
                            {
                                LOGGER.info("Error while merging document(s)", e.getMessage());
                                e.printStackTrace();
                            }

                            System.out.print("Merged File Name: " + file.getName());

                            try
                            {
                                //Adding merge file record into db...
                                LocalDateTime now = LocalDateTime.now();
                                MergedFiles mergedFiles = new MergedFiles();
                                String jobType = "Merging Job";
                                String status = "1";
                                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                                Date date = new Date();
                                dateFormat.format(date);
                                mergedFiles.setReceivedFiles(new Gson().toJson(nameWithIdentifier));
                                mergedFiles.setMergedFile(file.getName());
                                mergedFiles.setJobType(jobType);
                                mergedFiles.setLocalDate(date);
                                mergedFiles.setStatus(status);
                                mergedFiles.setCustomerId(mergingWorkFlow.getCustomerId());
                                LOGGER.info("Merged File record is saved: " + objMergedFilesQuery.save(mergedFiles));
                                objMergedFilesQuery.save(mergedFiles);
                                System.out.println("\n\n\n\n\n\n\n\n\n\n...Merged Files Log has been saved");

                                //send file to mutiple FTP Outbound
                                List<OutBoundFTP> tempFiles = outboundFTPQuery.getOutboundFTPByTransportId(mergingWorkFlow.getOutBoundPattern().getOutboundPatternId());
                                for(OutBoundFTP temp : tempFiles)
                                {
                                    OutBoundFTP outBoundFTP = outboundFTPQuery.findOne(temp.getOutboundId());

                                    if(outBoundFTP != null)
                                    {
                                        dynamicOutboundFTPChannelResolver.reloadChannel(temp.getOutboundId());
                                        dynamicOutboundFTPChannelResolver.send(outBoundFTP, file);

                                        String subjectLine = mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(x).getInboundCustomerIdentifier().concat(" ").concat(emailSubject.toUpperCase());
                                        String messageBody = emailBody.toUpperCase();

                                        //sending email
                                        UserRepositoryDAO userRepositoryQuery = null;
                                        UserController userController = new UserController(userRepositoryQuery);
                                        EmailController emailController = new EmailController(userController);
                                        EmailTransport emailTransport = emailTransportQuery.getEmailTransportChannel(mergingWorkFlow.getCustomerId());

                                        //"smtp.gmail.com
                                        emailController.sendEmail(emailTransport.getEmailServer(), emailTransport.getEmailUserName(), emailTransport.getEmailPassword(),
                                                emailTransport.getEmailPort(), emailTransport.getEmailUserName(), mergingWorkFlow.getOutBoundPattern().getEmail(),//customerRepositoryQuery.findOne(mergingWorkFlow.getUser().getCustomer().getCustomerId()).getCustomerEmail(), mergingWorkFlow.getOutBoundPattern().getEmail(),
                                                file.toString(), subjectLine, messageBody);

                                        LOGGER.info("Merged File is email successfully");
                                        cache.invalidate(setIdentifier);
                                        cache.cleanUp();
                                        fileName.delete();
                                        list.clear();
                                        nameWithIdentifier.clear();

                                        //delete files from temp folder
                                        try
                                        {
                                            System.gc();
                                            Thread.sleep(2000);
                                            File folder = new File(tempFolder);
                                            File[] listOfFiles = folder.listFiles();

                                            for (Map.Entry<Integer, String> filesss : fileOrder.entrySet())
                                            {
                                                for(File fils : listOfFiles)
                                                {
                                                    String smalFils = fils.toString();
                                                    String smalFilesss = filesss.getValue();
                                                    if(smalFils.contains(smalFilesss))
                                                    {
                                                        FileDeleteStrategy.FORCE.delete(new File(smalFils));
                                                        LOGGER.info("Deleteing file from ".concat(new File(smalFils).getAbsolutePath().toString()));
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            e.printStackTrace();
                                        }
                                    }
                                    else if(outBoundFTP != null)
                                    {
                                        Boolean checkConnection = false;

                                        if(checkConnection)
                                        {
                                            String subjectLine = mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(0).getInboundDocumentIdentifier().concat(" ").concat(emailSubject.toUpperCase());
                                            String messageBody = emailBody.toUpperCase();

                                            //sending email
                                            UserRepositoryDAO userRepositoryQuery = null;
                                            UserController userController = new UserController(userRepositoryQuery);
                                            EmailController emailController = new EmailController(userController);

                                            EmailTransport emailTransport = emailTransportQuery.getEmailTransportChannel(mergingWorkFlow.getCustomerId());

                                            //"smtp.gmail.com
                                            emailController.sendEmail(emailTransport.getEmailServer(), emailTransport.getEmailUserName(), emailTransport.getEmailPassword(),
                                                    emailTransport.getEmailPort(), customerRepositoryQuery.findOne(mergingWorkFlow.getUser().getCustomer().getCustomerId()).getCustomerEmail(), mergingWorkFlow.getOutBoundPattern().getEmail(),
                                                    file.toString(), subjectLine, messageBody);
                                            LOGGER.info("Merged File is email successfully");
                                        }
                                        else
                                        {
//                                        System.out.println("Failed to connected Channel with ip: " + outBoundFTP.getOutboundIpAddress());
                                        }

                                        Boolean checkFilesTransfer = false;
                                        checkFilesTransfer = sftpOperations.uploadFiles(outBoundFTP.getFilePath(), outBoundFTP.getOutboundDirectory());

                                        if(checkFilesTransfer)
                                        {
                                            LOGGER.info("Upload file to Outbound FTP" + sftpOperations.uploadFiles(outBoundFTP.getFilePath(), outBoundFTP.getOutboundDirectory()));
//                                        System.out.println(outBoundFTP.getFileName() + " file is transfered successfully");
                                            FileUtils.cleanDirectory(new File(String.valueOf(mergedDirectory)));
                                        }
                                        else
                                        {
//                                        LOGGER.info("Failed to transfer file with name" + outBoundFTP.getOutboundIpAddress());
//                                        System.out.println("Failed to transfer file with name: " + outBoundFTP.getOutboundIpAddress());
                                        }
                                        Boolean checkDisconnected = sftpOperations.disConnectChannel();

                                        if(checkDisconnected)
                                        {
//                                        LOGGER.info("Uploaded file to Outbound FTP & Now Disconnected" + outBoundFTP.getOutboundIpAddress());
//                                        System.out.println("Disconnected Channel with ip: " + outBoundFTP.getOutboundIpAddress() + " after completing transaction");
                                        }
                                        else
                                        {
//                                        LOGGER.info("Uploaded file to Outbound FTP Failed & Now Connected" + outBoundFTP.getOutboundIpAddress());
//                                        System.out.println("Channel with ip: " + outBoundFTP.getOutboundIpAddress() + " is not closed");
                                        }
                                    }
                                    else
                                    {
                                        System.out.println("\n\ninvlaid connection port");
                                    }
                                    dynamicOutboundFTPChannelResolver.stopChannel(temp.getOutboundId());
                                }
                                cache.invalidate(setIdentifier);
                                new File(tempMergedFolder).delete();
                                file.delete();
                                break;
                            }
                            catch (Exception e)
                            {
                                MergedFiles mergedFiles = new MergedFiles();
                                String jobType = "Merging Job";
                                String status = "0";
                                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                                Date date = new Date();
                                dateFormat.format(date);
                                mergedFiles.setReceivedFiles(new Gson().toJson(nameWithIdentifier));
                                mergedFiles.setMergedFile(file.getName());
                                mergedFiles.setJobType(jobType);
                                mergedFiles.setLocalDate(date);
                                mergedFiles.setStatus(status);
                                mergedFiles.setCustomerId(mergingWorkFlow.getCustomerId());
                                LOGGER.info("Merged File record is saved: " + objMergedFilesQuery.save(mergedFiles));
                                objMergedFilesQuery.save(mergedFiles);
                                e.printStackTrace();
                            }

                            System.out.println("\n\n File merged successfully...");

                            cache.invalidate(setIdentifier);
                        }
                        else
                        {
                            System.out.println("\nRequired File Not found moving to next one");
                        }
                    }
                }
                else
                {
                    files = new HashMap<>();
                    FileInfo fileInfo = new FileInfo();
                    fileInfo.setFile(fileName);
                    fileInfo.setFileName(fileName.getName());
                    fileInfo.setFileNameAttributes(fileNameDataList);
                    mergedReceivedFileContextDTO.setFilename("inbound");
                    mergedReceivedFileContextDTO.setFilePath(fileInfo.getFileName());
                    nameWithIdentifier.add(mergedReceivedFileContextDTO);

                    String fileType = mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(x).getInboundDocumentIdentifier();

                   /*
                        check in file if file contain customer identifier than process it
                    */
                    String[] mactes = setIdentifier.split(mergingWorkFlow.getMergingJobContext().getFileMergingPattern().get(x).getInboundDocumentSeprator());
                    if(fileNameDataList.contains(mactes[0]) && fileNameDataList.contains(mactes[1]))
                    {
                        for (String temp : fileNameDataList)
                        {
                            if (temp.equalsIgnoreCase(fileType))
                            {
                                if(getFileSequenceFromDB.get(x).equalsIgnoreCase(fileType))
                                {
                                    files.put(fileType, fileInfo);
                                    cache.getIfPresent(setIdentifier);
                                    cache.put(setIdentifier, files);
                                    newMap.putAll(files);
                                    invalidFile(fileName, tempFolder, "");
                                    fileName.delete();
                                    break;
                                }
                            }
                        }
                    }
                    else if(fileNameDataList.contains(mactes[0]) == false)
                    {
                        String message = "Warning : Importer not found for the file";
                        invalidFile(fileName, invalidImpoterType, message);
                        System.out.println(fileName.getName() + ": File has been deleted successfully because it doesn't contain any matching pattern");
                        fileName.delete();
                        break;
                    }
                    else
                    {


                    }
                }


            }
        }
        return "";
    }

    private static ArrayList<String> getFileNameAttributes(String fileName, String seprator) {
        StringTokenizer stringTokenizer = new StringTokenizer(fileName, seprator);

        ArrayList<String> fileAttributes = new ArrayList<>();

        while (stringTokenizer.hasMoreTokens()) {
            fileAttributes.add(stringTokenizer.nextToken());
        }

        return fileAttributes;

    }

    //Get merged transcation by id

    public static void invalidFile(File ftpFile, String path, String message) {
        File makeMergedDirectory = new File(path);

        if (makeMergedDirectory.exists()) {
            try {
                FileUtils.copyFileToDirectory(ftpFile.getAbsoluteFile(), makeMergedDirectory);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            makeMergedDirectory.mkdir();
            try {

                FileUtils.copyFileToDirectory(ftpFile.getAbsoluteFile(), makeMergedDirectory);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n  ====  " + message + "       ================\n");
        LOGGER.info("\n  ====  " + message + "       ================\n");
    }

    public static File makeDirectory(String path) {
        String ftpLocalDirectory = null;
        File dir = new File(path);
        if (dir.exists()) {

            ftpLocalDirectory = String.valueOf(dir);
        } else {
            dir.mkdir();
            String str = String.valueOf(dir);
            ftpLocalDirectory = str;
        }
        return dir;
    }

    public static File makeMergedDirectory(String path) {
        String ftpLocalDirectory = null;
        File dir = new File(path);
        if (dir.exists()) {

            ftpLocalDirectory = String.valueOf(dir);
        } else {
            dir.mkdir();
            String str = String.valueOf(dir);
            ftpLocalDirectory = str;
        }
        return dir;
    }
    // Delete merged transactions by id

    //Get all merged transactions
    @RequestMapping(path = "/getAllMergedFiles", produces = "application/json", method = RequestMethod.GET)
    public String getAllMergedFiles(@RequestHeader(value = "token") String token) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        String json = null;
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try {
            if (userController.verifyToken(token)) {
                List<MergedFiles> tempFiles = objMergedFilesQuery.findAll();
                List<MergedFilesDTO> receivedData = new ArrayList<>();
                Type ceptype = new TypeToken<List<MergedReceivedFileContextDTO>>() {
                }.getType();
                for (MergedFiles temp : tempFiles) {
                    MergedFilesDTO mergedFilesDTO = new MergedFilesDTO();

                    mergedFilesDTO.setMergedFileId(temp.getMergedFileId());

                    try {
                        mergedFilesDTO.setReceivedFiles(new Gson().fromJson(temp.getReceivedFiles(), ceptype));
                    } catch (IllegalStateException | JsonSyntaxException exception) {
                        exception.printStackTrace();
                    }
                    mergedFilesDTO.setMergedFile(temp.getMergedFile());
                    mergedFilesDTO.setFileContext(temp.getFileContext());
                    mergedFilesDTO.setJobType(temp.getJobType());
                    mergedFilesDTO.setLocalDate(temp.getLocalDate());
                    mergedFilesDTO.setStatus(temp.getStatus());
                    receivedData.add(mergedFilesDTO);
                }


                LOGGER.info("All merged files record are listed: " + gson.toJson(receivedData));
                return gson.toJson(new ResponseObject(200, "All record are mentioned below", receivedData));
            }
        } catch (Exception e) {
            LOGGER.info("Error while showing all merged files record: " + gson.toJson(e));
            e.printStackTrace();
        }
        return new Gson().toJson(new ResponseObject(403, INVALID_USER, null));
    }

    @RequestMapping(path = "/getMergedFileById/{id}", produces = "application/json", method = RequestMethod.GET)
    public String getMergedFileByStatus(@PathVariable(value = "id") Long id, @RequestHeader(value = "token") String token) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try {
            if (userController.verifyToken(token)) {
                if (id instanceof Long) {
                    if (objMergedFilesQuery.findOne(id) == null) {
                        return new Gson().toJson(new ResponseObject(500, "Record Does Not Exist", null));
                    }
                    String json = null;
                    try {
                        LOGGER.info("Merged file is not found: " + gson.toJson(objMergedFilesQuery.findOne(id)));
                        return new Gson().toJson(new ResponseObject(200, "Record is find against: " + id, objectMapper.writeValueAsString((objMergedFilesQuery.findOne(id)))));
                    } catch (JsonProcessingException e) {
                        LOGGER.info("Error in finding merged file: " + gson.toJson(e));
                        e.printStackTrace();
                    }
                    return json;
                } else
                    return new Gson().toJson(new ResponseObject(400, "Bad Request...", null));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Gson().toJson(new ResponseObject(403, INVALID_USER, null));
    }

    @RequestMapping(path = "/getMergedFilesByStatus/{status}", produces = "application/json", method = RequestMethod.GET)
    public String getMergedFileByStatus(@PathVariable(value = "status") String status, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(userController.verifyToken(token))
            {
                String json = null;
                List<MergedFiles> tempFiles = objMergedFilesQuery.findMergedFilesByStatus(status);
                if(status.contains("success") || status.contains("Success") || status.contains("SUCCESS") || status.contains("failed") || status.contains("Failed") || status.contains("FAILED")|| status.contains("inprocess") || status.contains("Inprocess") || status.contains("INPROCESS"))
                {
                    if(tempFiles.size() <= 0)
                    {
                        LOGGER.info("Merged files are: " + gson.toJson(tempFiles));
                        return gson.toJson(new ResponseObject(200,  status.toUpperCase() + " no record found", null));
                    }
                    LOGGER.info("Merged files are: " + gson.toJson(tempFiles));
                    return gson.toJson(new ResponseObject(200,  status.toUpperCase() + " wise record(s) are mentioned below", tempFiles));
                }

                return gson.toJson(new ResponseObject(403, "Invalid searching keyword, Please come with right keyword", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while searhing merged file by status: " + gson.toJson(e));
            e.printStackTrace();
        }
        return new Gson().toJson(new ResponseObject(403, INVALID_USER, null));
    }

    @RequestMapping(path = "/getMergedFilesBySearch/{status}", produces = "application/json", method = RequestMethod.GET)
    public String getMergedFileBySearch(@PathVariable(value = "status")String status,
                                        @RequestParam(value = "generalParam") String generalParam,
                                        @RequestParam(value = "startDate") String startDate,
                                        @RequestParam(value = "endDate") String endDate,
                                        @RequestParam(value = "offset") Integer offset,
                                        @RequestParam(value = "limit") Integer limit,
                                        @RequestParam(value = "customerId") String customerId,
                                        @RequestHeader(value = "token") String token)

    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        String json = null;
        GsonBuilder b = new GsonBuilder();
        Gson gson = b.create();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);

        try
        {
            if (userController.verifyToken(token))
            {
                Type ceptype = new TypeToken<List<MergedReceivedFileContextDTO>>() {}.getType();
                if((status.equals("1") || status.equals("0") || status.equals("2")) && startDate.equals("") && endDate.equals("") && offset == 0)
                {
                    if (generalParam.equals("") || generalParam.equals(" ") || generalParam.equals("%20"))
                    {
                        List<MergedFiles> temp = objMergedFilesQuery.findMergedFilesByStatus(status);
                        List<MergedFilesDTO> receivedData = new ArrayList<>();

                        for (MergedFiles temp1 : temp) {
                            MergedFilesDTO mergedFilesDTO = new MergedFilesDTO();
                            mergedFilesDTO.setMergedFileId(temp1.getMergedFileId());
                            try {
                                mergedFilesDTO.setReceivedFiles(new Gson().fromJson(temp1.getReceivedFiles(), ceptype));
                            } catch (IllegalStateException | JsonSyntaxException exception)
                            {
                                exception.printStackTrace();
                            }
                            mergedFilesDTO.setMergedFile(temp1.getMergedFile());
                            mergedFilesDTO.setFileContext(temp1.getFileContext());
                            mergedFilesDTO.setJobType(temp1.getJobType());
                            mergedFilesDTO.setLocalDate(temp1.getLocalDate());
                            mergedFilesDTO.setStatus(temp1.getStatus());
                            receivedData.add(mergedFilesDTO);
                        }
                        LOGGER.info("All Merged files by search are listed: " + gson.toJson(receivedData));
                        return gson.toJson(new ResponseObject(200, status.toUpperCase() + " wise record(s) are mentioned below", receivedData));
                    }
                }
                else
                {
                    List<MergedFiles> tempFiles = objMergedFilesQuery.mergedByParameters(status, generalParam, startDate, endDate, offset, limit, customerId);
                    if (tempFiles.size() < 0)
                    {
                        LOGGER.info("All Merged files by search are listed: " + gson.toJson(tempFiles));
                        return gson.toJson(new ResponseObject(200, "There is no record", tempFiles));
                    }
                    else if(tempFiles.size() > 0)
                    {
                        List<MergedFilesDTO> receivedData = new ArrayList<>();

                        for (MergedFiles temp1 : tempFiles)
                        {
                            MergedFilesDTO mergedFilesDTO = new MergedFilesDTO();
                            mergedFilesDTO.setMergedFileId(temp1.getMergedFileId());
                            try
                            {
                                mergedFilesDTO.setReceivedFiles(new Gson().fromJson(temp1.getReceivedFiles(), ceptype));
                            }
                            catch (IllegalStateException | JsonSyntaxException exception)
                            {
                                exception.printStackTrace();
                            }
                            mergedFilesDTO.setMergedFile(temp1.getMergedFile());
                            mergedFilesDTO.setFileContext(temp1.getFileContext());
                            mergedFilesDTO.setJobType(temp1.getJobType());
                            mergedFilesDTO.setLocalDate(temp1.getLocalDate());
                            mergedFilesDTO.setStatus(temp1.getStatus());
                            receivedData.add(mergedFilesDTO);
                        }
                        Integer count = objMergedFilesQuery.countMergedByParameters(status, generalParam, startDate, endDate, customerId);
                        LOGGER.info("All Merged files by search are listed: " + gson.toJson(receivedData));
                        return gson.toJson(new ResponseObject(200,  status.toUpperCase() + " wise record(s) are mentioned below", receivedData, count));
                    }
                    else
                    {
                        LOGGER.info("No record is found " + gson.toJson(tempFiles));
                        return gson.toJson(new ResponseObject(200,  status.toUpperCase() + " No record is found", tempFiles));
                    }
                }
            }
            return gson.toJson(new ResponseObject(403, "Invalid request", null));
        }
        catch (Exception e)
        {
            LOGGER.info("Error while showing all merged file by search: " + gson.toJson(e));
            e.printStackTrace();
        }
        return new Gson().toJson(new ResponseObject(403, INVALID_USER, null));
    }

    @RequestMapping(path = "/deleteMergedFileById/{id}", produces = "application/json", method = RequestMethod.DELETE)
    public String deleteMergedFile(@PathVariable(value = "id") Long id, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try
        {
            if(userController.verifyToken(token))
            {
                if(objMergedFilesQuery.findOne(id) == null)
                {
                    LOGGER.info("Merged file is not found:" + gson.toJson(objMergedFilesQuery.findOne(id)));
                    return new Gson().toJson(new ResponseObject(500 , "Record Does Not Exist", null));
                }
                else
                {
                    MergedFiles obj = objMergedFilesQuery.findOne(id);
                    objMergedFilesQuery.delete(obj);
                    LOGGER.info("Merged file is deleted:" + gson.toJson(obj));
                    return new Gson().toJson(new ResponseObject(200, "Record Has Been Deleted Sucessfully...", null));
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while deleting merged file: " + gson.toJson(e));
            e.printStackTrace();
        }
        return new Gson().toJson(new ResponseObject(403, INVALID_USER, null));
    }

    static class FileInfo
    {

        private String fileName;
        private File file;
        private List<String> fileNameAttributes;

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public File getFile() {
            return file;
        }

        public void setFile(File file) {
            this.file = file;
        }

        public List<String> getFileNameAttributes() {
            return fileNameAttributes;
        }

        public void setFileNameAttributes(List<String> fileNameAttributes) {
            this.fileNameAttributes = fileNameAttributes;
        }
    }

    private static void delete(File file) throws IOException {

        for (File childFile : file.listFiles()) {

            if (childFile.isDirectory())
            {
                childFile.delete();
            }
            else
            {
                if (!childFile.delete())
                {
                    throw new IOException();
                }
            }
        }

        if (!file.delete()) {
            throw new IOException();
        }
    }
}
