package com.gv.fmu.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.gv.fmu.domain.EmailTransport;
import com.gv.fmu.domain.User;
import com.gv.fmu.dto.EmailCustomTokensDTO;
import com.gv.fmu.dto.EmailTransportDTO;
import com.gv.fmu.dto.ResponseObject;
import com.gv.fmu.repository.EmailTransportDAO;
import com.gv.fmu.repository.UserRepositoryDAO;
import com.gv.fmu.util.HibernateProxyTypeAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by Muhammad Yaseen on 5/11/2018.
 */

@RestController
@RequestMapping("/email")
public class EmailController
{

    private static Logger LOGGER = LoggerFactory.getLogger(EmailController.class);
    private final UserController USER_CONTROLLER;
    private @Autowired
    EmailTransportDAO emailTransportQuery;
    @Autowired
    private UserRepositoryDAO userRepositoryDAO;

    public EmailController(UserController user_controller)
    {
        USER_CONTROLLER = user_controller;
    }

    //Get all records
    @RequestMapping(path = "/getAllEmailTransport", produces = "application/json", method = RequestMethod.GET)
    public String getAllEmailTransport(@RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                List<EmailTransport> tempFiles = emailTransportQuery.findAll();
                List<EmailTransportDTO> emailTransportDTOs = new ArrayList<>();
                Type objType = new TypeToken<List<EmailCustomTokensDTO>>() {}.getType();
                if(tempFiles.size() <= 0)
                {
                    LOGGER.info("\nAll Email(s) configuration record are listed: ", gson.toJson(tempFiles));
                    return gson.toJson(new ResponseObject(200, "There is no record", tempFiles));
                }
                for(EmailTransport temp : tempFiles)
                {
                    EmailTransportDTO obj = new EmailTransportDTO();
                    obj.setEmailTransportId(temp.getEmailTransportId());
                    obj.setEmailServer(temp.getEmailServer());
                    obj.setAuthentication(temp.getAuthentication());
                    obj.setSecureConnection(temp.getSecureConnection());
                    obj.setEmailServer(temp.getEmailServer());
                    obj.setEmailUserName(temp.getEmailUserName());
                    obj.setEmailPassword(temp.getEmailPassword());
                    obj.setEmailPort(temp.getEmailPort());
                    obj.setCustomer(temp.getCustomer());
                    emailTransportDTOs.add(obj);
                }
                LOGGER.info("\nAll Email(s) configuration record are listed\n", gson.toJson(tempFiles));
                return gson.toJson(new ResponseObject(200, "All record are mentioned below", emailTransportDTOs));
            }
            else
            {
                LOGGER.info("\n403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("\nError while showing Email(s)\n", e);
            e.printStackTrace();
        }
        return null;
    }

    //Get EmailTransport by id
    @RequestMapping(path = "/getEmailTransportById/{id}", produces = "application/json", method = RequestMethod.GET)
    public String getEmailTransportById(@PathVariable(value = "id") Long emailId, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                if(emailId instanceof Long)
                {
                    if (emailTransportQuery.findOne(emailId) == null)
                    {
                        LOGGER.info("\nEmail configuration not found: " + gson.toJson(emailTransportQuery.findOne(emailId)));
                        return new Gson().toJson(new ResponseObject(500, "Record Does Not Exist", null));
                    }
                    else
                    {
                        LOGGER.info("\nEmail configuration is found\n", emailTransportQuery.findOne(emailId));
                        return gson.toJson(new ResponseObject(200, "Records is find against id: " + emailId, emailTransportQuery.findOne(emailId)));
                    }
                }
                else
                {
                    LOGGER.info("\n400: Bad Request");
                    return new Gson().toJson(new ResponseObject(400, "Bad Request...", null));
                }

            }
            else
            {
                LOGGER.info("\n403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while getting email transport by id\n", e);
            e.printStackTrace();
        }
        return null;
    }

    //Create new EmailTransport
    @RequestMapping(path = "/saveOrUpdateEmailTransport", produces = "application/json", method = RequestMethod.POST)
    public String saveOrUpdateEmailTransport(@RequestBody EmailTransport emailTransport, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                User objUser = userRepositoryDAO.findByToken(token);
                if(emailTransport != null)
                {
                    emailTransport.setCustomer(objUser.getCustomer());
                    LOGGER.info("Email configuration is saved: " + gson.toJson(emailTransport));
                    return gson.toJson(new ResponseObject(200, "Record is saved", emailTransportQuery.save(emailTransport)));
                }
                else
                {
                    LOGGER.info("Bad Request");
                    return new Gson().toJson(new ResponseObject(400, "Bad Request ", null));
                }
            }
            else
            {
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while saving email configuration: " + gson.toJson(e));
            e.printStackTrace();
        }
        return null;
    }

    // Delete a EmailTransport
    @RequestMapping(path = "/deleteEmailTransportId/{id}", produces = "application/json", method = RequestMethod.DELETE)
    public String deleteMergingJobContextById(@PathVariable(value = "id") Long id, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                if(emailTransportQuery.findOne(id) == null)
                {
                    LOGGER.info("Email configuration not deleted:" + id);
                    return new Gson().toJson(new ResponseObject(500 , "Record Does Not Exist", null));
                }
                else
                {
                    EmailTransport emailTransport = emailTransportQuery.findOne(id);
                    emailTransportQuery.delete(emailTransport);
                    LOGGER.info("Email is deleted:" + gson.toJson(emailTransport));
                    return new Gson().toJson(new ResponseObject(200, "Record has been deleted successfully against: " + id, null));
                }
            }
            else
            {
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while deleting email configuration: " + gson.toJson(e));
            e.printStackTrace();
        }
        return null;
    }

    public String sendEmail(String mailHost, String emailUsername, String emailPassword,
                            Integer port, String emailFrom, String emailTo, String filePath,
                            String mailSubject, String messageBody)
    {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", mailHost);
        props.put("mail.smtp.port", port);

        // Get the Session object.
        Session session = Session.getInstance(props, new javax.mail.Authenticator()
                {
                    protected PasswordAuthentication getPasswordAuthentication()
                    {
                        return new PasswordAuthentication(emailUsername, emailPassword);
                    }
                }
                );
        try
        {
            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(emailFrom));

            // Set To: header field of the header.
            if (emailTo.indexOf(',') > 0)
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailTo));
            else
                message.setRecipient(Message.RecipientType.CC, new InternetAddress(emailTo));

            // Set Subject: header field
            message.setSubject(mailSubject);

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setText(messageBody);

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();
            String filename = filePath;
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(filename.substring(filename.lastIndexOf("/") + 1, filename.length()));
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);

            // Send message
            Transport.send(message);

            System.out.println("Sent message successfully....");
            LOGGER.info("Sent message successfully...");
        }
        catch (AddressException e)
        {
            e.printStackTrace();
        }
        catch (MessagingException e)
        {
            e.printStackTrace();
        }
        LOGGER.info("Ok");
        return "Ok";
    }
}
