package com.gv.fmu.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.gv.fmu.domain.InputOutputSetting;
import com.gv.fmu.domain.MergingJobContext;
import com.gv.fmu.domain.MergingWorkFlow;
import com.gv.fmu.domain.User;
import com.gv.fmu.dto.*;
import com.gv.fmu.ftp.listener.DynamicInboundFTPChannelResolver;
import com.gv.fmu.repository.MergingJobContextDAO;
import com.gv.fmu.repository.MergingWorkflowDAO;
import com.gv.fmu.repository.UserRepositoryDAO;
import com.gv.fmu.util.HibernateProxyTypeAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Muhammad Yaseen on 5/14/2018.
 */

@RestController
@RequestMapping("/mergingWorkflow")
public class MergingWorkflowController
{
    public static final String INVALID_USER_MESSAGE = "Invalid User";
    private static Logger logger = LoggerFactory.getLogger(MergingWorkflowController.class);
    private final UserController USER_CONTROLLER;
    public Cache<String, Map<String, InputOutputSetting>> cache = CacheBuilder.newBuilder().maximumSize(1000).build();
    @Autowired
    private MergingWorkflowDAO mergingWorkflowQuery;
    @Autowired
    private MergingJobContextDAO mergingJobContextQuery;
    @Autowired
    private DynamicInboundFTPChannelResolver dynamicInboundFTPChannelResolver;
    @Autowired
    private UserRepositoryDAO userRepositoryDAO;

    public MergingWorkflowController(UserController user_controller)
    {
        USER_CONTROLLER = user_controller;
    }

    @PostConstruct
    public void init()
    {
        this.cache = CacheBuilder.newBuilder().build();
    }

    @PostMapping(path = "/activateChannel/{flag}", produces = "application/json")
    public String activateChannel(@RequestBody MergingWorkFlow mergingWorkFlow, @RequestHeader(value = "token") String token, @PathVariable(name = "flag") Integer flag)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try
        {
            if (USER_CONTROLLER.verifyToken(token))
            {
                MergingWorkFlow savingObject = new MergingWorkFlow();
                MergingWorkFlow mergingWorkFlow1 = new MergingWorkFlow();
                mergingWorkFlow1 = mergingWorkflowQuery.findOne(mergingWorkFlow.getWorkflowId());
                if(mergingWorkFlow1 == null)
                {
                    logger.info("\n403: Invalid access of record");
                    return gson.toJson(new ResponseObject(403, "Invalid access of record", null));
                }
                else if(mergingWorkFlow1.getMergingJobContext() == null)
                {
                    logger.info("204: Please set merging job context first");
                    return gson.toJson(new ResponseObject(204, "Please set merging job context first", null));
                }
                else
                {
                    MergingJobContext mergingJobContext = mergingJobContextQuery.findOne(mergingWorkFlow1.getMergingJobContext().getMergingJobContextId());
                    if (mergingWorkFlow1.getMergingJobContext().getFileMergingPattern().isEmpty())
                    {
                        logger.info("\n204: Please set inbound merging pattern first");
                        return gson.toJson(new ResponseObject(204, "Please set inbound merging pattern first", null));
                    }
                    else
                    {
                        if(mergingWorkFlow1.getMergingJobContext().getFileMergingPattern().get(0).getInputOutputSetting() == null)
                        {
                            logger.info("\n204: Please set inbound FTP first");
                            return gson.toJson(new ResponseObject(204, "Please set inbound FTP first", null));
                        }
                        else if(mergingWorkFlow1.getOutBoundPattern() == null)
                        {
                            logger.info("\n204: Please set outbound config first");
                            return gson.toJson(new ResponseObject(204, "Please set outbound config first", null));
                        }
                        else
                        {
                            Map<String, InputOutputSetting> directories = new HashMap<>();
                            for(int x = 0; x < mergingWorkFlow1.getMergingJobContext().getFileMergingPattern().size(); x++)
                            {
                                String directoryIdentifier = mergingWorkFlow1.getMergingJobContext().getFileMergingPattern().get(x).getFtpRemoteDirectory().concat("||").concat(mergingWorkFlow1.getMergingJobContext().getFileMergingPattern().get(x).getInputOutputSetting().getInboundIpAddress());
                                User objUser = userRepositoryDAO.findByToken(token);
                                if(flag == 1)
                                {
                                    savingObject.setWorkflowId(mergingWorkFlow1.getWorkflowId());
                                    savingObject.setWorkflowName(mergingWorkFlow1.getWorkflowName());

                                    // activating bit
                                    savingObject.setActions(1);
                                    savingObject.setMergingJobContext(mergingWorkFlow1.getMergingJobContext());
                                    savingObject.setUser(mergingWorkFlow1.getUser());
                                    savingObject.setOutBoundPattern(mergingWorkFlow1.getOutBoundPattern());
                                    savingObject.setCustomerId(objUser.getCustomer().getCustomerId());
                                    mergingWorkflowQuery.save(savingObject);
                                    logger.info("State changed of workflow: ".concat(gson.toJson(savingObject)));
                                    System.out.println("Channel is activated successfully");
                                    directories.put(mergingWorkFlow1.getMergingJobContext().getFileMergingPattern().get(x).getFtpRemoteDirectory(), mergingWorkFlow1.getMergingJobContext().getFileMergingPattern().get(x).getInputOutputSetting());
                                    cache.put(mergingWorkFlow1.getMergingJobContext().getFileMergingPattern().get(x).getFtpRemoteDirectory(), directories);
                                    dynamicInboundFTPChannelResolver.reloadChannel(directoryIdentifier);
                                    logger.info("Active channel is: ".concat(gson.toJson(mergingWorkFlow1.getMergingJobContext().getFileMergingPattern().get(x).getFtpRemoteDirectory())));
                                    gson.toJson(new ResponseObject(200, "Channel is activated successfully: " + mergingWorkFlow1.getMergingJobContext().getFileMergingPattern().get(x).getFileMergingPatternId(), null));
                                }
                                else if(flag == 0)
                                {
                                    savingObject.setWorkflowId(mergingWorkFlow1.getWorkflowId());
                                    savingObject.setWorkflowName(mergingWorkFlow1.getWorkflowName());

                                    // deactivating bit
                                    savingObject.setActions(0);
                                    savingObject.setMergingJobContext(mergingWorkFlow1.getMergingJobContext());
                                    savingObject.setUser(mergingWorkFlow1.getUser());
                                    savingObject.setOutBoundPattern(mergingWorkFlow1.getOutBoundPattern());
                                    savingObject.setCustomerId(objUser.getCustomer().getCustomerId());
                                    mergingWorkflowQuery.save(savingObject);
                                    logger.info("State changed of workflow: ".concat(gson.toJson(savingObject)));
                                    System.out.println("Channel is deActivated successfully");
                                    dynamicInboundFTPChannelResolver.stopChannel(directoryIdentifier);
                                    cache.cleanUp();
                                    logger.info("Deactivate channel is: ".concat(gson.toJson(mergingWorkFlow1.getMergingJobContext().getFileMergingPattern().get(x).getInputOutputSetting().getFileTransportID())));
                                }
                                else
                                {
                                    logger.info("\n403: There is no channel");
                                    return gson.toJson(new ResponseObject(403, "There is no channel", null));
                                }
                            }
                            return "";
                        }
                    }
                }
            }
            else
            {
                logger.info("\n403: ".concat(INVALID_USER_MESSAGE));
                return new Gson().toJson(new ResponseObject(403, INVALID_USER_MESSAGE, null));
            }
        }
        catch (Exception e)
        {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return new Gson().toJson(new ResponseObject(404, "", null));
    }

    @RequestMapping(path = "/directoryIsUsedForInbound", produces = "application/json", method = RequestMethod.POST)
    public String directoryIsUsedForInbound(@RequestHeader(value = "token") String token, @RequestBody DirectoryExistDTO directoryExistDTO)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try
        {
            if (USER_CONTROLLER.verifyToken(token))
            {
                Object running = cache.getIfPresent(directoryExistDTO.getDirectory());
                if ((running != null))
                {
                    logger.info("\n400: Directory is in used");
                    return gson.toJson(new ResponseObject(400, "Directory is in used", null));
                }
                else
                {
                    logger.info("\n200: Directory is available");
                    return gson.toJson(new ResponseObject(200, "Directory is available", null));
                }
            } else
            {
                logger.info("\n403: Invalid User");
                return gson.toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            logger.error("Error:" + e);
            e.printStackTrace();
        }
        return "";
    }

    @RequestMapping(path = "/getAllMergingWorkflows", produces = "application/json", method = RequestMethod.GET)
    public String getAllMergingWorkflows(@RequestHeader(value = "token") String token, @RequestParam(value = "customerId") String customerId)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if (USER_CONTROLLER.verifyToken(token))
            {
                Type objType1 = new TypeToken<List<OutboundCustomTokens>>() {}.getType();
                Type objType2 = new TypeToken<List<InboundCustomTokens>>() {}.getType();
                List<MergingWorkFlow> tempFiles = mergingWorkflowQuery.getAllMergingWorkflowsByCustomer(customerId);
                List<MergingWorkAttributesDTO> mergingWorkAttributesDTOs = new ArrayList<>();
                List<String> inboundNames = new ArrayList<>();

                for (int x = 0; x < tempFiles.size(); x++)
                {
                    OutboundMergingPatternDTO outboundMergingPatternDTO = new OutboundMergingPatternDTO();
                    List<FileMergingPatternDTO> fileMergingPatternDTOs = new ArrayList<>();
                    MergingWorkAttributesDTO objMergingWorkAttributesDTO = new MergingWorkAttributesDTO();
                    objMergingWorkAttributesDTO.setWorkflowId(tempFiles.get(x).getWorkflowId());
                    objMergingWorkAttributesDTO.setWorkflowName(tempFiles.get(x).getWorkflowName());
                    if(tempFiles.get(x).getOutBoundPattern() == null && tempFiles.get(x).getMergingJobContext() == null)
                    {
                        objMergingWorkAttributesDTO.setWorkflow_Inbound("");
                        objMergingWorkAttributesDTO.setWorkflow_Outbound("");
                        objMergingWorkAttributesDTO.setOutboundMergingPatternDTO(new OutboundMergingPatternDTO());
                        objMergingWorkAttributesDTO.setMergingJobContext(new MergingJobContext());
                        objMergingWorkAttributesDTO.setActions(tempFiles.get(x).getActions());
                        objMergingWorkAttributesDTO.setUser(tempFiles.get(x).getUser());
                        objMergingWorkAttributesDTO.setCustomerId(tempFiles.get(x).getUser().getCustomer().getCustomerId());
                        mergingWorkAttributesDTOs.add(objMergingWorkAttributesDTO);
                    }
                    else
                    {
                        objMergingWorkAttributesDTO.setWorkflow_Inbound(gson.toJson(inboundNames));
                        objMergingWorkAttributesDTO.setActions(tempFiles.get(x).getActions());
                        objMergingWorkAttributesDTO.setUser(tempFiles.get(x).getUser());
                        objMergingWorkAttributesDTO.setMergingJobContext(tempFiles.get(x).getMergingJobContext());

                        //setting inbound properties
                        for(int j = 0; j < tempFiles.get(x).getMergingJobContext().getFileMergingPattern().size(); j++)
                        {
                            FileMergingPatternDTO fileMergingPatternDTO = new FileMergingPatternDTO();
                            fileMergingPatternDTO.setFileMergingPatternId(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getFileMergingPatternId());
                            fileMergingPatternDTO.setInboundCustomToken(new Gson().fromJson(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundCustomToken(), objType2));
                            fileMergingPatternDTO.setInboundCustomerIdentifier(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundCustomerIdentifier());
                            fileMergingPatternDTO.setInboundCustomerIdentifierOrder(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundCustomerIdentifierOrder());
                            fileMergingPatternDTO.setInboundDocumentIdentifier(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundDocumentIdentifier());
                            fileMergingPatternDTO.setInboundDocumentIdentifierOrder(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundDocumentIdentifierOrder());
                            fileMergingPatternDTO.setInboundPackageIdentifier(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundPackageIdentifier());
                            fileMergingPatternDTO.setInboundPackageIdentifierOrder(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundPackageIdentifierOrder());
                            fileMergingPatternDTO.setInboundDocumentSeprator(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundDocumentSeprator());
                            fileMergingPatternDTO.setInboundPatternName(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getPatternName());
                            fileMergingPatternDTO.setFtpRemoteDirectory(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getFtpRemoteDirectory());
                            fileMergingPatternDTO.setCompletePattern(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getCompletePattern());
                            fileMergingPatternDTO.setFileOrder(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getFileOrder());
                            fileMergingPatternDTO.setInputOutputSetting(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInputOutputSetting());
                            fileMergingPatternDTO.setMergingJobContext(tempFiles.get(x).getMergingJobContext());

                            //for speeding for data retrivel with frontend
                            /*FileMergingPatternDTO fileMergingPatternDTO = new FileMergingPatternDTO();
                            FileMergingPatternDTO2 fileMergingPattern = new FileMergingPatternDTO2();
                            MergingWorkFlow mergingWorkFlow = new MergingWorkFlow();
                            fileMergingPattern.setFileMergingPatternId(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getFileMergingPatternId());
                            fileMergingPattern.setInboundCustomToken(new Gson().fromJson(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundCustomToken(), objType2));
                            fileMergingPattern.setInboundCustomerIdentifier(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundCustomerIdentifier());
                            fileMergingPattern.setInboundCustomerIdentifierOrder(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundCustomerIdentifierOrder());
                            fileMergingPattern.setInboundDocumentIdentifier(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundDocumentIdentifier());
                            fileMergingPattern.setInboundDocumentIdentifierOrder(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundDocumentIdentifierOrder());
                            fileMergingPattern.setInboundPackageIdentifier(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundPackageIdentifier());
                            fileMergingPattern.setInboundPackageIdentifierOrder(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundPackageIdentifierOrder());
                            fileMergingPattern.setInboundDocumentSeprator(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInboundDocumentSeprator());
                            fileMergingPattern.setPatternName(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getPatternName());
                            fileMergingPattern.setCompletePattern(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getCompletePattern());
                            fileMergingPatternDTO.setFileMergingPattern(fileMergingPattern);
                            mergingWorkFlow.setWorkflowId(tempFiles.get(x).getWorkflowId());
                            mergingWorkFlow.setWorkflowName(tempFiles.get(x).getWorkflowName());
                            fileMergingPatternDTO.setMergingWorkFlow(mergingWorkFlow);
                            fileMergingPatternDTO.setInputOutputSetting(tempFiles.get(x).getMergingJobContext().getFileMergingPattern().get(j).getInputOutputSetting());
                            fileMergingPatternDTO.setMergingJobContext(tempFiles.get(x).getMergingJobContext());

                            if(!fileMergingPatternDTOs.contains(fileMergingPatternDTO))
                            {
                                fileMergingPatternDTOs.add(fileMergingPatternDTO);
                                objMergingWorkAttributesDTO.setFileMergingPatternDTO(fileMergingPatternDTOs);
                            }*/

                            if(!fileMergingPatternDTOs.contains(fileMergingPatternDTO))
                            {
                                fileMergingPatternDTOs.add(fileMergingPatternDTO);
                                objMergingWorkAttributesDTO.setFileMergingPatternDTO(fileMergingPatternDTOs);
                            }

                        }

                        //setting outbound properties
                        if(tempFiles.get(x).getOutBoundPattern() != null)
                        {
                            outboundMergingPatternDTO.setOutboundPatternId(tempFiles.get(x).getOutBoundPattern().getOutboundPatternId());
                            outboundMergingPatternDTO.setOutboundCustomToken(new Gson().fromJson(tempFiles.get(x).getOutBoundPattern().getOutboundCustomToken(), objType1));
                            outboundMergingPatternDTO.setOutboundCustomIdentifier(tempFiles.get(x).getOutBoundPattern().getOutboundCustomIdentifier());
                            outboundMergingPatternDTO.setOutboundCustomIdentifierOrder(tempFiles.get(x).getOutBoundPattern().getOutboundCustomIdentifierOrder());
                            outboundMergingPatternDTO.setOutboundDocumentIdentifier(tempFiles.get(x).getOutBoundPattern().getOutboundDocumentIdentifier());
                            outboundMergingPatternDTO.setOutboundDocumentIdentifierOrder(tempFiles.get(x).getOutBoundPattern().getOutboundDocumentIdentifierOrder());
                            outboundMergingPatternDTO.setOutboundPackageIdentifier(tempFiles.get(x).getOutBoundPattern().getOutboundPackageIdentifier());
                            outboundMergingPatternDTO.setOutboundPackageIdentifierOrder(tempFiles.get(x).getOutBoundPattern().getOutboundPackageIdentifierOrder());
                            outboundMergingPatternDTO.setOutboundDocumentSeprator(tempFiles.get(x).getOutBoundPattern().getOutboundDocumentSeprator());
                            outboundMergingPatternDTO.setOutboundPatternName(tempFiles.get(x).getOutBoundPattern().getOutboundPatternName());
                            outboundMergingPatternDTO.setEmail(tempFiles.get(x).getOutBoundPattern().getEmail());


                            for(int k = 0; k < tempFiles.get(x).getOutBoundPattern().getOutBoundFTPs().size(); k++)
                            {
                                outboundMergingPatternDTO.setOutBoundFTPs(tempFiles.get(x).getOutBoundPattern().getOutBoundFTPs());
                            }
                            objMergingWorkAttributesDTO.setOutboundMergingPatternDTO(outboundMergingPatternDTO);
                        }
                        else
                        {
                            objMergingWorkAttributesDTO.setOutboundMergingPatternDTO(outboundMergingPatternDTO);
                        }

                        objMergingWorkAttributesDTO.setCustomerId(tempFiles.get(x).getUser().getCustomer().getCustomerId());

                        mergingWorkAttributesDTOs.add(objMergingWorkAttributesDTO);
                    }
                }
                logger.info("All workflow(s) are listed: ".concat(gson.toJson(mergingWorkAttributesDTOs)));
                return gson.toJson(new ResponseObject(200, "All record are mentioned below", mergingWorkAttributesDTOs));
            }
            else
            {
                logger.info("\n403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, INVALID_USER_MESSAGE, null));
            }

        }
        catch (Exception e)
        {
            logger.info("Error while showing all Workflows: ".concat(gson.toJson(e)));
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(path = "/getMergingWorkflowById/{id}", produces = "application/json", method = RequestMethod.GET)
    public String getMergingWorkflowById(@PathVariable(value = "id") Long id, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                if(id instanceof Long)
                {
                    if (mergingWorkflowQuery.findOne(id) == null)
                    {
                        logger.info("500: Record Does Not Exist");
                        return new Gson().toJson(new ResponseObject(500, "Record Does Not Exist", null));
                    }
                    else
                    {
                        logger.info("Workflow is find: ".concat(gson.toJson(mergingWorkflowQuery.findOne(id))));
                        return gson.toJson(new ResponseObject(200, "Record is find against: " + id, mergingWorkflowQuery.findOne(id)));
                    }
                }
                else
                {
                    logger.info("\n400: Bad Request");
                    return new Gson().toJson(new ResponseObject(400, "Bad Request...", null));
                }

            }
            else
            {
                logger.info("\n403: ".concat(INVALID_USER_MESSAGE));
                return new Gson().toJson(new ResponseObject(403, INVALID_USER_MESSAGE, null));
            }
        }
        catch (Exception e)
        {
            logger.info("Error in finding workflow: ".concat(gson.toJson(e)));
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(path = "/saveOrUpdateMergingWorkflow", produces = "application/json", method = RequestMethod.POST)
    public String saveOrUpdateMergingWorkFlows(@RequestBody MergingWorkFlow mergingWorkFlow, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                User objUser = userRepositoryDAO.findByToken(token);
                MergingWorkFlow mergingWorkFlow1 = mergingWorkFlow;
                mergingWorkFlow1.setActions(0);
                mergingWorkFlow1.setWorkflowName(mergingWorkFlow.getWorkflowName());
                mergingWorkFlow1.setUser(objUser);
                mergingWorkFlow1.setCustomerId(objUser.getCustomer().getCustomerId());
                if(mergingWorkFlow.getOutBoundPattern() != null &&  mergingWorkFlow.getMergingJobContext() != null)
                {
                    mergingWorkFlow1.setMergingJobContext(mergingWorkFlow.getMergingJobContext());
                    mergingWorkFlow.setOutBoundPattern(mergingWorkFlow.getOutBoundPattern());
                }
                else
                {
                    mergingWorkFlow1.setOutBoundPattern(null);
                    mergingWorkFlow1.setMergingJobContext(null);
                }

/*                Long id = mergingJobContextMergingWorkflowDTO.getMergingJobContext().getMergingJobContextId();
                Long userId = mergingJobContextMergingWorkflowDTO.getUser().getUserId();
                MergingJobContext findOne = mergingJobContextQuery.findOne(id);
                User findUser = userRepositoryQuery.findOne(userId);
                OutBoundPattern outBoundPattern = outboundPatternDAO.findOne(mergingJobContextMergingWorkflowDTO.getOutBoundPattern().getOutboundId());*/
/*
                mergingWorkFlow.setCustomerId(findUser.getCustomer().getCustomerId());
                mergingWorkFlow.setMergingJobContext(findOne);
                mergingWorkFlow.setUser(findUser);
                mergingWorkFlow.setOutBoundPattern(outBoundPattern);*/

                logger.info("Workflow is saved: ".concat(gson.toJson(mergingWorkFlow1)));
                return gson.toJson(new ResponseObject(200, "Record is saved ", (mergingWorkflowQuery.save(mergingWorkFlow1))));
            }
            else
            {
                logger.info("\n403: ".concat(INVALID_USER_MESSAGE));
                return new Gson().toJson(new ResponseObject(403, INVALID_USER_MESSAGE, null));
            }
        }
        catch (Exception e)
        {
            logger.info("Error while saving workflow: ".concat(gson.toJson(e)));
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(path = "/deleteMergingWorkflowById/{id}", produces = "application/json", method = RequestMethod.DELETE)
    public String deleteMergingWorkflows(@PathVariable(value = "id") Long id, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                if(mergingWorkflowQuery.findOne(id) == null)
                {
                    logger.info("\n500: Record Does Not Exist");
                    return new Gson().toJson(new ResponseObject(500 , "Record Does Not Exist", null));
                }
                else
                {
                    MergingWorkFlow obj = mergingWorkflowQuery.findOne(id);
                    mergingWorkflowQuery.delete(obj);
                    logger.info("Workflow deleted:".concat(gson.toJson(obj)));
                    return new Gson().toJson(new ResponseObject(200, "Record has been deleted sucessfully...", null));
                }
            }
            else
            {
                logger.info("\n403: ".concat(INVALID_USER_MESSAGE));
                return new Gson().toJson(new ResponseObject(403, INVALID_USER_MESSAGE, null));
            }
        }
        catch (Exception e)
        {
            logger.info("\nError while deleting workflow: ", e);
            e.printStackTrace();
        }
        return null;
    }
}