package com.gv.fmu.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gv.fmu.domain.InputOutputSetting;
import com.gv.fmu.domain.User;
import com.gv.fmu.dto.ResponseObject;
import com.gv.fmu.repository.InputOutputSettingDAO;
import com.gv.fmu.repository.UserRepositoryDAO;
import com.gv.fmu.util.HibernateProxyTypeAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Muhammad Yaseen on 5/2/2018.
 */

@RestController
@RequestMapping("/inputOutputSetting")
public class FTPController
{
    private static Logger LOGGER = LoggerFactory.getLogger(FTPController.class);

    private final UserController USER_CONTROLLER;

    @Autowired
    private InputOutputSettingDAO inputOutputSettingQuery;

    @Autowired
    private UserRepositoryDAO userRepositoryDAO;

    public FTPController(UserController user_controller)
    {
        USER_CONTROLLER = user_controller;
    }

    @RequestMapping(path = "/getAllInputOutputSettings", produces = "application/json", method = RequestMethod.GET)
    public String getAllInputOutputSetting(@RequestHeader(value = "token") String token, @RequestParam(value = "customerId")String customerId)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                List<InputOutputSetting> tempFiles = inputOutputSettingQuery.getAllInboundFTPsByCustomer(customerId);

                if(tempFiles.size() <= 0)
                {
                    LOGGER.info("200: Empty List");
                    return gson.toJson(new ResponseObject(200, "Empty List", tempFiles));
                }
                LOGGER.info("All FTP(s) are listed: " + gson.toJson(tempFiles));
                return gson.toJson(new ResponseObject(200, "All record are mentioned below", tempFiles));
            }
            else
            {
                LOGGER.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while showing all FTP(s)\n", gson.toJson(e));
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(path = "/getInputOutputSettingById/{id}", produces = "application/json", method = RequestMethod.GET)
    public String getSingleInputOutputSetting(@PathVariable(value = "id") Integer id, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                if(id instanceof Integer)
                {
                    if (inputOutputSettingQuery.findOne(id) == null)
                    {
                        LOGGER.info("500: Record Does Not Exist");
                        return new Gson().toJson(new ResponseObject(500, "Record Does Not Exist", null));
                    }
                    LOGGER.info("FTP is find: " + gson.toJson(inputOutputSettingQuery.findOne(id)));
                    return gson.toJson(new ResponseObject(200, "Record is find against :" + id, inputOutputSettingQuery.findOne(id)));
                }
                else
                {
                    LOGGER.info("400: Bad Request");
                    return new Gson().toJson(new ResponseObject(400, "Bad Request...", null));
                }
            }
            else
            {
                LOGGER.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error in finding FTP: " + gson.toJson(e));
            e.printStackTrace();
        }
        return null;
    }


    @RequestMapping(path = "/saveOrUpdateInputOutputSetting", produces = "application/json", method = RequestMethod.POST)
    public String saveOrUpdateRecordInputOutputSetting(@RequestBody InputOutputSetting inputOutputSetting, @RequestHeader(value = "token") String token) throws JsonProcessingException
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                User objUser = userRepositoryDAO.findByToken(token);
                inputOutputSetting.setCustomerId(objUser.getCustomer().getCustomerId());
                LOGGER.info("FTP is saved: " + gson.toJson(inputOutputSetting));
                return gson.toJson(new ResponseObject(200, "Record is saved ", inputOutputSettingQuery.save(inputOutputSetting)));
            }
            else
            {
                LOGGER.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while saving FTP: " + gson.toJson(e));
            e.printStackTrace();
        }
       return null;
    }

    @RequestMapping(path = "deleteInputOutputSettingById/{id}", produces = "application/json", method = RequestMethod.DELETE)
    public String deleteInputOutputSettingRecord(@PathVariable(value = "id") Integer id, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                if(inputOutputSettingQuery.findOne(id) == null)
                {
                    LOGGER.info("500: Record Does Not Exist");
                    return new Gson().toJson(new ResponseObject(500 , "Record Does Not Exist", null));
                }
                else
                {
                    InputOutputSetting obj = inputOutputSettingQuery.findOne(id);
                    inputOutputSettingQuery.delete(obj);
                    LOGGER.info("FTP is deleted:" + gson.toJson(obj));
                    return new Gson().toJson(new ResponseObject(200, "Record has been deleted successfully...", null));
                }
            }
            else
            {
                LOGGER.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while deleting FTP: " + gson.toJson(e));
            e.printStackTrace();
        }
        return null;
    }
}
