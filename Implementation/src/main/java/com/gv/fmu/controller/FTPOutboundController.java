package com.gv.fmu.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gv.fmu.domain.OutBoundFTP;
import com.gv.fmu.domain.User;
import com.gv.fmu.dto.ResponseObject;
import com.gv.fmu.repository.OutboundFTPDAO;
import com.gv.fmu.repository.UserRepositoryDAO;
import com.gv.fmu.util.HibernateProxyTypeAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Muhammad Yaseen on 6/12/2018.
 */

@RestController
@RequestMapping("/ftpOutbound")
public class FTPOutboundController
{
    private static Logger LOGGER = LoggerFactory.getLogger(FTPOutboundController.class);

    private final UserController USER_CONTROLLER;

    @Autowired
    private OutboundFTPDAO outboundFTPQuery;

    @Autowired
    private UserRepositoryDAO userRepositoryDAO;

    public FTPOutboundController(UserController user_controller)
    {
        USER_CONTROLLER = user_controller;
    }

    @RequestMapping(path = "/getAllOutboundFTP", produces = "application/json", method = RequestMethod.GET)
    public String getAllOutboundFTP(@RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                List<OutBoundFTP> tempFiles = outboundFTPQuery.findAll();

                if(tempFiles.size() <= 0)
                {
                    LOGGER.info("All FTP Outbound(s) record are listed: " + gson.toJson(tempFiles));
                    return gson.toJson(new ResponseObject(200, "Empty List", tempFiles));
                }
                LOGGER.info("All FTP Outbound(s) record are listed: " + gson.toJson(tempFiles));
                return gson.toJson(new ResponseObject(200, "All record are mentioned below", tempFiles));
            }
            else
            {
                LOGGER.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while showing FTP Outbound(s): " + gson.toJson(e));
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(path = "/getFTPOutboundById/{id}", produces = "application/json", method = RequestMethod.GET)
    public String getFTPOutboundById(@PathVariable(value = "id") Integer id, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                if(id instanceof Integer)
                {
                    if (outboundFTPQuery.findOne(id) == null)
                    {
                        LOGGER.info("FTP outbound  not found: " + gson.toJson(outboundFTPQuery.findOne(id)));
                        return new Gson().toJson(new ResponseObject(500, "Record Does Not Exist", null));
                    }
                    LOGGER.info("FTP Outbound is found: " + gson.toJson(outboundFTPQuery.findOne(id)));
                    return gson.toJson(new ResponseObject(200, "Record is find against :" + id, outboundFTPQuery.findOne(id)));
                }
                else
                {
                    LOGGER.info("400: Bad Request...");
                    return new Gson().toJson(new ResponseObject(400, "Bad Request...", null));
                }

            }
            else
            {
                LOGGER.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
      return null;
    }

    @RequestMapping(path = "/saveOrUpdateFTPOutbound", produces = "application/json", method = RequestMethod.POST)
    public String saveOrUpdateFTPOutbound(@RequestBody OutBoundFTP outBoundFTP, @RequestHeader(value = "token") String token) throws JsonProcessingException
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                User objUser = userRepositoryDAO.findByToken(token);
                outBoundFTP.setCustomerId(objUser.getCustomer().getCustomerId());
                LOGGER.info("\nFTP outbound is saved: " + gson.toJson(outBoundFTP));
                return gson.toJson(new ResponseObject(200, "Record is saved ", outboundFTPQuery.save(outBoundFTP)));
            }
            else
            {
                LOGGER.info("\n403: Invalid User\n");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("\nError while saving ftp outbound: " + gson.toJson(e));
            e.printStackTrace();
        }
       return null;
    }

    @RequestMapping(path = "/deleteFTPOutboundById/{id}", produces = "application/json", method = RequestMethod.DELETE)
    public String deleteFTPOutboundById(@PathVariable(value = "id") Integer id, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                if(outboundFTPQuery.findOne(id) == null)
                {
                    LOGGER.info("FTP outbound not deleted:" + id);
                    return new Gson().toJson(new ResponseObject(500 , "Record Does Not Exist", null));
                }
                else
                {
                    OutBoundFTP obj = outboundFTPQuery.findOne(id);
                    outboundFTPQuery.delete(obj);
                    LOGGER.info("FTP outbound is deleted:" + gson.toJson(obj));
                    return new Gson().toJson(new ResponseObject(200, "Record has been deleted sucessfully...", null));
                }
            }
            else
            {
                LOGGER.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while deleting FTPOutboundById\n", e);
            e.printStackTrace();
        }
        return null;
    }
}