package com.gv.fmu.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.gv.fmu.domain.*;
import com.gv.fmu.dto.*;
import com.gv.fmu.repository.*;
import com.gv.fmu.util.HibernateProxyTypeAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.*;

/**
 * Created by Muhammad Yaseen on 5/2/2018.
 */
@RestController
@RequestMapping("/fileMergingPattern")
public class FileMergingPatternController
{
    private static Logger Logger = LoggerFactory.getLogger(FileMergingPatternController.class);

    private final UserController userController;

    @Autowired
    private FileMergingPatternDAO fileMergingPattern;

    @Autowired
    private InputOutputSettingDAO inputOutputSettingQuery;

    @Autowired
    private MergingJobContextDAO mergingJobContextDAO;

    @Autowired
    private UserRepositoryDAO userRepositoryDAO;

    @Autowired
    private MergingWorkflowDAO mergingWorkflowQuery;

    public FileMergingPatternController(UserController userController)
    {
        this.userController = userController;
    }

    @RequestMapping(path = "/directoryIsUsedForInbound", produces = "application/json", method = RequestMethod.POST)
    public String checkDirectoryIsUsedForInboundByOtherCustomers(@RequestHeader(value = "token") String token, @RequestBody DirectoryExistDTO directoryExistDTO) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try {
            /*
                TODO: check Directory on behalf ip, directory and customer id
                if directory is used with any ip with same customer then it is not allowed to used with other customer
                and other ip.
                if same customer is using same directory then he/she can used again else no customer have access to used that directory.
             */
            if (userController.verifyToken(token))
            {
                String usedDirectory = fileMergingPattern.getDirectoryIsUsedForInbound(directoryExistDTO.getDirectory(), directoryExistDTO.getIp(), directoryExistDTO.getCustomerId());
                if ((usedDirectory == null))
                {
                    Logger.info("200: Directory is available");
                    return gson.toJson(new ResponseObject(200, "Directory is available", null));
                }
                else
                {
                    Logger.info("403: Directory is in used");
                    return gson.toJson(new ResponseObject(403, "Directory is in used", null));
                }
            }
            else
            {
                Logger.info("403: Invalid User");
                return gson.toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            Logger.error("Error:" + e);
        }
        return "";
    }

    @RequestMapping(path = "/checkDirectoryByWorkflowWithInCustomer", produces = "application/json", method = RequestMethod.GET)
    public String checkDirectoryByWorkflowWithInCustomer(@RequestHeader(value = "token") String token, @Param(value = "directory") String directory)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try
        {
            if (userController.verifyToken(token))
            {
                String str = fileMergingPattern.checkDirectoryByWorkflowWithInCustomer(directory);
                if (str != null)
                {
                    Logger.info("201: Directory in used, Please select different one");
                    return gson.toJson(new ResponseObject(201, "Directory in used, Please select different one", null));
                }
                else
                {
                    Logger.info("200: Directory is available");
                    return gson.toJson(new ResponseObject(200, "Directory is available", null));
                }
            }
            else
            {
                Logger.info("403: Invalid User");
                return gson.toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            Logger.info("Error while checking workflow directory\n", e);
            e.printStackTrace();
        }
        return "";
    }

    @RequestMapping(path = "/checkDocumentIdentifierExist", produces = "application/json", method = RequestMethod.GET)
    public String checkDocumentIdentifierExist(@RequestHeader(value = "token")String token, @RequestParam(value = "documentIdentifier")String documentIdentifier)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try
        {
            if (userController.verifyToken(token))
            {
                if(documentIdentifier.equals(fileMergingPattern.checkDocumentIdentifierExist(documentIdentifier)))
                {
                    Logger.info("203: Document Identifier already exist");
                    return gson.toJson(new ResponseObject(203, "Document Identifier already exist", null));
                }
                else
                {
                    Logger.info("200: Available");
                    return gson.toJson(new ResponseObject(200, "Available", null));
                }
            }
            else
            {
                Logger.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch(Exception e)
        {
            Logger.info("Error While Checking Document Identifier Exist\n", e);
            e.printStackTrace();
        }
        return "";
    }


    @RequestMapping(path = "/getAllCustomerIdentifierByCustomer", produces = "application/json", method = RequestMethod.GET)
    public String getAllCustomerIdentifierByCustomer(@RequestHeader(value = "token") String token, @RequestParam(value = "customerId") String customerId)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if (userController.verifyToken(token))
            {
                List<String> tempFiles = mergingJobContextDAO.getAllCustomerIdentifierByCustomer(customerId);
                Logger.info("All inbound custom token(s) are listed: " + gson.toJson(tempFiles));
                return gson.toJson(new ResponseObject(200, "All record are mentioned below", tempFiles));
            }
            else
            {
                Logger.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            Logger.info("Error while Getting All Customer Identifier By Customer\n", e);
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(path = "/getAllFileMergingPatterns", produces = "application/json", method = RequestMethod.GET)
    public String getAllMergingPatterns(@RequestHeader(value = "token")String token, @RequestParam(value = "customerId")String customerId)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(userController.verifyToken(token))
            {
                List<FileMergingPattern> tempFiles = fileMergingPattern.getAllFileMergingPatternByCustomerId(customerId);
                List<FileMergingPatternDTO> fileMergingPatternDTO = new ArrayList<>();
                Type objType = new TypeToken<List<InboundCustomTokens>>() {}.getType();

                if(tempFiles.size() <=0)
                {
                    Logger.info("200: There is no record");
                    return gson.toJson(new ResponseObject(200, "There is no record", tempFiles));
                }
                for(FileMergingPattern temp : tempFiles)
                {
                    FileMergingPatternDTO obj = new FileMergingPatternDTO();
                    obj.setFileMergingPatternId(temp.getFileMergingPatternId());
                    try
                    {
                        obj.setInboundCustomToken(new Gson().fromJson(temp.getInboundCustomToken(), objType));
                    }
                    catch (IllegalStateException | JsonSyntaxException exception)
                    {
                        exception.printStackTrace();
                    }

                    obj.setInputOutputSetting(temp.getInputOutputSetting());
                    obj.setInboundCustomerIdentifier(temp.getInboundCustomerIdentifier());
                    obj.setInboundDocumentIdentifier(temp.getInboundDocumentIdentifier());
                    obj.setInboundDocumentSeprator(temp.getInboundDocumentSeprator());
                    obj.setInboundPackageIdentifier(temp.getInboundPackageIdentifier());
                    obj.setInboundPatternName(temp.getCompletePattern());
                    obj.setCustomerId(temp.getCustomerId());
                    obj.setInboundPatternName(temp.getPatternName());
                    fileMergingPatternDTO.add(obj);
                }
                Logger.info("All file merging pattern(s) are listed: " + gson.toJson(fileMergingPatternDTO));
                return gson.toJson(new ResponseObject(200, "All record are mentioned below", fileMergingPatternDTO));
            }
            else
            {
                Logger.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }

        }
        catch (Exception e)
        {
            Logger.info("Error while showing file merging pattern(s)\n", e);
            e.printStackTrace();
        }
        return null;
    }

    //Get single record
    @RequestMapping(path = "/getFileMergingPatternById/{id}", produces = "application/json", method = RequestMethod.GET)
    public String getSingleMergingPattern(@PathVariable(value = "id") Long id, @RequestHeader(value = "token") String token)
    {
        try
        {
            if(userController.verifyToken(token))
            {
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
                GsonBuilder b = new GsonBuilder();
                b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
                Gson gson = b.create();
                if (id instanceof Long)
                {
                    if (fileMergingPattern.findOne(id) == null)
                    {
                        Logger.info("500: Record Does Not Exist");
                        return new Gson().toJson(new ResponseObject(500, "Record Does Not Exist", null));
                    }
                    else
                    {
                        Logger.info("File merging pattern is find: " + gson.toJson(fileMergingPattern.findOne(id)));
                        return gson.toJson(new ResponseObject(200, "Record is find against: " + id, (fileMergingPattern.findOne(id))));
                    }
                }
            }
            else
            {
                Logger.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            Logger.info("Error in finding file merging pattern\n", e);
            e.printStackTrace();
        }
       return null;
    }

    @RequestMapping(path = "/saveOrUpdateFileMergingPattern", produces = "application/json", method = RequestMethod.POST)
    public String saveOrUpdateFileMergingPattern(@RequestBody List<InputOutputSettingFileMergigPatternDTO> inputOutputSettingFileMergigPatternDTO, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(userController.verifyToken(token))
            {
                Type objType = new TypeToken<List<InboundCustomTokens>>() {}.getType();
                List<FileMergingPattern> fileMergingPatterns = new ArrayList<>();
                Long mergingJobContextId = null;
                User objUser = userRepositoryDAO.findByToken(token);
                for(int x = 0; x < inputOutputSettingFileMergigPatternDTO.size(); x++)
                {
                    FileMergingPatternDTO fileMergingPatternDTO = inputOutputSettingFileMergigPatternDTO.get(x).getFileMergingPattern();
                    FileMergingPattern objFileMergingPattern = new FileMergingPattern();
                    MergingJobContext objMergingJobContext = new MergingJobContext();
                    MergingWorkFlow mergingWorkFlow = new MergingWorkFlow();

                    if(inputOutputSettingFileMergigPatternDTO.get(x).getMergingJobContext().getMergingJobContextId() != null)
                    {
                        mergingJobContextId = inputOutputSettingFileMergigPatternDTO.get(x).getMergingJobContext().getMergingJobContextId();

                        mergingJobContextDAO.save(inputOutputSettingFileMergigPatternDTO.get(x).getMergingJobContext());
                    }
                    else if(mergingJobContextId == null)
                    {
                        objMergingJobContext.setMergingJobContextId(inputOutputSettingFileMergigPatternDTO.get(x).getMergingJobContext().getMergingJobContextId());
                        objMergingJobContext.setCustomIdentifier(inputOutputSettingFileMergigPatternDTO.get(x).getMergingJobContext().getCustomIdentifier());
                        objMergingJobContext.setBundleIdentifier(inputOutputSettingFileMergigPatternDTO.get(x).getMergingJobContext().getBundleIdentifier());
                        objMergingJobContext.setFileExtension(inputOutputSettingFileMergigPatternDTO.get(x).getMergingJobContext().getFileExtension());
                        objMergingJobContext.setCustomerId(objUser.getCustomer().getCustomerId());
                        objMergingJobContext.setFileMergingPattern(fileMergingPatterns);
                        mergingJobContextDAO.save(objMergingJobContext);
                        mergingJobContextId = objMergingJobContext.getMergingJobContextId();

                        mergingWorkFlow.setWorkflowId(inputOutputSettingFileMergigPatternDTO.get(x).getMergingWorkFlow().getWorkflowId());
                        mergingWorkFlow.setWorkflowName(inputOutputSettingFileMergigPatternDTO.get(x).getMergingWorkFlow().getWorkflowName());
                        mergingWorkFlow.setActions(0);
                        mergingWorkFlow.setUser(objUser);
                        mergingWorkFlow.setCustomerId(objUser.getCustomer().getCustomerId());
                        mergingWorkFlow.setMergingJobContext(objMergingJobContext);
                        mergingWorkflowQuery.save(mergingWorkFlow);

                    }

                    else
                    {
//                        mergingJobContextDAO.save(inputOutputSettingFileMergigPatternDTO.get(x).getMergingJobContext());
                    }

                    // CR:31-07-18 GA: Eliminate Duplicate code

                    if (inputOutputSettingFileMergigPatternDTO.get(x).getInputOutputSetting() == null)
                    {
                        objFileMergingPattern.setInputOutputSetting(new InputOutputSetting());
                        String inboundPatternName = "";

                        //saving merging job context
                        objMergingJobContext.setMergingJobContextId(mergingJobContextId);
                        objFileMergingPattern.setMergingJobContextId(objMergingJobContext.getMergingJobContextId());

                        //setting properties for pattern
                        objFileMergingPattern.setFileMergingPatternId(fileMergingPatternDTO.getFileMergingPatternId());
                        objFileMergingPattern.setInboundCustomerIdentifier(objMergingJobContext.getCustomIdentifier());
                        objFileMergingPattern.setInboundCustomerIdentifierOrder(fileMergingPatternDTO.getInboundCustomerIdentifierOrder());
                        objFileMergingPattern.setInboundDocumentIdentifier(fileMergingPatternDTO.getInboundDocumentIdentifier());
                        objFileMergingPattern.setInboundDocumentIdentifierOrder(fileMergingPatternDTO.getInboundDocumentIdentifierOrder());
                        objFileMergingPattern.setInboundPackageIdentifier(objMergingJobContext.getBundleIdentifier());
                        objFileMergingPattern.setInboundPackageIdentifierOrder(fileMergingPatternDTO.getInboundPackageIdentifierOrder());
                        objFileMergingPattern.setInboundDocumentSeprator(fileMergingPatternDTO.getInboundDocumentSeprator());
                        objFileMergingPattern.setCustomerId(objUser.getCustomer().getCustomerId());
                        objFileMergingPattern.setPatternName(fileMergingPatternDTO.getInboundPatternName());
                        objFileMergingPattern.setFileOrder(fileMergingPatternDTO.getFileOrder());

                        objFileMergingPattern.setInboundCustomToken(new Gson().toJson(inputOutputSettingFileMergigPatternDTO.get(x).getFileMergingPattern().getInboundCustomToken()));

                        Map<Integer, String> pattern = new LinkedHashMap<Integer, String>();
                        pattern.put(fileMergingPatternDTO.getInboundCustomerIdentifierOrder(), fileMergingPatternDTO.getInboundCustomerIdentifier());
                        pattern.put(fileMergingPatternDTO.getInboundDocumentIdentifierOrder(), fileMergingPatternDTO.getInboundDocumentIdentifier());
                        pattern.put(fileMergingPatternDTO.getInboundPackageIdentifierOrder(), fileMergingPatternDTO.getInboundPackageIdentifier());

                        if (inputOutputSettingFileMergigPatternDTO.get(x).getFileMergingPattern().getInboundCustomToken() != null)
                        {
                            for (InboundCustomTokens str : inputOutputSettingFileMergigPatternDTO.get(x).getFileMergingPattern().getInboundCustomToken())
                            {
                                pattern.put(str.getCustomOrder(), str.getCustomValue());
//                              inboundPatternName +=  str.getCustomValue() + fileMerging.getInboundDocumentSeprator();
                            }
                        }

                        for (Map.Entry<Integer, String> temp : pattern.entrySet())
                        {
                            inboundPatternName = inboundPatternName + temp.getValue() + fileMergingPatternDTO.getInboundDocumentSeprator();
                        }
                        objFileMergingPattern.setCompletePattern(inboundPatternName);
                    }
                    else
                    {
                        if (inputOutputSettingFileMergigPatternDTO.get(x).getInputOutputSetting().getFileTransportID() == null) {
                            objFileMergingPattern.setInputOutputSetting(null);
                        } else {
                            InputOutputSetting getinInputOutputSetting = inputOutputSettingQuery.findOne(inputOutputSettingFileMergigPatternDTO.get(x).getInputOutputSetting().getFileTransportID());
                            objFileMergingPattern.setInputOutputSetting(getinInputOutputSetting);
                            inputOutputSettingQuery.save(getinInputOutputSetting);
                        }


                        if (inputOutputSettingFileMergigPatternDTO.get(x).getMergingJobContext() != null)
                        {
                            String inboundPatternName = "";

                            //saving merging job context
                            objMergingJobContext.setMergingJobContextId(mergingJobContextId);
                            objFileMergingPattern.setMergingJobContextId(objMergingJobContext.getMergingJobContextId());

                            //setting properties for pattern
                            objFileMergingPattern.setFileMergingPatternId(fileMergingPatternDTO.getFileMergingPatternId());
                            objFileMergingPattern.setInboundCustomerIdentifier(fileMergingPatternDTO.getInboundCustomerIdentifier());
                            objFileMergingPattern.setInboundCustomerIdentifierOrder(fileMergingPatternDTO.getInboundCustomerIdentifierOrder());
                            objFileMergingPattern.setInboundDocumentIdentifier(fileMergingPatternDTO.getInboundDocumentIdentifier());
                            objFileMergingPattern.setInboundDocumentIdentifierOrder(fileMergingPatternDTO.getInboundDocumentIdentifierOrder());
                            if (fileMergingPatternDTO.getInboundPackageIdentifier() == null) {
                                objFileMergingPattern.setInboundPackageIdentifier("");
                            } else {
                                objFileMergingPattern.setInboundPackageIdentifier(fileMergingPatternDTO.getInboundPackageIdentifier());
                            }


                            objFileMergingPattern.setInboundPackageIdentifierOrder(fileMergingPatternDTO.getInboundPackageIdentifierOrder());
                            objFileMergingPattern.setInboundDocumentSeprator(fileMergingPatternDTO.getInboundDocumentSeprator());
                            objFileMergingPattern.setFtpRemoteDirectory(fileMergingPatternDTO.getFtpRemoteDirectory());
                            objFileMergingPattern.setCustomerId(objUser.getCustomer().getCustomerId());
                            objFileMergingPattern.setPatternName(fileMergingPatternDTO.getInboundPatternName());
                            objFileMergingPattern.setFileOrder(fileMergingPatternDTO.getFileOrder());

                            Map<Integer, String> pattern = new HashMap<Integer, String>();
                            pattern.put(fileMergingPatternDTO.getInboundCustomerIdentifierOrder(), fileMergingPatternDTO.getInboundCustomerIdentifier());
                            pattern.put(fileMergingPatternDTO.getInboundDocumentIdentifierOrder(), fileMergingPatternDTO.getInboundDocumentIdentifier());
                            pattern.put(fileMergingPatternDTO.getInboundPackageIdentifierOrder(), fileMergingPatternDTO.getInboundPackageIdentifier());

                            if (inputOutputSettingFileMergigPatternDTO.get(x).getFileMergingPattern().getInboundCustomToken() == null)
                            {
                                objFileMergingPattern.setInboundCustomToken("");
                            }
//                            else (inputOutputSettingFileMergigPatternDTO.get(x).getFileMergingPattern().getInboundCustomToken().size() != 0)
                            else {
                                for (InboundCustomTokens str : inputOutputSettingFileMergigPatternDTO.get(x).getFileMergingPattern().getInboundCustomToken())
                                {
                                    if(str.getUuid().equals("") || str.getUuid().equals(" ") || str.getUuid().equals(null))
                                    {
                                        str.setUuid(UUID.randomUUID().toString());
                                        str.setDocumentIdentifier(fileMergingPatternDTO.getInboundDocumentIdentifier());
                                    }
                                    else
                                    {
                                        str.setUuid("");
                                    }
                                    pattern.put(str.getCustomOrder(), str.getCustomValue());
                                }
                                objFileMergingPattern.setInboundCustomToken(new Gson().toJson(inputOutputSettingFileMergigPatternDTO.get(x).getFileMergingPattern().getInboundCustomToken()));
                            }


                            for (Map.Entry<Integer, String> temp : pattern.entrySet())
                            {
                                inboundPatternName = inboundPatternName + temp.getValue() + fileMergingPatternDTO.getInboundDocumentSeprator();
                            }

                            if(inboundPatternName.lastIndexOf(fileMergingPatternDTO.getInboundDocumentSeprator()) != -1)
                            {
                                inboundPatternName = inboundPatternName.substring(0, inboundPatternName.length() - 1);
                            }
                            else
                            {
                                inboundPatternName = inboundPatternName.substring(0, inboundPatternName.length());
                            }
                            objFileMergingPattern.setCompletePattern(inboundPatternName);
                        }
                        else
                        {
                            Logger.info("400: Please select any job context first");
                            return gson.toJson(new ResponseObject(400, "Please select any job context first ", null));
                        }
                    }
                    fileMergingPatterns.add(objFileMergingPattern);
                }
                Logger.info("File merging pattern is saved: " + fileMergingPatterns);
                return gson.toJson(new ResponseObject(200, "Record is saved ", fileMergingPattern.save(fileMergingPatterns)));
            }
            else
            {
                Logger.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            Logger.info("Error while saving file merging pattern\n ", e);
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(path = "/deleteFileMergingPatternById/{id}", produces = "application/json", method = RequestMethod.DELETE)
    public String deleteFileMergingPatternById(@PathVariable(value = "id") Long id, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if (userController.verifyToken(token))
            {
                if (fileMergingPattern.findOne(id) == null)
                {
                    Logger.info("500: Record Does Not Exist");
                    return new Gson().toJson(new ResponseObject(500, "Record Does Not Exist", null));
                }
                else
                {
                    String json = null;
                    try
                    {
                        json = objectMapper.writeValueAsString(fileMergingPattern.findOne(id));
                        FileMergingPattern objFileMergedPattern = fileMergingPattern.findOne(id);
                        fileMergingPattern.delete(objFileMergedPattern);

                        Logger.info("File merging pattern is deleted:" + gson.toJson(objFileMergedPattern));
                        return new Gson().toJson(new ResponseObject(200, "Record has been deleted sucessfully against: " + id, null));
                    }
                    catch (JsonProcessingException e)
                    {
                        Logger.info("Error while deleting file merging pattern\n ", e);
                        e.printStackTrace();
                    }
                    return json;
                }
            }
            else
            {
                Logger.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
}