package com.gv.fmu.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gv.fmu.domain.Customer;
import com.gv.fmu.domain.User;
import com.gv.fmu.dto.ResponseObject;
import com.gv.fmu.dto.UserCustomerDTO;
import com.gv.fmu.repository.CustomerDAO;
import com.gv.fmu.repository.UserRepositoryDAO;
import com.gv.fmu.util.HibernateProxyTypeAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Muhammad Yaseen on 4/24/2018.
 */

@RestController
@RequestMapping("/customer")
public class CustomerController
{
    private static Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CustomerDAO customerRepositoryQuery;

    @Autowired
    private UserRepositoryDAO userRepositoryDAO;

    //Get all records
    @RequestMapping(path = "/getAllCustomers", produces = "application/json", method = RequestMethod.GET)
    public String getAllCustomer()
    {
        GsonBuilder b = new GsonBuilder();
        Gson gson = b.create();
        try
        {
            //Get All Customers
            List<Customer> tempFiles = customerRepositoryQuery.findAll();
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
            LOGGER.info("Customer List: " + gson.toJson(tempFiles));
            return gson.toJson(new ResponseObject(200, "All record are mentioned below", tempFiles));
        }
        catch (Exception e)
        {
            LOGGER.error("Error While Showing List Of Customer: " + gson.toJson(e));
            e.printStackTrace();
        }
        return null;
    }

    //Get customer by id
    @RequestMapping(path = "/getCustomerById/{id}", produces = "application/json", method = RequestMethod.GET)
    public String getCustomerById(@PathVariable(value = "id") Long customerId)
    {
        GsonBuilder b = new GsonBuilder();
        Gson gson = b.create();

        try
        {
            if(customerId instanceof Long)
            {
                if (customerRepositoryQuery.findOne(customerId) == null)
                {
                    LOGGER.info("500: Record Does Not Exist");
                    return new Gson().toJson(new ResponseObject(500, "Record Does Not Exist", null));
                }
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
                b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);

                LOGGER.info("Customer is find: " + gson.toJson(customerRepositoryQuery.findOne(customerId)));
                return gson.toJson(new ResponseObject(200, "Records is find against id: " + customerId, customerRepositoryQuery.findOne(customerId)));
            }
            else
            {
                LOGGER.info("400: Bad Request");
                return new Gson().toJson(new ResponseObject(400, "Bad Request...", null));
            }

        }
        catch (Exception e)
        {
            LOGGER.error("Error While Selecting Customer By Id\n", e);
            e.printStackTrace();
        }
        return null;
    }

    //Create new customer
    @RequestMapping(path = "/saveOrUpdateCustomer", produces = "application/json", method = RequestMethod.POST)
    public String saveOrUpdateCustomer(@Valid @RequestBody UserCustomerDTO userCustomerDTO)
    {
        GsonBuilder b = new GsonBuilder();
        Gson gson = b.create();

        try
        {
            User objUser = userCustomerDTO.getUser();
            Customer objCustomer = userCustomerDTO.getCustomer();
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
            if(objCustomer.getCustomerEmail().equals(customerRepositoryQuery.checkCustomerExist(objCustomer.getCustomerEmail())) && objUser.getUserName().equals(userRepositoryDAO.checkUserExist(objUser.getUserName())))
            {
                LOGGER.warn("Customer email or User Name already exists please select difference one");
                return gson.toJson(new ResponseObject(401, "Customer email or User Name already exists please select difference one", null));
            }
            else if(!objCustomer.getCustomerEmail().equals(customerRepositoryQuery.checkCustomerExist(objCustomer.getCustomerEmail())) && objUser.getUserName().equals(userRepositoryDAO.checkUserExist(objUser.getUserName())))
            {
                LOGGER.warn("Customer email or User Name already exists please select difference one");
                return gson.toJson(new ResponseObject(401, "Customer email or User Name already exists please select difference one", null));
            }
            else
            {
                LOGGER.info("Customer Record Saved: " + gson.toJson(customerRepositoryQuery.save(objCustomer)));
                objUser.setCustomer(objCustomer);
                objUser.setEmail(objCustomer.getCustomerEmail());
                userRepositoryDAO.save(objUser);
                LOGGER.info("200: Customer Record Is Saved");
                return gson.toJson(new ResponseObject(200, "Customer Record Saved:", customerRepositoryQuery.save(objCustomer)));
            }
        }
        catch (Exception e)
        {
            LOGGER.error("Error While Saving Customer Record\n", e);
            e.printStackTrace();
        }
        return null;
    }

    // Delete a customer
    @RequestMapping(path = "/deleteCustomerById/{id}", produces = "application/json", method = RequestMethod.DELETE)
    public String deleteCustomerById(@PathVariable(value = "id") Long id)
    {
        GsonBuilder b = new GsonBuilder();
        Gson gson = b.create();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        try
        {
            if(customerRepositoryQuery.findOne(id) == null)
            {
                LOGGER.info("Record Does Not Exist");
                return new Gson().toJson(new ResponseObject(500 , "Record Does Not Exist", null));
            }
            else
            {
                Customer customer = customerRepositoryQuery.findOne(id);
                customerRepositoryQuery.delete(customer);
                LOGGER.info("Customer Record Deleted\n", customer);
                return new Gson().toJson(new ResponseObject(200, "Record Has Been Deleted Sucessfully against id: " + id, null));
            }
        }
        catch (Exception e)
        {
            LOGGER.error("Error While Deleting Customer Record\n", e);
            e.printStackTrace();
        }
        return null;
    }
}