package com.gv.fmu.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.gv.fmu.domain.*;
import com.gv.fmu.dto.*;
import com.gv.fmu.repository.*;
import com.gv.fmu.util.HibernateProxyTypeAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.*;

/**
 * Created by Muhammad Yaseen on 6/8/2018.
 */

@RestController
@RequestMapping("/outboundPattern")
@Lazy
public class OutboundPatternController
{
    private static Logger LOGGER = LoggerFactory.getLogger(OutboundPatternController.class);

    @Autowired
    private OutboundPatternDAO outboundPatternQuery;

    @Autowired
    private UserController userController;

    @Autowired
    private UserRepositoryDAO userRepositoryDAO;

    @Autowired
    private OutboundFTPDAO outboundFTPDAO;

    @Autowired
    private FileMergingPatternDAO fileMergingPatternDAO;

    @Autowired
    private MergingWorkflowDAO mergingWorkflowDAO;

    @Autowired
    private InputOutputSettingDAO inputOutputSettingDAO;

    @RequestMapping(path = "/directoryIsUsed", produces = "application/json", method = RequestMethod.POST)
    public String directoryIsUsed(@RequestHeader(value = "token") String token, @RequestBody DirectoryExistDTO directoryExistDTO) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try {
            if (userController.verifyToken(token)) {
                String usedDirectory = fileMergingPatternDAO.getUsedDirectory(directoryExistDTO.getDirectory());
                if ((usedDirectory == null))
                {
                    LOGGER.info("\n200: Directory is available: ");
                    return gson.toJson(new ResponseObject(200, "Directory is available", null));
                } else
                {
                    LOGGER.info("\n403: Directory is in used ");
                    return gson.toJson(new ResponseObject(403, "Directory is in used", null));
                }
            } else
            {
                LOGGER.info("\n403: Invalid User");
                return gson.toJson(new ResponseObject(403, "Invalid User", null));
            }
        } catch (Exception e)
        {
            LOGGER.info("\nError: directoryIsUsed", e);
            e.printStackTrace();
        }
        return "";
    }

    @RequestMapping(path = "/getAllTokensByWorkflowId", produces = "application/json", method = RequestMethod.GET)
    public String getAllTokensByWorkflowId(@RequestHeader(value = "token") String token, @RequestParam(value = "workflowId") String workflowId)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if (userController.verifyToken(token))
            {
                List<String> tempFiles = fileMergingPatternDAO.getAllTokensByWorkflowId(workflowId);
                LOGGER.info("All inbound custom token(s) are listed: " + gson.toJson(tempFiles));
                Type objType = new TypeToken<List<InboundCustomTokens>>() {}.getType();
                List<List<InboundCustomTokens>> tokensList = new ArrayList<>();
                List<InboundCustomTokens> inboundCustomTokenses = new ArrayList<>();

                for (String tokenSet : tempFiles)
                {
                    if(!tokenSet.isEmpty())
                        tokensList.add(new Gson().fromJson(tokenSet, objType));
                }

                for (List<InboundCustomTokens> customTokenses : tokensList)
                {
                    for (InboundCustomTokens tokens : customTokenses)
                    {
                        inboundCustomTokenses.add(tokens);
                    }
                }
                LOGGER.info("All inbound custom token(s) are listed: " + gson.toJson(inboundCustomTokenses));
                return gson.toJson(new ResponseObject(200, "All record are mentioned below", inboundCustomTokenses));
            }
            else
            {
                LOGGER.info("\n403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.error("Error getAllTokensByWorkflowId" + e);
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(path = "/getAllOutboundPattern", produces = "application/json", method = RequestMethod.GET)
    public String getAllOutboundPattern(@RequestHeader(value = "token")String token, @RequestParam(value = "customerId") String customerId)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try
        {
            if(userController.verifyToken(token))
            {
                List<OutBoundPattern> tempFiles = outboundPatternQuery.getAllOutboundPatternByCustomerId(customerId);
                List<OutboundMergingPatternDTO> fileMergingPatternDTO = new ArrayList<>();
                Type objType1 = new TypeToken<List<OutboundCustomTokens>>() {}.getType();
                if(tempFiles.size() <=0)
                {
                    LOGGER.info("All outbound file merging pattern(s) are listed: " + gson.toJson(tempFiles));
                    return gson.toJson(new ResponseObject(200, "There is no record", tempFiles));
                }
                for(OutBoundPattern temp : tempFiles)
                {
                    OutboundMergingPatternDTO outboundMergingPatternDTO = new OutboundMergingPatternDTO();
                    outboundMergingPatternDTO.setOutboundPatternId(temp.getOutboundPatternId());
                    try
                    {
                        outboundMergingPatternDTO.setOutboundCustomToken(new Gson().fromJson(temp.getOutboundCustomToken(), objType1));
                    }
                    catch (IllegalStateException | JsonSyntaxException exception)
                    {
                        exception.printStackTrace();
                    }

                     //obj.setInputOutputSetting(temp.getInputOutputSetting());
                     outboundMergingPatternDTO.setOutboundCustomIdentifier(temp.getOutboundCustomIdentifier());
                     outboundMergingPatternDTO.setOutboundDocumentIdentifier(temp.getOutboundDocumentIdentifier());
                     outboundMergingPatternDTO.setOutboundDocumentSeprator(temp.getOutboundDocumentSeprator());
                     outboundMergingPatternDTO.setOutboundPackageIdentifier(temp.getOutboundPackageIdentifier());
                     outboundMergingPatternDTO.setOutboundPatternName(temp.getOutboundPatternName());
                     outboundMergingPatternDTO.setCustomerId(temp.getCustomerId());
                     fileMergingPatternDTO.add(outboundMergingPatternDTO);
                }
                LOGGER.info("All outbound file merging pattern(s) are listed: " + gson.toJson(fileMergingPatternDTO));
                return gson.toJson(new ResponseObject(200, "All record are mentioned below", fileMergingPatternDTO));
            }
            else
            {
                LOGGER.error("\n403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while showing outbound file merging pattern(s): " + gson.toJson(e));
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(path = "/getOutboundPatternById/{id}", produces = "application/json", method = RequestMethod.GET)
    public String getOutboundPatternById(@PathVariable(value = "id") Integer id, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try
        {
            if(userController.verifyToken(token))
            {
                if (id instanceof Integer)
                {
                    if (outboundPatternQuery.findOne(id) == null)
                    {
                        LOGGER.info("Outbound file merging pattern not found: " + gson.toJson(outboundPatternQuery.findOne(id)));
                        return new Gson().toJson(new ResponseObject(500, "Record Does Not Exist", null));
                    }
                    else
                    {
                        LOGGER.info("Outbound file merging pattern is found: " + gson.toJson(outboundPatternQuery.findOne(id)));
                        return gson.toJson(new ResponseObject(200, "Record is find against: " + id, (outboundPatternQuery.findOne(id))));
                    }
                }
            }
            else
            {
                LOGGER.error("\n403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.error("\nError: getOutboundPatternById", e);
            e.printStackTrace();
        }
       return null;
    }

    @RequestMapping(path = "/saveOrUpdateOutboundPattern", produces = "application/json", method = RequestMethod.POST)
    public String saveOrUpdateOutboundPattern(@Valid @RequestBody OutboundMergingPatternDTO outboundMergingPatternDTO, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(userController.verifyToken(token))
            {
                Type objType = new TypeToken<List<OutboundCustomTokens>>() {}.getType();
                User objUser = userRepositoryDAO.findByToken(token);
                OutBoundPattern outboundPattern = new OutBoundPattern();
                String outboundPatternName = "";

                outboundPattern.setOutboundCustomToken(new Gson().toJson(outboundMergingPatternDTO.getOutboundCustomToken()));
                outboundPattern.setOutboundPatternId(outboundMergingPatternDTO.getOutboundPatternId());
                outboundPattern.setOutboundCustomIdentifier(outboundMergingPatternDTO.getOutboundCustomIdentifier());
                outboundPattern.setOutboundCustomIdentifierOrder(outboundMergingPatternDTO.getOutboundCustomIdentifierOrder());
                outboundPattern.setOutboundDocumentIdentifier(outboundMergingPatternDTO.getOutboundDocumentIdentifier());
                outboundPattern.setOutboundDocumentIdentifierOrder(outboundMergingPatternDTO.getOutboundDocumentIdentifierOrder());
                outboundPattern.setOutboundDocumentSeprator(outboundMergingPatternDTO.getOutboundDocumentSeprator());
                outboundPattern.setOutboundPackageIdentifier(outboundMergingPatternDTO.getOutboundPackageIdentifier());
                outboundPattern.setOutboundPackageIdentifierOrder(outboundMergingPatternDTO.getOutboundPackageIdentifierOrder());
                outboundPattern.setCustomerId(objUser.getCustomer().getCustomerId());
                outboundPattern.setEmail(outboundMergingPatternDTO.getEmail());

                Map<Integer, String> pattern = new HashMap<Integer, String>();

                pattern.put(outboundMergingPatternDTO.getOutboundCustomIdentifierOrder(), outboundMergingPatternDTO.getOutboundCustomIdentifier());
                pattern.put(outboundMergingPatternDTO.getOutboundPackageIdentifierOrder(), outboundMergingPatternDTO.getOutboundPackageIdentifier());

                if(outboundMergingPatternDTO.getOutboundCustomToken() != null)
                {
                    for(OutboundCustomTokens str : outboundMergingPatternDTO.getOutboundCustomToken())
                    {
                        if(str.getUuid().equals("") || str.getUuid().equals(" ") || str.getUuid().equals(null))
                        {
                            str.setUuid(UUID.randomUUID().toString());
                        }
                        else
                        {
                            str.setUuid("");
                        }
                        pattern.put(str.getCustomOrder(), str.getCustomValue());
                    }
                    outboundPattern.setOutboundCustomToken(new Gson().toJson(outboundMergingPatternDTO.getOutboundCustomToken()));
                } else
                    outboundPattern.setOutboundCustomToken("");

                for (Map.Entry<Integer, String> temp : pattern.entrySet())
                {
                    outboundPatternName = outboundPatternName + temp.getValue() + outboundMergingPatternDTO.getOutboundDocumentSeprator();
                }

                if(outboundPatternName.lastIndexOf(outboundPattern.getOutboundDocumentSeprator()) != -1)
                {
                    outboundPatternName = outboundPatternName.substring(0, outboundPatternName.length() - 1);
                }

                outboundPattern.setOutboundPatternName(outboundPatternName);
                MergingWorkFlow mergingWorkFlow = mergingWorkflowDAO.findOne(outboundMergingPatternDTO.getWorkflowId());

                if(outboundMergingPatternDTO.getOutboundPatternId() > 0)
                {
                    OutBoundPattern objOutBoundPattern  = outboundPatternQuery.getOne(outboundMergingPatternDTO.getOutboundPatternId());
                    outboundPattern.setEmailSubject(objOutBoundPattern.getEmailSubject());
                    outboundPattern.setEmailBody(objOutBoundPattern.getEmailBody());
                }
                else
                {
                    outboundPattern.setEmailSubject(outboundMergingPatternDTO.getEmailBody());
                    outboundPattern.setEmailBody(outboundMergingPatternDTO.getEmailSubject());
                }
                OutBoundPattern outboundPatternId = outboundPatternQuery.save(outboundPattern);
                LOGGER.info("Outbound file merging pattern is saved: " + gson.toJson(outboundPattern));
                mergingWorkFlow.setOutBoundPattern(outboundPatternId);
                mergingWorkflowDAO.save(mergingWorkFlow);
                List<OutBoundFTP> savingObjects = new ArrayList<>();

                for(int x = 0; x < outboundMergingPatternDTO.getOutBoundFTPs().size(); x++)
                {
                    OutBoundFTP outBoundFTP = new OutBoundFTP();
                    outBoundFTP.setCustomerId(outboundPatternId.getCustomerId());
                    outBoundFTP.setOutboundId(outboundMergingPatternDTO.getOutBoundFTPs().get(x).getOutboundId());
                    outBoundFTP.setOutboundPatternId(outboundPatternId.getOutboundPatternId());
                    outBoundFTP.setOutboundDirectory(outboundMergingPatternDTO.getOutBoundFTPs().get(x).getOutboundDirectory());
                    outBoundFTP.setOutboundFTPName(outboundMergingPatternDTO.getOutBoundFTPs().get(x).getOutboundFTPName());
                    InputOutputSetting inputOutputSetting = inputOutputSettingDAO.findOne(outboundMergingPatternDTO.getOutBoundFTPs().get(x).getFileTransportID());
                    if (inputOutputSetting == null) {
                        outBoundFTP.setFileTransportID(null);
                    } else {
                        outBoundFTP.setFileTransportID(inputOutputSetting.getFileTransportID());
                    }

                    LOGGER.info("Outbound file ftp directory is saved: " + gson.toJson(outBoundFTP));
//                    outboundFTPDAO.save(outBoundFTP);

                    if (outBoundFTP.getOutboundId() == 0) {
                        savingObjects.add(outBoundFTP);
                        outboundFTPDAO.save(savingObjects);
                        //'32, '/gvtest/fmu/merged6546465465', 'ABCD', 1, 1)
//                        outboundFTPDAO.saveOutboundFTP(outBoundFTP.getOutboundId(), outBoundFTP.getOutboundPatternId(), outBoundFTP.getOutboundDirectory(), outBoundFTP.getOutboundFTPName(), outBoundFTP.getFileTransportID(), outBoundFTP.getCustomerId());
                    } else {
                        outboundFTPDAO.updateOutBoundFTP(outBoundFTP.getOutboundId(), outBoundFTP.getOutboundPatternId(), outBoundFTP.getOutboundDirectory(),
                                outBoundFTP.getOutboundFTPName(), outBoundFTP.getFileTransportID(), outBoundFTP.getCustomerId());
                    }

                    /*
                        outBoundFTP.setFileName(outboundPatternName);
                        outBoundFTP.setOutboundFTPName(outboundMergingPatternDTO.getOutBoundFTPs().get(x).getOutboundFTPName());
                        outBoundFTP.setOutboundHostUserName(outboundMergingPatternDTO.getOutBoundFTPs().get(x).getOutboundHostUserName());
                        outBoundFTP.setOutboundHostPassword(outboundMergingPatternDTO.getOutBoundFTPs().get(x).getOutboundHostPassword());
                        outBoundFTP.setOutboundIpAddress(outboundMergingPatternDTO.getOutBoundFTPs().get(x).getOutboundIpAddress());
                        outBoundFTP.setOutboundPort(outboundMergingPatternDTO.getOutBoundFTPs().get(x).getOutboundPort());
                    */

                }

                LOGGER.info("Outbound file merging pattern is saved: " + gson.toJson(outboundPattern));
                return gson.toJson(new ResponseObject(200, "Record is saved ", outboundMergingPatternDTO));
            }
            else
            {
                LOGGER.error("\n403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while saving outbound file merging pattern: " + gson.toJson(e));
            e.printStackTrace();
        }
       return null;
    }

    @RequestMapping(path = "/deleteOutboundById/{id}", produces = "application/json", method = RequestMethod.DELETE)
    public String deleteOutboundById(@PathVariable(value = "id") Integer id, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try
        {
            if (userController.verifyToken(token))
            {
                if (outboundPatternQuery.findOne(id) == null)
                {
                    LOGGER.error("\n500: Record Does Not Exist");
                    return new Gson().toJson(new ResponseObject(500, "Record Does Not Exist", null));
                }
                else
                {

                    String json = null;
                    try
                    {
                        json = objectMapper.writeValueAsString(outboundPatternQuery.findOne(id));
                        OutBoundPattern outBoundPattern = outboundPatternQuery.findOne(id);
                        outboundPatternQuery.delete(outBoundPattern);
                        LOGGER.info("Outbound file merging pattern is deleted: " + gson.toJson(outBoundPattern));
                        return new Gson().toJson(new ResponseObject(200, "Record has been deleted sucessfully against: " + id, null));
                    }
                    catch (JsonProcessingException e)
                    {
                        LOGGER.info("Error while deleting outbound file merging pattern: " + gson.toJson(e));
                        e.printStackTrace();
                    }
                    return json;
                }
            }
            else
            {
                LOGGER.error("\n403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.error("\nError: deleteOutboundById", e);
            e.printStackTrace();
        }
        return null;
    }
}