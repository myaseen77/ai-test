package com.gv.fmu.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gv.fmu.domain.MergingJobContext;
import com.gv.fmu.domain.User;
import com.gv.fmu.dto.ResponseObject;
import com.gv.fmu.repository.MergingJobContextDAO;
import com.gv.fmu.repository.UserRepositoryDAO;
import com.gv.fmu.util.HibernateProxyTypeAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Muhammad Yaseen on 5/23/2018.
 */
@RestController
@RequestMapping("/mergingJobContext")
public class MergingJobContextController
{
    private static Logger LOGGER = LoggerFactory.getLogger(MergingJobContextController.class);

    private final UserController USER_CONTROLLER;

    @Autowired
    private MergingJobContextDAO mergingJobContextQuery;

    @Autowired
    private UserRepositoryDAO userRepositoryDAO;

    public MergingJobContextController(UserController user_controller)
    {
        USER_CONTROLLER = user_controller;
    }

    //Get all records

    @RequestMapping(path = "/getAllMergingJobContext", produces = "application/json", method = RequestMethod.GET)
    public String getAllMergingJobContext(@RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                List<MergingJobContext> tempFiles = mergingJobContextQuery.findAll();

                if(tempFiles.size() <= 0)
                {
                    LOGGER.info("200: There is no record");
                    return gson.toJson(new ResponseObject(200, "There is no record", tempFiles));
                }
                else
                {
                    LOGGER.info("All Merging job context(s) are listed: " + gson.toJson(tempFiles));
                    return gson.toJson(new ResponseObject(200, "All record(s) are mentioned below", tempFiles));
                }
            }
            else
            {
                LOGGER.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while showing all merging job context(s): " + gson.toJson(e));
            e.printStackTrace();
        }
        return null;
    }

    //Get MergingJobContext by id

    @RequestMapping(path = "/getMergingJobContextById/{id}", produces = "application/json", method = RequestMethod.GET)
    public String getMergingJobContextById(@PathVariable(value = "id") Long jobId, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                if(jobId instanceof Long)
                {
                    if (mergingJobContextQuery.findOne(jobId) == null)
                    {
                        LOGGER.info("500: Record Does Not Exist");
                        return new Gson().toJson(new ResponseObject(500, "Record Does Not Exist", null));
                    }
                    else
                    {
                        LOGGER.info("Merging job context is find: " + gson.toJson(mergingJobContextQuery.findOne(jobId)));
                        return gson.toJson(new ResponseObject(200, "Records is find against id: " + jobId, mergingJobContextQuery.findOne(jobId)));
                    }
                }
                else
                {
                    LOGGER.info("400: Bad Request");
                    return new Gson().toJson(new ResponseObject(400, "Bad Request...", null));
                }
            }
            else
            {
                LOGGER.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error in finding merging job context: " + gson.toJson(e));
            e.printStackTrace();
        }
        return null;
    }

    //Create new MergingJobContext

    @RequestMapping(path = "/saveOrUpdateMergingJobContext", produces = "application/json", method = RequestMethod.POST)
    public String saveOrUpdateMergingJobContext(@RequestBody MergingJobContext mergingJobContext, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                User objUser = userRepositoryDAO.findByToken(token);
                if(mergingJobContext != null)
                {
                    mergingJobContext.setCustomerId(objUser.getCustomer().getCustomerId());
                    LOGGER.info("Merging job context is saved: " + gson.toJson(mergingJobContext));
                    return gson.toJson(new ResponseObject(200, "Record is saved", mergingJobContextQuery.save(mergingJobContext)));
                }
                else
                {
                    LOGGER.info("400: Bad Request");
                    return new Gson().toJson(new ResponseObject(400, "Bad Request...", null));
                }
            }
            else
            {
                LOGGER.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while saving merging job context: " + gson.toJson(e));
            e.printStackTrace();
        }
        return null;
    }


    @RequestMapping(path = "/deleteMergingJobContextById/{id}", produces = "application/json", method = RequestMethod.DELETE)
    public String deleteMergingJobContextById(@PathVariable(value = "id") Long id, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(USER_CONTROLLER.verifyToken(token))
            {
                if(mergingJobContextQuery.findOne(id) == null)
                {
                    LOGGER.info("500: Record Does Not Exist ");
                    return new Gson().toJson(new ResponseObject(500 , "Record Does Not Exist", null));
                }
                else
                {
                    MergingJobContext mergingJobContext = mergingJobContextQuery.findOne(id);
                    mergingJobContextQuery.delete(mergingJobContext);
                    LOGGER.info("Merging job context is deleted:" + gson.toJson(mergingJobContext));
                    return new Gson().toJson(new ResponseObject(200, "Record has been deleted successfully against: " + id, null));
                }
            }
            else
            {
                LOGGER.info("403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while deleting merging job context: " + gson.toJson(e));
            e.printStackTrace();
        }
       return null;
    }
}
