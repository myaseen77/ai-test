package com.gv.fmu.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gv.fmu.domain.Customer;
import com.gv.fmu.domain.User;
import com.gv.fmu.dto.ResponseObject;
import com.gv.fmu.dto.UserCustomerDTO;
import com.gv.fmu.repository.CustomerDAO;
import com.gv.fmu.repository.UserRepositoryDAO;
import com.gv.fmu.util.HibernateProxyTypeAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Random;

/**
 * Created by Muhammad Yaseen on 5/2/2018.
 */

@RestController
@RequestMapping("/user")
public class UserController
{
    private static Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private final UserRepositoryDAO userRepositoryQuery;
    @Autowired
    private CustomerDAO customerRepositoryQuery;

    public UserController(UserRepositoryDAO userRepositoryQuery)
    {
        this.userRepositoryQuery = userRepositoryQuery;
    }


    @RequestMapping(path = "/getAllUsers", produces = "application/json", method = RequestMethod.GET)
    public  String getAllUser(@RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try
        {
            if(new UserController(userRepositoryQuery).verifyToken(token))
            {
                List<User> tempFiles = userRepositoryQuery.findAll();
                LOGGER.info("All users are listed below: " + gson.toJson(tempFiles));
                return gson.toJson(new ResponseObject(401, "All records are mentioned below", tempFiles));
            }
            else
            {
                LOGGER.error("\nInvalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e )
        {
            LOGGER.info("Error while showing all users" + gson.toJson(e));
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(path = "/getUserById/{id}",produces = "application/json", method = RequestMethod.GET)
    public  String getUserById(@PathVariable(value = "id") Long id, @RequestHeader(value = "token") String token)
    {
        if(id instanceof Long)
        {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            GsonBuilder b = new GsonBuilder();
            b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
            Gson gson = b.create();
            try
            {
                if(new UserController(userRepositoryQuery).verifyToken(token))
                {
                    if (userRepositoryQuery.findOne(id) == null)
                    {
                        LOGGER.error("\n500: Record Does Not Exist");
                        return new Gson().toJson(new ResponseObject(500, "Record Does Not Exist", null));
                    }
                    LOGGER.info("User is find: " + (userRepositoryQuery.findOne(id)));
                    return gson.toJson(new ResponseObject(200, "Record is find against: " + id, (userRepositoryQuery.findOne(id))));
                }
                else
                {
                    LOGGER.error("\n403: Invalid User");
                    return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
                }

            }
            catch (Exception e)
            {
                LOGGER.info("Error in finding user: " + gson.toJson(e));
                e.printStackTrace();
            }

        }
        return null;
    }

    @RequestMapping(path = "/saveOrUpdateUserRecord", produces = "application/json", method = RequestMethod.POST)
    public String saveOrUpdateUserRecord(@Valid @RequestBody UserCustomerDTO userCustomer, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        try
        {
            if(new UserController(userRepositoryQuery).verifyToken(token))
            {
                User objUser = userCustomer.getUser();
                Long newCustomerId = userCustomer.getCustomer().getCustomerId();
                Customer customer1 = customerRepositoryQuery.findOne(newCustomerId);
                objUser.setCustomer(customer1);
                LOGGER.info("User Record Saved: " + gson.toJson(userRepositoryQuery.save(objUser)));
                return gson.toJson(new ResponseObject(200, "Record is saved ", (userRepositoryQuery.save(objUser))));

/*                if(objUser.getUserName().equals(userRepositoryQuery.checkUserExist(objUser.getUserName())))
                {
                    LOGGER.info("User already exists: " + objUser.getUserName());
                    return gson.toJson(new ResponseObject(409 , "User already exists please select different one", null));
                }
                else
                {
                    LOGGER.info("User Record Saved: " + gson.toJson(userRepositoryQuery.save(objUser)));
                    return gson.toJson(new ResponseObject(200, "Record is saved ", (userRepositoryQuery.save(objUser))));
                }*/
            }
            else
            {
                LOGGER.error("\n403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while saving user record: " + gson.toJson(e));
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(path = "/deleteUserById/{id}", produces = "application/json", method = RequestMethod.DELETE)
    public  String deleteUserById(@PathVariable(value = "id") Long id, @RequestHeader(value = "token") String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();

        try
        {
            if(new UserController(userRepositoryQuery).verifyToken(token))
            {
                if(userRepositoryQuery.findOne(id) == null)
                {
                    LOGGER.error("\n500: Record Does Not Exist");
                    return new Gson().toJson(new ResponseObject(500 , "Record Does Not Exist", null));
                }
                 /*  if(userRepositoryQuery.findOne(id) == userRepositoryQuery.findOne(id))
                {
                    return new Gson().toJson(new ResponseObject(500, "You Can't Delete This Record...", userRepositoryQuery.findOne(id)));
                }*/
                else
                {
                    User objUser = userRepositoryQuery.findOne(id);
                    userRepositoryQuery.delete(objUser);
                    LOGGER.info("User record deleted:" + gson.toJson(objUser));
                    return new Gson().toJson(new ResponseObject(200, "Record has been deleted sucessfully...", null));
                }
            }
            else
            {
                LOGGER.error("\n403: Invalid User");
                return new Gson().toJson(new ResponseObject(403, "Invalid User", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while deleting user record: " + gson.toJson(e));
            e.printStackTrace();
        }
        return null;

    }

    @RequestMapping(path = "/login", produces = "application/json", method = RequestMethod.GET)
    public String login(@RequestParam(value = "userName") String userName, @RequestParam(value = "password") String password) throws JsonProcessingException
    {
        User user;
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        Gson gson = b.create();
        ObjectMapper objectMapper = new ObjectMapper();

        try
        {
            user = new User();
            user = userRepositoryQuery.findByUserNameAndPassword(userName, password);
            if(user == null)
            {
                return new Gson().toJson(new ResponseObject(401 , "Invalid Username Or Password", null));
            }
            else if(userName.equals(null) || userName.equals("") || userName.equals(" "))
            {
                return new Gson().toJson(new ResponseObject(500 , "Empty Username", null));
            }
            else if(password.equals(null) || password.equals("") || password.equals(" "))
            {
                return new Gson().toJson(new ResponseObject(500 , "Empty Password", null));
            }

            else if(!userName.equalsIgnoreCase(user.getUserName()) && !password.equalsIgnoreCase(user.getPassword()))
            {
                return new Gson().toJson(new ResponseObject(404 , "Invalid Username & Password", null));
            }

            else if(!userName.equalsIgnoreCase(user.getUserName()))
            {
                return new Gson().toJson(new ResponseObject(401 , "Invalid User Name", null));
            }

            else if(!password.equalsIgnoreCase(user.getPassword()))
            {
                return new Gson().toJson(new ResponseObject(401 , "Invalid Password", null));
            }

            else if(userName != null && password != null)
            {
                if (userName.equals(user.getUserName()) && password.equals(user.getPassword()))
                {
                    user.setUserToken(new UserController(userRepositoryQuery).tokenGenerator());
                    String strGeneratedToken = user.getUserToken();
                    while (true)
                    {
                        if(strGeneratedToken.equals(userRepositoryQuery.findByToken(strGeneratedToken))) {
                            user.setUserToken(new UserController(userRepositoryQuery).tokenGenerator());
                        }
                        else {
                            userRepositoryQuery.save(user);
                        }

                        break;
                    }

                    LOGGER.info("User logged in successfully: " + gson.toJson(user));
                    return gson.toJson(new ResponseObject(200, "Logged in", user));
                }
                else
                {
                    return new Gson().toJson(new ResponseObject(404, "User Not Found: ", null));
                }
            }
            else
            {
                return new Gson().toJson(new ResponseObject(520, "Unknown Error", null));
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Error while login into account:" + gson.toJson(e));
        }
        return null;
    }

    public  String tokenGenerator()
    {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder sbr = new StringBuilder();
        Random random = new Random();
        while (sbr.length() < 18)
        {
            // length of the random string.
            int index = (int) (random.nextFloat() * characters.length());
            sbr.append(characters.charAt(index));
        }

        String result = sbr.toString();
        return result;
    }

    public  boolean verifyToken(String token)
    {
        User objUser = userRepositoryQuery.findByToken(token);
        if(objUser != null)
        {
            String toke = userRepositoryQuery.findByToken(token).getUserToken();
            if(token == null)
            {
                new Gson().toJson(new ResponseObject(403, "Invalid User", null));
                return false;
            }
            if(userRepositoryQuery.findByToken(token).getUserToken() == null)
            {
                new Gson().toJson(new ResponseObject(403, "There is no user", toke));
                return true;
            }
            if(token.equals(userRepositoryQuery.findByToken(token).getUserToken()))
            {
                new Gson().toJson(new ResponseObject(200, "Valid user",token));
                return true;
            }
        }
        else
        {
            new Gson().toJson(new ResponseObject(403, "Invalid User", null));
        }
        return false;
    }
}
