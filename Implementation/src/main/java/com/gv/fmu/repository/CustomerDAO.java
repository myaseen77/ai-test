package com.gv.fmu.repository;

import com.gv.fmu.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by Muhammad Yaseen on 4/24/2018.
 */

@Repository
public interface CustomerDAO extends JpaRepository<Customer, Long>
{
    @Query(value = "SELECT customer_email FROM customer WHERE customer_email =:customerEmail", nativeQuery = true)
    public String checkCustomerExist(@Param("customerEmail") String customerEmail);
}
