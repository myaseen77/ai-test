package com.gv.fmu.repository;

import com.gv.fmu.domain.MergedFiles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Muhammad Yaseen on 5/2/2018.
 */

@SuppressWarnings("JpaQlInspection")
@Repository
public interface MergedFilesDAO extends JpaRepository<MergedFiles, Long>
{

    @Query(value = "select * from merged_files where file_context =:context", nativeQuery = true)
    public List<MergedFiles> findMergedFilesByContext(@Param(value = "context") String status);

    @Query(value = "select * from merged_files", nativeQuery = true)
    public List<MergedFiles> mergedFilesByContext();

    @Query(value = "select * from merged_files " +
            "where customer_id =:customerId " +
            "AND status LIKE %:status% " +
            "AND " +
            "(timestamp BETWEEN (:startDate) AND (:endDate)) " +
            "OR" +
            "((received_files LIKE %:generalParam% " +
            "OR directory LIKE %:generalParam% " +
            "OR file_context LIKE %:generalParam% ))" +
            "order by timestamp desc limit :offset, :limit",
            nativeQuery = true)
    public List<MergedFiles> mergedByParameters(@Param(value = "status")String status,
                                                @Param(value = "generalParam") String generalParam,
                                                @Param(value = "startDate") String startDate,
                                                @Param(value = "endDate") String endDate,
                                                @Param(value = "offset") Integer offset,
                                                @Param(value = "limit") Integer limit,
                                                @Param(value = "customerId") String customerId);

    @Query(value = "select count(*) from merged_files " +
            "where customer_id =:customerId " +
            "AND status LIKE %:status% " +
            "AND " +
            "(timestamp BETWEEN (:startDate) AND (:endDate)) " +
            "OR" +
            "((received_files LIKE %:generalParam% " +
            "OR directory LIKE %:generalParam% " +
            "OR file_context LIKE %:generalParam% ))",
            nativeQuery = true)
    public Integer countMergedByParameters(@Param(value = "status")String status,
                                                @Param(value = "generalParam") String generalParam,
                                                @Param(value = "startDate") String startDate,
                                                @Param(value = "endDate") String endDate,
                                                @Param(value = "customerId") String customerId);

    @Query(value = "select * from merged_files where status =:status order by timestamp desc", nativeQuery = true)
    public List<MergedFiles> findMergedFilesByStatus(@Param(value = "status") String status);

}
