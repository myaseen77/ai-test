package com.gv.fmu.repository;

import com.gv.fmu.domain.OutBoundFTP;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by WhiztecTestingServer on 6/11/2018.
 */
@Repository
public interface OutboundFTPDAO extends JpaRepository<OutBoundFTP, Integer>
{
    @Query(value = "select * from outbound_ftp where outbound_pattern_id =:id ", nativeQuery = true)
    public List<OutBoundFTP> getOutboundFTPByTransportId(@Param(value = "id") Integer id);

    @Transactional
    @Modifying
    @Query(value = "update outbound_ftp set customer_id =?6, outbound_merged_files_location =?3, ftp_name =?4, file_transportid =?5, outbound_pattern_id =?2 where outbound_id =?1",
            nativeQuery = true)
    public void updateOutBoundFTP(@Param(value = "outboundId") Integer outboundId, @Param(value = "outboundPatternId") Integer outboundPatternId,
                                  @Param(value = "outboundDirectory") String outboundDirectory, @Param(value = "outboundFTPName") String outboundFTPName,
                                  @Param(value = "fileTransportID") Integer fileTransportID, @Param(value = "customerId") Long customerId);

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO outbound_ftp (:outboundId, :outboundPatternId, :outboundDirectory, :outboundFTPName, :fileTransportID, :customerId)", nativeQuery = true)
    public void saveOutboundFTP(@Param(value = "outboundId") Integer outboundId, @Param(value = "outboundPatternId") Integer outboundPatternId,
                                @Param(value = "outboundDirectory") String outboundDirectory, @Param(value = "outboundFTPName") String outboundFTPName,
                                @Param(value = "fileTransportID") Integer fileTransportID, @Param(value = "customerId") Long customerId);


}