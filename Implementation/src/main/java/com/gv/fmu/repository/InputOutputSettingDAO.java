package com.gv.fmu.repository;

import com.gv.fmu.domain.InputOutputSetting;
import com.gv.fmu.domain.MergingWorkFlow;
import com.gv.fmu.dto.InboundCustomTokens;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by Muhammad Yaseen on 5/2/2018.
 */
@Repository
public interface InputOutputSettingDAO extends JpaRepository<InputOutputSetting, Integer>
{
    @Query(value =  "select distinct (workflow_id) from merging_workflow mw, " +
                    "merging_job_context mjb, file_pattern fp, ftp_transport ft " +
                    "where mw.merging_job_context_id = mjb.merging_job_context_id " +
                    "AND mjb.merging_job_context_id = fp.merging_job_context_id " +
                    "AND fp.file_transportid=ft.file_transportid " +
                    "AND ft.file_transportid =:id", nativeQuery = true)
    public List<BigInteger> getMergingWorkFlowIdByFTPInboundSetting(@Param(value = "id") Integer id);

    @Query(value = "select * from ftp_transport where customer_id =:customerId", nativeQuery = true)
    public List<InputOutputSetting> getAllInboundFTPsByCustomer(@Param(value = "customerId") String customerId);
}
