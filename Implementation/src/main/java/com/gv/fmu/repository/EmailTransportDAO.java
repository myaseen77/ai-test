package com.gv.fmu.repository;

import com.gv.fmu.domain.EmailTransport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by Muhammad Yaseen on 5/23/2018.
 */

@Repository
public interface EmailTransportDAO extends JpaRepository<EmailTransport, Long>
{
    @Query(value = "select * from email_transport where customer_id =:customerId", nativeQuery = true)
    public EmailTransport getEmailTransportChannel(@Param(value = "customerId") Long customerId);
}
