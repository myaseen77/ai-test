package com.gv.fmu.repository;

import com.gv.fmu.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Muhammad Yaseen on 5/2/2018.
 */

@SuppressWarnings("ALL")
@Repository
public interface UserRepositoryDAO extends JpaRepository<User, Long>
{
    @Query(value = "SELECT * FROM user WHERE user_name = :param AND user_password = :password", nativeQuery = true)
    public User findByNameAndPassword(@Param("param") String param, @Param("password") String password);

    @Query(value = "SELECT * FROM user WHERE user_password = :password", nativeQuery = true)
    public User findByPassword(@Param("password") String password);

    @Query(value = "SELECT * FROM user WHERE user_name = :userName and user_password = :password", nativeQuery = true)
    public User findByUserNameAndPassword(@Param("userName") String userName,@Param("password") String password);

    @Query(value = "SELECT * FROM user WHERE user_name = :userName", nativeQuery = true)
    public User findByUserName(@Param("userName") String userName);

    @Query(value = "SELECT * FROM user WHERE token = :token", nativeQuery = true)
    public User findByToken(@Param("token") String token);

    @Query(value = "SELECT user_name FROM user WHERE user_name =:userName", nativeQuery = true)
    public String checkUserExist(@Param("userName") String userName);

    @Query(value = "select * from user where customer_id =:customerId", nativeQuery = true)
    public List<User> getAllUsersByCustomer(@Param("customerId") Long customerId);


}
