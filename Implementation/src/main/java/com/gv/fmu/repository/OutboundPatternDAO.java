package com.gv.fmu.repository;

import com.gv.fmu.domain.OutBoundPattern;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Muhammad Yaseen on 6/8/2018.
 */

@Repository
public interface OutboundPatternDAO extends JpaRepository<OutBoundPattern, Integer>
{
    @Query(value = "select * from outbound_pattern where customer_id =:customerId", nativeQuery = true)
    public List<OutBoundPattern> getAllOutboundPatternByCustomerId(@Param(value = "customerId")String customerId);

}
