package com.gv.fmu.repository;

import com.gv.fmu.domain.MergingJobContext;
import com.gv.fmu.domain.MergingWorkFlow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Muhammad Yaseen on 5/14/2018.
 */
@Repository
public interface MergingWorkflowDAO extends JpaRepository<MergingWorkFlow, Long>
{
/*    @Query(value = "select DISTINCT mg.workflow_id, mg.merging_job_context_id, mg.user_id, mg.merging_workflow_name, fp.inbound_pattern_name, fp.ounbound_pattern_name, mg.merging_actions from merging_workflow mg, merging_job_context mjb, file_pattern fp, ftp_transport ftp WHERE mg.merging_job_context_id = mjb.merging_job_context_id AND mjb.file_merging_pattern_id = fp.file_transportid AND fp.file_transportid = ftp.file_transportid", nativeQuery = true)
    public List<MergingWorkFlow> getMergingWorkFlow();*/

    @Query(value = "select workflow_id from merging_workflow where workflow_id <>:id", nativeQuery = true)
    public List<Integer> getAllWorkflowIDs(@Param("id") Integer id);

    @Query(value = "select * from merging_workflow where customer_id =:customerId", nativeQuery = true)
    public List<MergingWorkFlow> getAllMergingWorkflowsByCustomer(@Param(value = "customerId") String customerId);

    @Modifying
    @Transactional
    @Query(value = "select * from merging_workflow where workflow_id =: workflowId", nativeQuery = true)
    public MergingWorkFlow findById(@Param(value = "workflowId") Long workflowId);

    public MergingWorkFlow findByMergingJobContext(MergingJobContext mergingJobContext);

}
