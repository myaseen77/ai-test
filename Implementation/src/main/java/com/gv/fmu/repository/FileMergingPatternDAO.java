package com.gv.fmu.repository;

import com.gv.fmu.domain.FileMergingPattern;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Muhammad Yaseen on 5/2/2018.
 */
public interface FileMergingPatternDAO extends JpaRepository<FileMergingPattern, Long>
{
    @Query(value = "select * from file_pattern where file_transportid =:id", nativeQuery = true)
    public List<FileMergingPattern> getListOfInboundFTPsById(@Param(value = "id") Integer id);

    @Query(value = "select * from file_pattern where customer_id =:customerId", nativeQuery = true)
    public List<FileMergingPattern> getAllFileMergingPatternByCustomerId(@Param(value = "customerId") String customerId);

    @Query(value = "select inbound_document_identifier from file_pattern where inbound_document_identifier =:documentIdentifier", nativeQuery = true)
    public String checkDocumentIdentifierExist(@Param(value = "documentIdentifier") String documentIdentifier);


    @Query(value = "select inbound_custom_token from merging_workflow workflow, " +
            "merging_job_context mergingJobContext, file_pattern filePattern " +
            "WHERE workflow.merging_job_context_id = mergingJobContext.merging_job_context_id " +
            "AND mergingJobContext.merging_job_context_id = filePattern.merging_job_context_id " +
            "AND workflow.workflow_id =:workflowId", nativeQuery = true)
    public List<String> getAllTokensByWorkflowId(@Param(value = "workflowId") String workflowId);

    @Query(value = "select * from file_pattern where ftp_remote_directory =:directory", nativeQuery = true)
    public List<FileMergingPattern> getFileMergingPatternByDirectory(@Param(value = "directory") String directory);

    @Query(value = "select fp.ftp_remote_directory from file_pattern fp, ftp_transport ftp where fp.ftp_remote_directory =:directory AND ftp.inbound_ip_address =:ipAddress AND fp.customer_id <>:customerId", nativeQuery = true)
    public String getDirectoryIsUsedForInbound(@Param(value = "directory") String directory, @Param(value = "ipAddress") String ipAddress, @Param(value = "customerId") String customerId);

    @Query(value = "select ftp_remote_directory from file_pattern where ftp_remote_directory =:directory", nativeQuery = true)
    public String getUsedDirectory(@Param(value = "directory") String directory);

    @Query(value = "select distinct ftp_remote_directory from file_pattern fp, merging_workflow mw, merging_job_context mjc\n" +
            "where\n" +
            "mw.merging_job_context_id = mjc.merging_job_context_id\n" +
            "AND\n" +
            "fp.merging_job_context_id = mjc.merging_job_context_id\n" +
            "AND fp.ftp_remote_directory =:str", nativeQuery = true)
    public String checkDirectoryByWorkflowWithInCustomer(@Param(value = "str") String str);
}
