package com.gv.fmu.repository;

import com.gv.fmu.domain.MergingJobContext;
import org.hibernate.mapping.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by Muhammad Yaseen on 5/23/2018.
 */
@Repository
public interface MergingJobContextDAO extends JpaRepository<MergingJobContext, Long>
{
    @Query(value = "select custom_identifier from merging_job_context where customer_id =:customerId", nativeQuery = true)
    public java.util.List<String> getAllCustomerIdentifierByCustomer(@Param(value = "customerId") String customerId);
}
