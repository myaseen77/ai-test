package com.gv.fmu;

import com.gv.fmu.controller.MergingWorkflowController;
import com.gv.fmu.domain.MergingWorkFlow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.integration.config.EnableIntegration;

import javax.annotation.PostConstruct;


@SpringBootApplication
@ComponentScan("com.gv.fmu")
@EnableIntegration
public class SpringSftpApplication
{
    @Autowired
    private ApplicationContext appContext;

    @Autowired
    private static MergingWorkflowController mergingWorkflowController;

    private static Logger LOGGER = LoggerFactory.getLogger(SpringSftpApplication.class);


/*    static
    {
        LOGGER.info("activating all channels");
        mergingWorkflowController.activeChannelsOnStartServer();
    }*/

    public static void main(String[] args)
    {
        SpringApplication.run(SpringSftpApplication.class, args);
    }

//    @Bean
//    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
//        return args ->
//        {
//            System.out.println("Let's inspect the beans provided by Spring Boot:");
//            String[] beanNames = ctx.getBeanDefinitionNames();
//            Arrays.sort(beanNames);
//            for (String beanName : beanNames)
//            {
//                System.out.println(beanName);
//            }
//        };
//    }


}
