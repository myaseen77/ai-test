package com.gv.fmu.ftp.listener;

import com.gv.fmu.domain.OutBoundFTP;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.file.FileHeaders;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.File;

/**
 * Created by Muhammad Yaseen on 6/3/2018.
 */

@Service
public class FTPTransportService
{

    @Inject
    private ApplicationContext applicationContext;

    @Async
    public void send(OutBoundFTP outBoundFTP) throws JSchException, SftpException
    {

        MessageChannel channel = applicationContext.getBean("inputChannel", MessageChannel.class);
        File folder = new File(outBoundFTP.getFilePath());
//        System.out.println(outBoundFTP.getFileName());
        Message<File> message = MessageBuilder.withPayload(folder).build();
        channel.send(message);
        }

    /*@Inject
    private ApplicationContext applicationContext;

    @Async
    public void send(OutBoundFTP outBoundFTP )
    {
        try
        {
            MessageChannel channel = applicationContext.getBean("output", MessageChannel.class);
            File file = new  File(outBoundFTP.getFilePath());
            MessageBuilder<File> mb = MessageBuilder.withPayload(file);
            if(StringUtils.isNotEmpty(outBoundFTP.getFileName()))
            {
                mb.setHeader(FileHeaders.FILENAME, outBoundFTP.getFileName());
            }

            Message < File > message = mb.setHeader("channelId", outBoundFTP.getOutboundId()).build();

            channel.send(message);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }*/

    public void getAddresses()
    {

    }

    public void shutdown()
    {

    }
}
