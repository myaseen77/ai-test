package com.gv.fmu.ftp.listener;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.gv.fmu.domain.InputOutputSetting;
import com.gv.fmu.domain.OutBoundFTP;
import com.gv.fmu.repository.InputOutputSettingDAO;
import com.gv.fmu.repository.OutboundFTPDAO;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.File;
import java.util.List;
import java.util.Properties;
/**
 * Created by Muhammad Yaseen on 6/11/2018.
 */

@Service
public class DynamicOutboundFTPChannelResolver
{
    public static final int MAX_CACHE_SIZE = 100;
    private final String FTP_XML_PATH = "/ftp-outbound-listen-dynamic.xml";
    @Inject
    private ApplicationContext applicationContext;
    @Autowired
    private OutboundFTPDAO outboundFTPQuery;
    @Autowired
    private InputOutputSettingDAO getOutboundIpAddress;
    private Logger LOGGER = LoggerFactory.getLogger(DynamicOutboundFTPChannelResolver.class);
    private Cache<Integer, MessageChannel> channelCache;
    private Cache <MessageChannel, ConfigurableApplicationContext>	contextCache;

    @PostConstruct
    public void init()
    {
        this.channelCache = CacheBuilder.newBuilder().removalListener(new ChannelRemoveListener()).build();
        this.contextCache = CacheBuilder.newBuilder().build();
    }

    //find channel
    public MessageChannel find(Integer channelId)
    {
        MessageChannel channel = this.channelCache.getIfPresent(channelId);
        if (channel == null)
        {
            channel = createNewCustomerChannel(channelId);
        }
        return channel;
    }

    //reload channel
    public void reloadChannel(Integer channelId)
    {
        // Mark old channel invalidate
        this.channelCache.invalidate(channelId);

        // Recreate channel from databse
        createNewCustomerChannel(channelId);
    }

    public void stopChannel(Integer channelId)
    {
        MessageChannel channel = this.channelCache.getIfPresent(channelId);
        if (channel != null)
        {
            this.channelCache.invalidate(channelId);
            this.channelCache.cleanUp();
        }
    }

    private synchronized MessageChannel createNewCustomerChannel(Integer channelId)
    {
        MessageChannel channel = this.channelCache.getIfPresent(channelId);
        if (channel == null)
        {
            ConfigurableApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[]{FTP_XML_PATH}, false);
            classPathXmlApplicationContext.setParent(applicationContext);
            this.setEnvironmentForCustomer(classPathXmlApplicationContext, channelId);
            classPathXmlApplicationContext.refresh();
            channel = classPathXmlApplicationContext.getBean("toFtpChannel", MessageChannel.class);
            this.channelCache.put(channelId, channel);
            this.contextCache.put(channel, classPathXmlApplicationContext);
        }
        return channel;
    }

    private void setEnvironmentForCustomer(ConfigurableApplicationContext ctx, Integer channelId)
    {

        StandardEnvironment standardEnvironment = new StandardEnvironment();
        Properties props = new Properties();

        // populate properties for customer
        List<InputOutputSetting> temp = getOutboundIpAddress.findAll();
        OutBoundFTP outBoundFTP = outboundFTPQuery.findOne(channelId);
        if(outBoundFTP == null)
        {

        }

        for(InputOutputSetting inputOutputSetting : temp)
        {
/*            props.setProperty("host", outBoundFTP.getOutboundIpAddress());
            props.setProperty("user", outBoundFTP.getOutboundHostUserName());
            props.setProperty("password", outBoundFTP.getOutboundHostPassword());
            props.setProperty("port", outBoundFTP.getOutboundPort().toString());*/

            props.setProperty("host", inputOutputSetting.getInboundIpAddress());
            props.setProperty("user", inputOutputSetting.getHostUserName());
            props.setProperty("password", inputOutputSetting.getHostPassword());
            props.setProperty("port", String.valueOf(inputOutputSetting.getPort()));
            props.setProperty("remote.directory", outBoundFTP.getOutboundDirectory());
        }


        PropertiesPropertySource pps = new PropertiesPropertySource("ftpprops"+ channelId, props);

        standardEnvironment.getPropertySources().addLast(pps);
        ctx.setEnvironment(standardEnvironment);
}

    public void send(OutBoundFTP outBoundFTP, File file)
    {
        try
        {
            ConfigurableApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext(new String[]{FTP_XML_PATH}, false);
            classPathXmlApplicationContext.setParent(applicationContext);
            this.setEnvironmentForCustomer(classPathXmlApplicationContext, outBoundFTP.getOutboundId());
            classPathXmlApplicationContext.refresh();
            MessageChannel channel = classPathXmlApplicationContext.getBean("toFtpChannel", MessageChannel.class);
            MessageBuilder<File> mb = MessageBuilder.withPayload(file);
            if (StringUtils.isNotEmpty(file.getAbsolutePath()))
            {
//                mb.setHeader(FileHeaders.FILENAME, file.getAbsolutePath());
            }
            Message <File> message = mb.setHeader("channelId", outBoundFTP.getOutboundId()).build();
            LOGGER.info(file.getAbsolutePath());
            channel.send(message);
            LOGGER.info(message.getPayload().getAbsolutePath());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    class ChannelRemoveListener implements RemovalListener <Integer, MessageChannel>
    {
        @Override
        public void onRemoval (RemovalNotification <Integer, MessageChannel> notification)
        {
            MessageChannel channel = notification.getValue();
            ConfigurableApplicationContext configurableApplicationContext = contextCache.getIfPresent( channel );
            if (configurableApplicationContext != null)
            {
                configurableApplicationContext.close();
                contextCache.invalidate( channel );
            }
        }
    }
}
