package com.gv.fmu.ftp.listener;

import java.io.File;

/**
 * Created by Muhammad Yaseen on 6/3/2018.
 */
public interface IListenerService
{
    public void onDataReceived();

    File onDataReceived(File ftpFile);

    public void onStatus();
}
