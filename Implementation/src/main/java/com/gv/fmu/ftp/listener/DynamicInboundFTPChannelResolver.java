package com.gv.fmu.ftp.listener;


import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.gv.fmu.connection.setting.FTPConfiguration;
import com.gv.fmu.domain.FileMergingPattern;
import com.gv.fmu.domain.MergingJobContext;
import com.gv.fmu.domain.MergingWorkFlow;
import com.gv.fmu.repository.FileMergingPatternDAO;
import com.gv.fmu.repository.InputOutputSettingDAO;
import com.gv.fmu.repository.MergingJobContextDAO;
import com.gv.fmu.repository.MergingWorkflowDAO;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.math.BigInteger;
import java.util.List;
import java.util.Properties;

/**
 * Created by Muhammad Yaseen on 6/3/2018.
 */

@Service
public class DynamicInboundFTPChannelResolver
{
    public static final int MAX_CACHE_SIZE = 100;
    private static final String XML_PATH_SFTP = "/sftp-inbound-listen-dynamic.xml";
    private static final String XML_PATH_FTP = "/ftp-inbound-listen-dynamic.xml";
    private static final String MESSAGE = "No proper configuration.";
    private static final String SLASH = "/";
    private static Logger logger = LoggerFactory.getLogger(DynamicInboundFTPChannelResolver.class);
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private InputOutputSettingDAO inputOutputSettingQuery;
    @Autowired
    private FileMergingPatternDAO fileMergingPatternDAO;
    @Autowired
    private  FTPConfiguration ftpConfiguration;
    @Autowired
    private MergingWorkflowDAO mergingWorkflowDAO;
    @Autowired
    private MergingJobContextDAO mergingJobContextDAO;
    private Cache<String, MessageChannel> channelCache;

    private Cache<MessageChannel, ConfigurableApplicationContext> contextCache;

    @Autowired
    private MergingWorkflowDAO mergingWorkflowQuery;

    @Autowired
    private DynamicInboundFTPChannelResolver dynamicInboundFTPChannelResolver;

    @PostConstruct
    public void init()
    {
        this.channelCache = CacheBuilder.newBuilder().removalListener(new ChannelRemoveListener()).build();
        this.contextCache = CacheBuilder.newBuilder().build();
        startChannels();

    }

    public void startChannels()
    {
        try
        {
            List<MergingWorkFlow> tempFiles = mergingWorkflowQuery.findAll();
            for (MergingWorkFlow temp : tempFiles)
            {
//                /gvtest/my/inbound||54.39.18.22
                for (FileMergingPattern fileMergingPattern : temp.getMergingJobContext().getFileMergingPattern())
                {
                    if (temp.getActions() == 1)
                        dynamicInboundFTPChannelResolver.reloadChannel(fileMergingPattern.getFtpRemoteDirectory().concat("||").concat(fileMergingPattern.getInputOutputSetting().getInboundIpAddress()));
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Error is: " + e);
            e.printStackTrace();
        }
    }

    public MessageChannel resolve(String directory)
    {
        MessageChannel channel = this.channelCache.getIfPresent(directory);
        if (channel == null) {
            channel = createNewChannelForSFTP(directory);
        }
        return channel;
    }

    public MessageChannel reloadChannel(String directory)
    {
//        /gvtest/my/inbound||54.39.18.22
        // Mark old channel invalidat
        MessageChannel channel = this.channelCache.getIfPresent(directory);
        if (channel != null)
        {
            return channel;
//            this.channelCache.invalidate(directory);
        }
        else
            // Recreate channel from databse
            return createNewChannelForSFTP(directory);
    }

    public void stopChannel(String directory)
    {
        MessageChannel channel = this.channelCache.getIfPresent(directory);
        if (channel != null)
        {
            this.channelCache.invalidate(directory);
            this.channelCache.cleanUp();
        }
    }

    private synchronized MessageChannel createNewChannelForSFTP(String directory)
    {
        // CR:31-07-18 GA: Eliminate Duplicate code

        MessageChannel channel = this.channelCache.getIfPresent(directory);
        String finalDirectory = directory.substring(0, directory.lastIndexOf("||"));
        List<FileMergingPattern> fileMergingPattern = fileMergingPatternDAO.getFileMergingPatternByDirectory(finalDirectory);
        for (FileMergingPattern temp : fileMergingPattern)
        {
            if (channel != null) {
                return channel;
            } else if (temp.getInputOutputSetting().getPort() == 22) {
                ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext(new String[]{XML_PATH_SFTP}, false);
                ctx.setParent(applicationContext);
                this.setFtpEnvironment(ctx, temp.getFtpRemoteDirectory());
                ctx.refresh();
                channel = ctx.getBean("sftpInboundChannel", MessageChannel.class);
                this.channelCache.put(temp.getFtpRemoteDirectory().concat("||").concat(temp.getInputOutputSetting().getInboundIpAddress()), channel);
                this.contextCache.put(channel, ctx);
                return channel;
            } else {
                ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext(new String[]{XML_PATH_FTP}, false);
                ctx.setParent(applicationContext);
                this.setFtpEnvironment(ctx, temp.getFtpRemoteDirectory());
                ctx.refresh();
                channel = ctx.getBean("ftpInbound", MessageChannel.class);
                this.channelCache.put(temp.getFtpRemoteDirectory().concat("||").concat(temp.getInputOutputSetting().getInboundIpAddress()), channel);
                this.contextCache.put(channel, ctx);
                return channel;
            }
        }
        return null;
    }

    private void setFtpEnvironment(ConfigurableApplicationContext configurableApplicationContext, String directory)
    {
        StandardEnvironment env = new StandardEnvironment();
        Properties props = new Properties();

        String direc = directory.substring(directory.lastIndexOf("/"), directory.length());
        //CR:31-07-18 GA: Eliminate Duplicate code
        List<FileMergingPattern> fileMergingPattern = fileMergingPatternDAO.getFileMergingPatternByDirectory(directory);

        for (FileMergingPattern temp : fileMergingPattern)
        {
            MergingJobContext mergingJobContext = mergingJobContextDAO.findOne(temp.getMergingJobContextId());
            MergingWorkFlow workflowId =  mergingWorkflowDAO.findByMergingJobContext(mergingJobContext);//inputOutputSettingQuery.getMergingWorkFlowIdByFTPInboundSetting(temp.getInputOutputSetting().getFileTransportID());
//            for (BigInteger lo : workflowId)
//            {
                String path = ftpConfiguration.getBasePath() + SLASH + temp.getInputOutputSetting().getCustomerId();

                if (temp.getInputOutputSetting().getPort() == 21)
                {
                    String ftpLocalDirectory = makeDirectory(path.concat("/syncFolder")).concat(direc);
                    props.setProperty("workflowId", String.valueOf(workflowId.getWorkflowId()));
                    props.setProperty("ftp.host", temp.getInputOutputSetting().getInboundIpAddress());
                    props.setProperty("ftp.port", temp.getInputOutputSetting().getPort().toString());
                    props.setProperty("ftp.username", temp.getInputOutputSetting().getHostUserName());
                    props.setProperty("ftp.password", temp.getInputOutputSetting().getHostPassword());
                    props.setProperty("ftp.remote-directory", temp.getFtpRemoteDirectory());
                    props.setProperty("ftp.local-directory", String.valueOf(ftpLocalDirectory));
                    String message1 = "Channel Created 21:".concat(props.toString());
                    logger.info(message1);
                    PropertiesPropertySource pps = new PropertiesPropertySource("FTP Props for channel id: " + temp.getFileMergingPatternId(), props);
                    env.getPropertySources().addLast(pps);
                    configurableApplicationContext.setEnvironment(env);

                    temp.getInputOutputSetting().setFtpLocalDirectory(ftpLocalDirectory);
                    inputOutputSettingQuery.save(temp.getInputOutputSetting());
                }
                else
                {
                    if (temp.getInputOutputSetting() == null)
                    {
                        throw new RuntimeException(MESSAGE);
                    }

                    if (StringUtils.isEmpty(temp.getInputOutputSetting().getInboundIpAddress()))
                    {
                        throw new RuntimeException(MESSAGE);
                    }

                    if (temp.getInputOutputSetting().getPort() != null && StringUtils.isEmpty(temp.getInputOutputSetting().getPort().toString()))
                    {
                        throw new RuntimeException(MESSAGE);
                    }

                    if (StringUtils.isEmpty(temp.getInputOutputSetting().getHostUserName()))
                    {
                        throw new RuntimeException(MESSAGE);
                    }

                    if (StringUtils.isEmpty(temp.getInputOutputSetting().getHostPassword()))
                    {
                        throw new RuntimeException(MESSAGE);
                    }
                    String ftpLocalDirectory = makeDirectory(path.concat(directory));
                    props.setProperty("workflowId", String.valueOf(workflowId.getWorkflowId()));
                    props.setProperty("ftp.host", temp.getInputOutputSetting().getInboundIpAddress());
                    props.setProperty("ftp.port", temp.getInputOutputSetting().getPort().toString());
                    props.setProperty("ftp.username", temp.getInputOutputSetting().getHostUserName());
                    props.setProperty("ftp.password", temp.getInputOutputSetting().getHostPassword());
                    props.setProperty("ftp.remote-directory", temp.getFtpRemoteDirectory());
                    props.setProperty("ftp.local-directory", String.valueOf(ftpLocalDirectory));
                    temp.getInputOutputSetting().setFtpLocalDirectory(ftpLocalDirectory);
                    inputOutputSettingQuery.save(temp.getInputOutputSetting());
                    String message = "Channel Created other:".concat(props.toString());
                    logger.info(message);
                    PropertiesPropertySource pps = new PropertiesPropertySource("FTP Props for channel id: " + temp.getFileMergingPatternId(), props);
                    env.getPropertySources().addLast(pps);
                    configurableApplicationContext.setEnvironment(env);

                }
//            }
        }
    }

    public String makeDirectory(String path)
    {
        String ftpLocalDirectory = null;
        File dir = new File(path);
        if(dir.exists())
        {

            ftpLocalDirectory = String.valueOf(dir);
        }
        else
        {
            dir.mkdir();
            String str = String.valueOf(dir);
            ftpLocalDirectory = str;
        }
        return ftpLocalDirectory;
    }

    class ChannelRemoveListener implements RemovalListener<String, MessageChannel>
    {
        @Override
        public void onRemoval(RemovalNotification<String, MessageChannel> notification)
        {
            MessageChannel channel = notification.getValue();
            ConfigurableApplicationContext ctx = contextCache.getIfPresent(channel);
            if (ctx != null)
            {
                // shouldn't be null ideally
                ctx.close();
                contextCache.invalidate(channel);
            }
        }
    }
}
