/*
 * @Project   : EDDI
 * @Package   : gv.ecgd.util.enums
 * @FileName  : TransportTypeEnum.java
 *
 * Copyright 2013-2014
 * GeoViz Inc, 
 * All rights reserved.
 * 
 */
package com.gv.fmu.enums;


/**
 * @author    : Zeeshan
 * @Date      : Feb 14, 2014
 * @version   : Ver. 1.0.0
 *
 * 						   <center><b>TransportTypeEnum.java</b></center>
 * 						<center><b>Modification History</b></center>
 * <pre>
 *
 * ________________________________________________________________________________________________
 *
 *  Developer				Date		     Version		Operation		Description
 * ________________________________________________________________________________________________ 
 *	
 * 
 * ________________________________________________________________________________________________
 * </pre>
 *
 */
public enum TransportTypeEnum {

	FTP, EMAIL, WS;
	
}
