package com.gv.fmu;

import com.gv.fmu.SpringSftpApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

/**
 * Created by Muhammad Yaseen on 5/18/2018.
 */

public class ServletInitializer extends SpringBootServletInitializer
{
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
    {
        return application.sources(SpringSftpApplication.class);
    }

}
